import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import relatedFiles from '@salesforce/apex/ShareFilesController.relatedFiles';
import saveFile from '@salesforce/apex/ShareFilesController.saveFile';
import deleteFiles from '@salesforce/apex/ShareFilesController.deleteFiles';
import updateFileSharing from '@salesforce/apex/ShareFilesController.updateFileSharing';
import updateAllFileSharing from '@salesforce/apex/ShareFilesController.updateAllFileSharing';
import { NavigationMixin } from 'lightning/navigation';

const columns = [
    { label: 'Name', fieldName: 'AttachmentName', type: 'button' ,
        typeAttributes: {variant: 'base'} },
    { label: 'Created Date', fieldName: 'AttachmentCreateDate', type: 'date' },
    { label: 'Created By', fieldName: 'AttachmentCreatedBy'},
];

export default class PartnerSharedAttachementParent extends NavigationMixin(LightningElement) {
    @api recordId;
    @track data;
    @track col = columns;
    @track isModalOpen = false;
    @track showDeleteButton = false;
    @track rowsToDelete;
    @track shareWithConnection = false;
    @track showAttachmentData = false;
    @track openModal = false;
    @track IdToDelete;
    @track NameToDelete;
    @track showShareAllButton = false;

    connectedCallback(){
        console.log('recordId: ', this.recordId);
        this.getRelatedFiles();
    }

    openFileModal(){
        this.isModalOpen = true;
    }

    closeFileModal(){
        this.isModalOpen = false;
    }

    getSelectedId(event){
        this.rowsToDelete = event.detail.selectedRows;
        if(this.rowsToDelete){
            this.showDeleteButton = true;
        }
        else{
            this.showDeleteButton = false;
        }
    }

    shareFileCheckbox(event){
        console.log('is shared checkbox checked? ',event.target.checked);
        this.shareWithConnection = event.target.checked;
    }

    uploadHelper() {
        this.file = this.filesUploaded[0];
       if (this.file.size > this.MAX_FILE_SIZE) {
            return ;
        }
        this.showLoadingSpinner = true;
        // create a FileReader object 
        this.fileReader= new FileReader();
        // set onload function of FileReader object  
        this.fileReader.onloadend = (() => {
            this.fileContents = this.fileReader.result;
            let base64 = 'base64,';
            this.content = this.fileContents.indexOf(base64) + base64.length;
            this.fileContents = this.fileContents.substring(this.content);
            
            // call the uploadProcess method 
            this.saveToFile();
        });
    
        this.fileReader.readAsDataURL(this.file);
    }

    saveToFile() {
        console.log('this.recordToLink',this.recordToLink);
        saveFile({ idParent: this.recordId, strFileName: 'Test This', base64Data: encodeURIComponent(this.fileContents), shareWithConnection: this.shareWithConnection})
        .then(result => {
            window.console.log('result ====> ' +result);
            // refreshing the datatable
            this.getRelatedFiles();
            this.isModalOpen = false;
            this.shareWithConnection = false;
        })
        .catch(error => {
            // Showing errors if any while inserting the files
            window.console.log('Error>>> ', error);
        });
        
    }

    getRelatedFiles() {
        relatedFiles({recordId: this.recordId})
        .then(data => {
            if(data){
                console.log(data);
                this.data = data;
                for(let i = 0; i < this.data.length; i++) {
                    if(this.data[i].ShowToggle == true){
                        this.showShareAllButton = true;
                        break;
                    }
                }
            }
            else{
                this.data = null;
            }
        })
        .catch(error => {
            console.log(error);
        });
    }

    deleteFiles(event){
        this.openModal = false;
        console.log('IdCollection', event.target.value);
        deleteFiles({listOfFileIdsToDelete: event.target.value})
        .then(data => {
            if(data){
                this.data = data;
                for(let i = 0; i < this.data.length; i++) {
                    if(this.data[i].ShowToggle == true){
                        this.showShareAllButton = true;
                        break;
                    }
                }
                this.rowsToDelete = null;
            }
            else{
                this.data = null;
            }
        })
        .catch(error => {
            console.log(error);
        });
       
    }

    filePreview(event) {
        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: 'filePreview'
            },
            state : {
                selectedRecordId: event.target.value
            }
        });
    }

    toggleChange(event){
        console.log('Id To Toggle', event.target.value);
        console.log('checked Toggle', event.target.checked);
        updateFileSharing({AttachmentId: event.target.value, ShareValue: event.target.checked})
        .then(data => {
            if(data){
                console.log(data);
               
                // this.data = data;
                // this.rowsToDelete = null;
            }
            else{
                // this.data = null;
            }
        })
        .catch(error => {
            console.log(error);
        });
    }

    expandAttachements(){
        this.showAttachmentData = true;
    }

    retractAttachements(){
        this.showAttachmentData = false;
    }

    downloadFile(event){
        console.log('item to download'+ JSON.stringify(event.target.value));
        let downloadElement = document.createElement('a');
        downloadElement.href = event.target.value;
        // downloadElement.setAttribute("download","download");
        downloadElement.download = 'download.jpg';
        downloadElement.target = '_blank';
        downloadElement.click(); 
        // window.open(downloadElement);
    }

    openDeleteModal(event){
        this.IdToDelete = event.target.value.AttachmentId;
        this.NameToDelete = event.target.value.AttachmentName;
        console.log('IdToDelete', this.IdToDelete);
        console.log('NameToDelete', this.NameToDelete);
        this.openModal = true;
    }

    closeDeleteModal(){
        this.openModal = false;
        this.IdToDelete = null;
        this.NameToDelete = null;
    }

    shareAllFiles(){
        console.log('IN SHARE ALL FILES');
        updateAllFileSharing({recordId: this.recordId})
        .then(data => {
            if(data){
                console.log(data);
                const event = new ShowToastEvent({
                    variant: 'Success',
                    title: 'Success',
                    message: `Sharing all files for Case ${this.recordId}!`,
                });
                this.dispatchEvent(event);
            }
            else{
                // this.data = null;
            }
        })
        .catch(error => {
            console.log(error);
        });
    }
}