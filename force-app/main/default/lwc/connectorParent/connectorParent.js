import { LightningElement, api } from 'lwc';

export default class ConnectorParent extends LightningElement {
    @api recordId;
    //Boolean tracked variable to indicate if modal is open or not default value is false as modal is closed when page is loaded 
    isModalOpen = false;
    openModal() {
        console.log('in Open Modal');
        // to open modal set isModalOpen tarck value as true
        this.isModalOpen = true;
    }
    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.isModalOpen = false;
        const closeModal = new CustomEvent('close');
        this.dispatchEvent(closeModal);
    }
    submitDetails() {
        // to close modal set isModalOpen tarck value as false
        //Add your code to call apex method or do some processing
        this.isModalOpen = false;
    }
}