import { LightningElement, api, wire } from 'lwc';
import getExternalSharingRecords from '@salesforce/apex/SF2SFConnectorUtility.getExternalSharingRecords';

export default class CaseStatusBannerComponent extends LightningElement {
    @api recordId;
    externalSharing = false;
    externalSharingRecord;

    @wire(getExternalSharingRecords, {recordId: '$recordId'})
    wiredSharingRecords({ error, data }) {
        if (data) {
            console.log('getExternalSharingRecords', data);
            this.externalSharing = true;
            this.externalSharingRecord = data;
            
        } else if (error) {
            this.error = error;
        }
    }
}