import { LightningElement, api, wire, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getExternalSharingRecords from '@salesforce/apex/SF2SFConnectorUtility.getExternalSharingRecords';
import getConnectionOptions from '@salesforce/apex/SF2SFConnectorUtility.getConnectionOptions';
import stopSharing from '@salesforce/apex/SF2SFConnectorUtility.stopSharing';
import startSharing from '@salesforce/apex/SF2SFConnectorUtility.startSharing';

export default class Sf2sfConnector extends LightningElement {
    @api recordId;
    @track record;
    @track error;
    @track attachmentFields = [];
    @track commentFields =[];
    @track caseFields =[];
    @track externalSharingRecord;
    @track externalSharing = false;
    @track ConnectionOptions =[];
    @track ConnectionName = '';
    @track showSyncButton = false;
    @track shareHistoricFiles = false;

    get checkboxOptions() {
        return [
            { label: 'Case', value: 'Case' },
            { label: 'CaseComment', value: 'CaseComment' },
            { label: 'Attachment', value: 'Attachment' },
        ];
    }

    @wire(getExternalSharingRecords, {recordId: '$recordId'})
    wiredSharingRecords({ error, data }) {
        if (data) {
            console.log('getExternalSharingRecords', data);
            this.externalSharing = true;
            this.externalSharingRecord = data;
            
        } else if (error) {
            this.error = error;
        }
    }

    @wire(getConnectionOptions)
    wiredConnectionOptions({ error, data }) {
        if (data) {
            console.log('getConnectionOptions', data);
            data.forEach(key => {
                this.ConnectionOptions.push({
                    label : key.ConnectionName,
                    value: key.Id
                })
            });
            
        } else if (error) {
            this.error = error;
        }
    }

    value = 'inProgress';
    ConnectionId = '';

    handleChange(event) {
        console.log('event.detail.label', event.detail.label);
        this.ConnectionId = event.detail.value;
        for (let i = 0; i < this.ConnectionOptions.length; i++) {
            console.log('this.ConnectionOptions', this.ConnectionOptions[i]);
            if(this.ConnectionOptions[i].value == this.ConnectionId){
                this.ConnectionName = this.ConnectionOptions[i].label;
            }
        }
        this.showSyncButton = true;
    }
    
    //Boolean tracked variable to indicate if modal is open or not default value is false as modal is closed when page is loaded 
    isModalOpen = false;
    openModal() {
        console.log('in Open Modal');
        // to open modal set isModalOpen tarck value as true
        this.isModalOpen = true;
    }

    stopSharing(){
        console.log('stop sharing Id', this.recordId);
        this.externalSharing = false;
        stopSharing({recordId: this.recordId})
        this.closeModal();
        const event = new ShowToastEvent({
            variant: 'warning',
            title: 'Disconnecting',
            message: `Case ${this.recordId} is disconnecting from ${this.externalSharingRecord.PartnerName}!`,
        });
        this.dispatchEvent(event);
    }

    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.isModalOpen = false;
        const closeModal = new CustomEvent('close');
            this.dispatchEvent(closeModal);
    }

    submitDetails() {
        // to close modal set isModalOpen tarck value as false
        //Add your code to call apex method or do some processing
        this.isModalOpen = false;
        startSharing({recordId: this.recordId, ConnectionId: this.ConnectionId, shareFiles:this.shareHistoricFiles})
        this.closeModal();
        const event = new ShowToastEvent({
            variant: 'Success',
            title: 'Success',
            message: `Case ${this.recordId} is connecting to ${this.ConnectionName}!`,
        });
        this.dispatchEvent(event);
    }

    handleCheckboxChange(event){
        this.shareHistoricFiles = event.detail.checked;
        console.log('this.shareHistoricFiles ', this.shareHistoricFiles);
    }

}