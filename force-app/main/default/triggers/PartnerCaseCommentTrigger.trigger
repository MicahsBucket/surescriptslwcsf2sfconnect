trigger PartnerCaseCommentTrigger on Partner_Case_Comments__c (after insert) {
    map<Id, List<Partner_Case_Comments__c>> CaseToPartnerCommentsMap = new map<Id, List<Partner_Case_Comments__c>>();
    // map<Id, List<PartnerNetworkRecordConnection>> LinkedEntityIdToConnection = new map<Id, List<PartnerNetworkRecordConnection>>();
    for(Partner_Case_Comments__c a: Trigger.new){
        if(!CaseToPartnerCommentsMap.containskey(a.Case__c)){
            CaseToPartnerCommentsMap.put(a.Case__c, new List<Partner_Case_Comments__c>() );
            CaseToPartnerCommentsMap.get(a.Case__c).add(a);
        }
        else{
            CaseToPartnerCommentsMap.get(a.Case__c).add(a);
        }
    }

    List<PartnerNetworkRecordConnection> pnrcList = [SELECT Id, ConnectionId, Status, LocalRecordId, ParentRecordId, PartnerRecordId, 
                                                        StartDate, EndDate, SendEmails, SendOpenTasks, SendClosedTasks, RelatedRecords 
                                                        FROM PartnerNetworkRecordConnection
                                                        WHERE LocalRecordId IN: CaseToPartnerCommentsMap.keySet()];
    
    List<PartnerNetworkRecordConnection> newConnectionList = new List<PartnerNetworkRecordConnection>();
    for(PartnerNetworkRecordConnection pnrc: pnrcList){
        if(pnrc.Status != 'Inactive'){
            for(Partner_Case_Comments__c pc: CaseToPartnerCommentsMap.get(pnrc.LocalRecordId)){
                PartnerNetworkRecordConnection newConnection = new PartnerNetworkRecordConnection();
                newConnection.LocalRecordId = pc.Id;
                newConnection.ConnectionId = pnrc.ConnectionId;
                newConnection.ParentRecordId = pc.Case__c;
                newConnectionList.add(newConnection);
            }
        }
    }
    system.debug(newConnectionList);
    if(newConnectionList.size() > 0 && !Test.isRunningTest()){
        insert newConnectionList;
    }
}