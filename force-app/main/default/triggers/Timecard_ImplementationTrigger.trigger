trigger Timecard_ImplementationTrigger on Service_Timecard__c (before insert) {
    
    //[JGP 9/14/2012] Service Time card and Implementation Object custom mappings
    //REQUIRES Class: Timecard_ImplementationTEST for testing of this trigger
    //Expected behavior when a new Timecard record is created
      //1. Populate the Last Timecard Date field on the related ImpCert record with the Timecard date field information.
      //2. Populate the Account field on the Timecard record with the Account ID from the related ImpCert record's lookup Account field.
   
   
    //Set to hold Implementation Certs to Query
    Set<Id> implementationCerts = new Set<Id>();
    //Map to hold the ImplementationCerts
    Map<Id,Implementation_Certification__c> ImplementationCertToTimeCard = new Map<Id,Implementation_Certification__c>();
    
    for (Service_Timecard__c timecard: trigger.new)
    {
        //Populate the Timecard with the Account information tied to the Implementation Object
        if (timecard.Implementation_Certification__c != null)
            if (!implementationCerts.contains(timecard.Implementation_Certification__c))
                implementationCerts.add(timecard.Implementation_Certification__c);
    }
    //Perform a single Query for all needed implementationCerts
    if (!implementationCerts.isEmpty())
    {
        ImplementationCertToTimeCard = new Map<Id,Implementation_Certification__c>([SELECT 
        Implementation_Certification__c.Id,
        Implementation_Certification__c.Last_Timecard_Date__c,
        Implementation_Certification__c.Account__c
        FROM Implementation_Certification__c 
        WHERE Implementation_Certification__c.Id IN :implementationCerts]);
    }
    if (!ImplementationCertToTimeCard.isEmpty())
    {
        for (Service_Timecard__c timecard: trigger.new)
        {
            //For Each Timecard get the Associated ImplementationCert
            Implementation_Certification__c workingImplementation = ImplementationCertToTimeCard.get(timecard.Implementation_Certification__c);
            if (workingImplementation != null)
            {
                //Set the Account Values on the Timecard
                timecard.Account__c = workingImplementation.Account__c;
                //Update the Last Time Card Entered on the Implementation Cert Object
                workingImplementation.Last_Timecard_Date__c = timecard.Timecard_Date__c;
                //Update the Implementation Cert with the new Time on the Map for later updating
                ImplementationCertToTimeCard.put(workingImplementation.Id,workingImplementation);
            }
        }
        //Perform DML Update of the Implementation Cert with the new Timecard Value
        update(ImplementationCertToTimeCard.values());
    }
}