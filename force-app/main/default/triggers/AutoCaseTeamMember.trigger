trigger AutoCaseTeamMember on Case_Team_Members__c (after insert, before delete) {
//Enable bulk processing
    if (trigger.isInsert)
    {
        Case_Team_Members__c[] ctm = trigger.new;
        AutoCaseTeamMember.InsertNew(ctm);
    }
    if (trigger.isDelete)
    {
        Case_Team_Members__c[] ctm = trigger.old;
        AutoCaseTeamMember.CleanUp(ctm);
    }
}