trigger Messagetransactiontrigger on Messages_Transactions__c (after insert, after delete, after update) {
    //Project_Name__c   
    Set<String> projectIds = new Set<String>();
    for (Messages_Transactions__c mt: (trigger.isDelete ? trigger.old : trigger.new)) {
        projectIds.add(mt.Project_Name__c);
    }
    

    //pse__Proj__c
    
    List<pse__Proj__c> projects = new List<pse__proj__c>();
    for ( pse__Proj__c pro: [select id, Attachment_Types_Receive__c,Attachment_Types_Send1__c,Connectivity1__c,Delivery_Option1__c,Imp_Guides__c,Imp_Guide_2__c,
    Directory_Message_Type1__c,Message_Versions__c,On_Ramp__c,Transactions__c,
    (select id, name,Attachment_Types_Receive__c,Attachment_Types_Send__c,Connectivity__c,Delivery_Option__c,
                                         IG_Version__c,IG_Version_2__c,Message_Type__c,Message_Version__c,On_Ramp__c,Transactions__c,Project_Name__c
                                         from Messages_Transactions__r )
     from pse__Proj__c where id in : projectIds]) {
        
        string Attachment_Types_Receive =  null;
        string Attachment_Types_Send =  null;
        string Connectivity =  null;
        string Delivery_Option =  null;
        string IG_Version =  null;
        string IG_Version_2 =  null;
        string Message_Type =  null;
        string On_Ramp =  null;
        string Transactions =  null;
        string Message_Version = null;
        
        Set<String> attTypeSend = new Set<String>();
        Set<String> attTypeReceive = new Set<String>();
        
        for (Messages_transactions__c mt: pro.Messages_Transactions__r) {
            
             if (mt.Attachment_Types_Receive__c != null) {
                for (string atr : mt.Attachment_Types_Receive__c.split(';')) {
                     if (!attTypeReceive.contains(atr)) {
                         attTypeReceive.add(atr);
                     }
                }
                
            }    
            
            if (mt.Attachment_Types_Send__c != null) {
                for (string ats : mt.Attachment_Types_Send__c.split(';')) {
                     
                     if (!attTypeSend.contains(ats)) {
                         attTypeSend.add(ats);
                     } 
                     
                     
                }
                
            }  
            
            
            if (mt.Connectivity__c != null) {
                if (Connectivity == null) 
                    Connectivity = mt.Connectivity__c ;
                else {
                    if (!Connectivity.contains(mt.Connectivity__c))
                        Connectivity += ','+ mt.Connectivity__c ; 
                }
            }
            
            if (mt.Delivery_Option__c != null) {    
                if (Delivery_Option == null) 
                    Delivery_Option = mt.Delivery_Option__c ;
                else {
                    if (!Delivery_Option.contains(mt.Delivery_Option__c))
                        Delivery_Option += ','+ mt.Delivery_Option__c ; 
                }
            }
            
            if (mt.IG_Version__c != null) {    
                if (IG_Version == null) 
                    IG_Version = mt.IG_Version__c ;
                else {
                    if (!IG_Version.contains(mt.IG_Version__c))
                        IG_Version += ','+ mt.IG_Version__c ; 
                }
            }
            
            
            if (mt.IG_Version_2__c != null) {    
                if (IG_Version_2 == null) 
                    IG_Version_2 = mt.IG_Version_2__c ;
                else {
                    if (!IG_Version_2.contains(mt.IG_Version_2__c))
                        IG_Version_2 += ','+ mt.IG_Version_2__c ; 
                }
            }
            
            if (mt.Message_Type__c != null) {    
                if (Message_Type == null) 
                    Message_Type = mt.Message_Type__c ;
                else {
                    if (!Message_Type.contains(mt.Message_Type__c))
                        Message_Type += ','+ mt.Message_Type__c ; 
                }
            }
            
            if (mt.Message_Version__c != null) {    
                if (Message_Version == null) 
                    Message_Version = mt.Message_Version__c ;
                else {
                    if (!Message_Version.contains(mt.Message_Version__c))
                        Message_Version += ','+ mt.Message_Version__c ;     
                }
            }
            
            if (mt.On_Ramp__c != null) {    
                if (On_Ramp == null) 
                    On_Ramp = mt.On_Ramp__c ;
                else {
                    if (!On_Ramp.contains(mt.On_Ramp__c))
                        On_Ramp += ','+ mt.On_Ramp__c ;  
                }
            }    
            
            if (mt.Transactions__c != null) {                
                if (Transactions == null) 
                    Transactions = mt.Transactions__c ;
                else {
                    if (!Transactions.contains(mt.Transactions__c))
                        Transactions += ','+ mt.Transactions__c ;                         
                }       
            }
        }
        
        
        for (string s: attTypeReceive) {
            if (Attachment_Types_Receive == null) {
                Attachment_Types_Receive = s;
             }
             else {
                
                    Attachment_Types_Receive += ','+ s; 
             }
        }
        //system.assert(False,attTypeSend);
        
        for (string s: attTypeSend) {
            if (Attachment_Types_Send == null) {
                Attachment_Types_Send = s;
             }
             else {
                 
                    Attachment_Types_Send += ','+ s; 
             }
        }
        
        projects.add(new pse__proj__c(id = pro.id, Attachment_Types_Receive__c = Attachment_Types_Receive,
        Attachment_Types_Send1__c = Attachment_Types_Send,
        Connectivity1__c = Connectivity,
        Delivery_Option1__c = Delivery_Option,
        Imp_Guides__c = IG_Version,
        Imp_Guide_2__c = IG_Version_2,
        Directory_Message_Type1__c = Message_Type ,
        Message_Versions__c = Message_Version,
        On_Ramp__c = On_Ramp,
        Transactions__c = Transactions));
    }
    
    update projects;
    
    
    
}