trigger LeadTrigger on Lead (After Update) {
    
    if(Trigger.isAfter && Trigger.isUpdate) {
        LeadTriggerHandler.updateConvertedLeadsContracts(Trigger.new);
    }

}