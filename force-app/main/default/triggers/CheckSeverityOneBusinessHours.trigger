/*
    05/04/15    SB @ IC    00114842
*/
trigger CheckSeverityOneBusinessHours on Case (before insert,before update) {
    try {
        list<Case> severityOne = new list<Case>{};
        for (Case cs : Trigger.new) {
            if (!cs.IsClosed && cs.Priority == 'Severity 1 (Use sparingly)' && cs.FLAGS__Initial_Response__c == null)
                severityOne.add(cs);
        }
        
        CaseSeverityUtil.checkBusinessHours(severityOne);
    } catch (Exception e) {
        System.debug(e);
    }
}