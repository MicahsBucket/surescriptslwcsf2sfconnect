/****************************************************************************************************
*Description:   	Trigger for Synchronizing Data from Parent to Child Project information
*					pse__Proj__c on way sync to Project_Read_Only__c
*Required Class(es):Sync, Sync_Test
*
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0    	11/20/2014	Justin Padilla      Initial Version
*****************************************************************************************************/
trigger ProjectSyncToProjectReadOnly on pse__Proj__c (before delete, after insert, after update)
{
	//Clean Up Process to remove child objects on delete
	if (trigger.isDelete)
	{
		Set<Id> projIds = new Set<Id>();
		for (pse__Proj__c proj: trigger.oldMap.values())
		{
			projIds.add(proj.Id);
		}
		if (!projIds.isEmpty())
		{
			List<Project_Read_Only__c> toDelete = new List<Project_Read_Only__c>([SELECT Id FROM Project_Read_Only__c WHERE Project__c IN: projIds]);
			if (!toDelete.isEmpty()) delete(toDelete);
		}
	}
	if (trigger.isInsert)
	{
		//Since this is an insert, send all new records to the Sync routine
		Map<Id,pse__Proj__c> toProcess = new Map<Id,pse__Proj__c>();
		for (pse__Proj__c proj: trigger.new)
		{
			toProcess.put(proj.Id,proj);
		}
		if (!toProcess.isEmpty()) Sync.SyncRecords(toProcess);
	}
	if (trigger.isUpdate)
	{
		//Determine if a monitored field on the parent has been updated or not. If one of the fields in the fieldset has changed, update child records
		Map<Id,pse__Proj__c> toProcess = new Map<Id,pse__Proj__c>();
		Set<String> monitoredFieldNames = Sync.getParentProjectSyncFields();
		//Manually add Account and Opportunity to the monitor
		if (!monitoredFieldNames.contains('pse__Account__c')) monitoredFieldNames.add('pse__Account__c');
		if (!monitoredFieldNames.contains('pse__Opportunity__c')) monitoredFieldNames.add('pse__Opportunity__c');
		for (pse__Proj__c proj: trigger.new)
		{
			for (string s: monitoredFieldNames)
			{
				if (trigger.oldMap.get(proj.Id).get(s) != proj.get(s))
				{
					//A change has occured, add it to the processing list and send to Sync class
					toProcess.put(proj.Id,proj);
					break; //stop evaluating fields, at least one has already been found
				}
			}			
		}
		system.debug('ProjectSyncToProjectReadOnly - Update - toProcess'+toProcess);
		if (!toProcess.isEmpty()) Sync.SyncRecords(toProcess);
		
	}
}