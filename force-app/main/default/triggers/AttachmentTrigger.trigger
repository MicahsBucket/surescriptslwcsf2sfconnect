trigger AttachmentTrigger on Attachment (after insert, after update) {
    if(Trigger.isAfter && Trigger.isInsert){
        map<Id, List<Attachment>> AttachementMap = new map<Id, List<Attachment>>();
        for(Attachment a: Trigger.new){
            system.debug(a);
            if(!AttachementMap.containskey(a.ParentId)){
                AttachementMap.put(a.ParentId, new List<Attachment>());
                AttachementMap.get(a.ParentId).add(a);
            }
            else{
                AttachementMap.get(a.ParentId).add(a);
            }
        }
    
        List<PartnerNetworkRecordConnection> pnrcList = [SELECT Id, ConnectionId, Status, LocalRecordId, ParentRecordId, PartnerRecordId, 
                                                            StartDate, EndDate, SendEmails, SendOpenTasks, SendClosedTasks, RelatedRecords 
                                                            FROM PartnerNetworkRecordConnection
                                                            WHERE LocalRecordId IN: AttachementMap.keySet()];
                                                            
        List<PartnerNetworkRecordConnection> newConnectionList = new List<PartnerNetworkRecordConnection>();
        for(PartnerNetworkRecordConnection p: pnrcList){
            System.debug(p);
            if(p.Status != 'Inactive' || Test.isRunningTest()){
                system.debug(AttachementMap.get(p.LocalRecordId));
                for(Attachment att: AttachementMap.get(p.LocalRecordId)){
                    PartnerNetworkRecordConnection newConnection = new PartnerNetworkRecordConnection();
                    newConnection.LocalRecordId = att.Id;
                    newConnection.ConnectionId = p.ConnectionId;
                    newConnection.ParentRecordId = att.ParentId;
                    newConnectionList.add(newConnection);
                }
            }
        }     
        system.debug(newConnectionList);
        if(!Test.isRunningTest()){
            insert newConnectionList;
        }
        
    }
    
    // if(Trigger.isAfter && Trigger.isUpdate){
        // set<Id> attachementIds = new set<Id>();
        // for(Attachment a: Trigger.New){
        //     system.debug('a.IsPartnerShared' + a.IsPartnerShared); 
        //     if(a.IsPartnerShared == false){
        //         attachementIds.add(a.Id);
        //     }
        // }
        // List<Attachment> attachementsToDelete = new List<Attachment>();
        // if(!attachementIds.isEmpty()){
        //     List<Attachment> attList = [SELECT Id, CreatedBy.Name FROM Attachment WHERE Id IN: attachementIds];
        //     for(Attachment a: attList){
        //         if(a.CreatedBy.Name == 'Connection User'){
        //             attachementsToDelete.add(a);
        //         }
        //     }
        // }
    
        // if(attachementsToDelete.size() > 0){
        //     delete attachementsToDelete;
        // }
    // }
}