trigger Timecard_Rollup_Calculations_Trigger on Service_Timecard__c (after delete, after insert, after undelete, 
after update) {
	/****************************************************************************************************
	*Description:	Calculates the Billable and non-Billable Hour(s) for all Time Cards associated to a 
	*				particular ImpCert based on Service_Timecard__c.Billable_Status__c (billable/non-billable)
	*				and Service_Timecard__c.Effort_Hours__c then rolls up to the 
	*				Implementation_Certification__c.Total_Billable_Time__c or 
	*				Implementation_Certification__c.Total_Non_Billable_Time__c respectively
	*
	*Test Class:	Timecard_Rollup_Calculations_TEST
	*
	*Revision	|Date		|Author				|AdditionalNotes
	*====================================================================================================
	*1.0		3/15/2013	Justin Padilla		WO#1268 Initial Version
	*****************************************************************************************************/
	//Set of Implementation_Certification__c Ids to collect timecards for
	Set<Id> impCertIds = new Set<Id>();
	if (!trigger.isDelete)
	{
		for (Service_Timecard__c stc:trigger.new)
		{
			if (stc.Implementation_Certification__c != null && !impCertIds.contains(stc.Implementation_Certification__c))
				impCertIds.add(stc.Implementation_Certification__c);
		}
	}
	else //Is Delete Gather Id from the old value (new no longer exists)
	{
		for (Service_Timecard__c stc:trigger.old)
		{
			if (stc.Implementation_Certification__c != null && !impCertIds.contains(stc.Implementation_Certification__c))
				impCertIds.add(stc.Implementation_Certification__c);
		}
	}
	//Map to hold the Implemenation Certifications
	Map<Id,Implementation_Certification__c> impCerts = new Map<Id,Implementation_Certification__c>(
	[SELECT
	Implementation_Certification__c.Id,
	Implementation_Certification__c.Total_Billable_Time__c,
	Implementation_Certification__c.Total_Non_Billable_Time__c
	FROM Implementation_Certification__c
	WHERE Implementation_Certification__c.Id IN:impCertIds]);
	if (impCerts != null && !impCerts.isEmpty())
	{
		//Reset all values to Zero
		for (Implementation_Certification__c ic:impCerts.values())
		{
			ic.Total_Billable_Time__c = 0;
			ic.Total_Non_Billable_Time__c = 0;
		}
		//Now that we have all of the Implemenation Certifications retieve all of the timecards
		List<Service_Timecard__c> timecards = new List<Service_Timecard__c>();
		timecards = [SELECT
		Service_Timecard__c.Id,
		Service_Timecard__c.Billable_Status__c,
		Service_Timecard__c.Effort_Hours__c,
		Service_Timecard__c.Implementation_Certification__c
		FROM Service_Timecard__c WHERE Service_Timecard__c.Implementation_Certification__c IN:impCertIds];
		if (!timecards.isEmpty())
		{
			for (Service_Timecard__c stc:timecards)
			{
				//Get the Implemenation Certification
				Implementation_Certification__c workingImpCert = impCerts.get(stc.Implementation_Certification__c);
				if (workingImpCert != null)
				{
					if (stc.Billable_Status__c && stc.Effort_Hours__c != null) //Billable
						workingImpCert.Total_Billable_Time__c += stc.Effort_Hours__c;
					else if (!stc.Billable_Status__c && stc.Effort_Hours__c != null)//Non Billable
						workingImpCert.Total_Non_Billable_Time__c += stc.Effort_Hours__c;
					//Replace Map value with the new Values
					impCerts.remove(stc.Implementation_Certification__c);
					impCerts.put(stc.Implementation_Certification__c,workingImpCert);
				}
			}
		}
		//Update all of the	Implemenation Certifications
		update(impCerts.values());	
	}
}