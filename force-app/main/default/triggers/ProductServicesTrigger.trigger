//grabbing this multipicklist field to be filled out by requestor. should this be required? Surescripts_Products_Services__c
    //Only created for a new contract with an associated quote
    // [SELECT Id, SBQQ__ProductName__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Optional__c != TRUE //use products loopip to product rollup(Product_Rollup__c) ****Can't find Product_Rollup__c field **** ];


trigger ProductServicesTrigger on Contract (after update) {
  
  if (Trigger.isAfter && Trigger.isUpdate) {
        ProductServicesTriggerHandler.updateProductServicesRecord(Trigger.new, Trigger.oldMap);
    }
}