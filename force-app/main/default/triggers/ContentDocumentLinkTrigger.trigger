trigger ContentDocumentLinkTrigger on ContentDocumentLink (after insert) {
    map<Id, List<ContentDocument>> ContentDocumentIdToContentDocumentMap = new map<Id, List<ContentDocument>>();
    map<Id, List<PartnerNetworkRecordConnection>> LinkedEntityIdToConnection = new map<Id, List<PartnerNetworkRecordConnection>>();
    for(ContentDocumentLink a: Trigger.new){
        LinkedEntityIdToConnection.put(a.LinkedEntityId, new List<PartnerNetworkRecordConnection>());
        ContentDocumentIdToContentDocumentMap.put(a.ContentDocumentId, new List<ContentDocument>() );
    }

    List<PartnerNetworkRecordConnection> pnrcList = [SELECT Id, ConnectionId, Status, LocalRecordId, ParentRecordId, PartnerRecordId, 
                                                        StartDate, EndDate, SendEmails, SendOpenTasks, SendClosedTasks, RelatedRecords 
                                                        FROM PartnerNetworkRecordConnection
                                                        WHERE LocalRecordId IN: LinkedEntityIdToConnection.keySet()];
    
    if(pnrcList.size() > 0){
        for(PartnerNetworkRecordConnection p: pnrcList){
            LinkedEntityIdToConnection.get(p.LocalRecordId).add(p);
        }
    }
    System.debug(LinkedEntityIdToConnection);
    List<ContentDocument> cdList = [SELECT Id, FileType, ContentAssetId, SharingOption, FileExtension, Description, LatestPublishedVersionId, 
                                    LatestPublishedVersion.VersionData, PublishStatus, Title, ContentSize
                                    FROM ContentDocument
                                    WHERE Id IN: ContentDocumentIdToContentDocumentMap.keyset() ];
    if(cdList.size() > 0){
        for(ContentDocument cd: cdList){
            ContentDocumentIdToContentDocumentMap.get(cd.Id).add(cd);
        }
    }
    
    System.debug(ContentDocumentIdToContentDocumentMap);
    List<PartnerNetworkRecordConnection> newConnectionList = new List<PartnerNetworkRecordConnection>();
    list<Attachment> AttachementsToInsert = new list<Attachment>();
    if(!ContentDocumentIdToContentDocumentMap.isEmpty()){
        for(ContentDocumentLink a: Trigger.new){
            for(PartnerNetworkRecordConnection linkedEntity: LinkedEntityIdToConnection.get(a.LinkedEntityId)){
                if(linkedEntity.Status != 'Inactive' || Test.isRunningTest()){
                    for(ContentDocument cd: ContentDocumentIdToContentDocumentMap.get(a.ContentDocumentId)){
                        System.debug('Document'+ cd);
                        Attachment at = new Attachment();
                        at.ParentId = a.LinkedEntityId;
                        at.Name = cd.Title + '.'+ cd.FileExtension;
                        at.ContentType = SF2SFConnectorUtility.GetMIMEType(cd.FileType);
                        at.Description = cd.Title;
                        at.Body = cd.LatestPublishedVersion.VersionData;
                        AttachementsToInsert.add(at);
                    }
                }
            } 
        }
    }
    system.debug('AttachementsToInsert: '+AttachementsToInsert);
    if(AttachementsToInsert.size() > 0 && !Test.isRunningTest()){
        insert AttachementsToInsert;
    }
}