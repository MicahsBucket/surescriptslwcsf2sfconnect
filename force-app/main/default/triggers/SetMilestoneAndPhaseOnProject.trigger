/*
  Purpose:
        This trigger sets the Current Milestone & Phase on the project
        
  Initiative: IconATG FinancialForce PSA Implementation 
  Author:     William Rich
  Company:    IconATG
  Contact:    william.rich@iconatg.com
  Created:    5/21/2014
*/

trigger SetMilestoneAndPhaseOnProject on pse__Milestone__c (after update) {
    Set<Id> projectIds = new Set<Id>();
    for (pse__Milestone__c newMilestone : Trigger.new) {
        pse__Milestone__c oldMilestone = Trigger.oldMap.get(newMilestone.Id);
        if (newMilestone.pse__Status__c != null && newMilestone.pse__Status__c.equals('Open')) {
            if (oldMilestone.pse__Status__c == null || !oldMilestone.pse__Status__c.equals('Open')) {
                if (newMilestone.pse__Project__c != null) {
                    projectIds.add(newMilestone.pse__Project__c);
                }
            }
            
        }
        
    }
    
    if (!projectIds.isEmpty()) {
        Map<Id, pse__Proj__c> projectMap = new Map<Id, pse__Proj__c>([
            select
                Id,
                Current_Milestone__c,
                Current_Phase__c
            from pse__Proj__c
            where Id in :projectIds
        ]);
        
        List<pse__Proj__c> projects = new List<pse__Proj__c>();
        for (pse__Milestone__c milestone : Trigger.new) {
            if (milestone.pse__Project__c != null) {
                if (projectMap.containsKey(milestone.pse__Project__c)) {
                    pse__Proj__c project = projectMap.get(milestone.pse__Project__c);
                    project.Current_Milestone__c = milestone.Id;
                    project.Current_Phase__c = milestone.Related_Phase__c;
                    projects.add(project);
                }
            }
        }
        
        if (!projects.isEmpty()) update projects;
    }
}