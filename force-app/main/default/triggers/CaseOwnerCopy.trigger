//Trigger copies the Case OwnerID from the Case 'Owner' standard field to the 'Case Owner Copy' custom lookup field to the User object. This allows for creation of cross-object formula fields from the User record of the Case Owner to the Case object records.

trigger CaseOwnerCopy on Case (before update, before insert) {
 
  // When 'Owner' field is changed, update 'CaseOwnerCopy' too
 
 User[] users = [select Id from User where Isactive= True limit 2];
 
    // Loop through the incoming records
    for (Case x : Trigger.new) {
 
        // Has Owner changed?
        if (x.OwnerID != x.Case_Owner_Copy__c) {
            x.Case_Owner_Copy__c = x.OwnerId;
 
         // check that owner is a user (not a queue)
        if( ((String)x.OwnerId).substring(0,3) == '005' ){
            x.Case_Owner_Copy__c = x.OwnerId;
        }
        else{
            // in case of Queue we clear out our copy field
            x.Case_Owner_Copy__c = null;

 }
        }
    }
}