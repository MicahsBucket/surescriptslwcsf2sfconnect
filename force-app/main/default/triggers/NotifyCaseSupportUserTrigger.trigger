trigger NotifyCaseSupportUserTrigger on CaseComment ( after insert, after update) {

String[] toAddresses;
Messaging.SingleEmailMessage mail;

for(CaseComment curCC : Trigger.new){
    //System.debug('sonaal123'+curCC.parent.Email_address__c);
    //if(curCC.parent.Email_address__c!=null && curCC.IsPublished){
    for(CaseComment cc:[select ParentId, CommentBody, parent.Email_address__c,parent.caseNumber from CaseComment where id = :curCC.Id and IsPublished= true])
    {        
       try{ 
           if(cc.parent.Email_address__c!=null) {
               User u = [select name from User where id =: curCC.CreatedById limit 1];
               Case c = [select description, subject, CaseNumber, User_Reporting_Case__c, User_Case__c from case where id =: cc.ParentId];
               toAddresses = new String[] {cc.parent.Email_address__c}; 
               //System.debug('sonaal123'+cc.parent.Email_address__c);
               mail = new Messaging.SingleEmailMessage();
               mail.setToAddresses(toAddresses);
               mail.setSubject('Case #' + c.CaseNumber  + ': A New Case Comment Has Been Added/Updated');
               
               /* string msg = 'A Comment has been added/updated to a Case#'+cc.parent.caseNumber+' that you had supported. <br>  The Comment was added/Updated by UserID: ';
               msg = msg + u.name;
               msg = msg + '<br>' + cc.CommentBody;
               */
               
                String msg1 = 'Dear Surescripts Support <br>';
                msg1 = msg1 + 'Support User Reporting Case:' + c.User_Reporting_Case__c + '<br>';
                if(c.User_Case__c!=null){
                msg1 = msg1 + '<br>Support User Internal Ticket#:' + c.User_Case__c + '<br>'; 
                }else{
                     msg1 = msg1 + '<br>Support User Internal Ticket#: Not Available<br>';    
                }
                msg1 = msg1 + '<br>Case# ' + c.CaseNumber + ': ' + c.subject + ' has been updated with a new comment.<br><br>';

                msg1 = msg1 + 'COMMENT:' + cc.CommentBody + '<br><br>';  

                msg1 = msg1 + 'Initial Case Description:' + '<br><br>';
                msg1 = msg1 + c.Description + '<br><br>';

                msg1 = msg1 + 'If you have any questions or concerns regarding this comment, please reply to this email or send an inquiry to support@surescripts.com. <br>Thank you,<br>Surescripts Customer Care.';

               mail.setHtmlBody(msg1);    
               Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
               //System.debug('sonaal123'+mail);
              }
          }
          catch(Exception ex)
          {
          System.debug('Exception occurs in trigger : NotifyCaseSupportUser .'+ex.getMessage() );
          }     
     }      
        
  }
}