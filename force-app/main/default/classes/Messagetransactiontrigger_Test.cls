@isTest
private class Messagetransactiontrigger_Test{
static testMethod void testMe(){
    
    Account a = new Account();
    a.Name = 'testAccName';
    insert a;
    
    Contact c = new Contact();
    c.LastName = 'testLName';
    c.AccountId = a.Id;
    insert c;
    
    Opportunity opp = new Opportunity();
    opp.Name = 'testOppName';
    opp.AccountId = a.Id;
    opp.StageName = '1_Planning';
    opp.CloseDate = System.today().addDays(5);
    opp.Type = 'Clinical Messaging (CI)';
    opp.Project_Product__c = 'Directory';
    insert opp;
    
    Contact cPm = new Contact();
    cPm.LastName = 'TestProjectManager';
    insert cPm;
    
    pse__Proj__c pr=new pse__Proj__c();
    pr.pse__Opportunity__c = opp.Id;
    pr.Attachment_Types_Receive__c='CCD';
    pr.Attachment_Types_Send1__c='CCR';
    pr.Connectivity1__c='Send Only';
    pr.Delivery_Option1__c='Standard';
    pr.Imp_Guides__c='abc';
    pr.Directory_Message_Type1__c='msg';
    pr.Message_Versions__c='ghf';
    pr.On_Ramp__c='adi';
    pr.Transactions__c='hhhhh';
    pr.Name = 'testProjName';
    pr.pse__Account__c = a.Id;
    pr.pse__Start_Date__c = System.today();
    pr.pse__End_Date__c = System.today().addDays(2);
    pr.Project_Classification__c = 'New Project';
    pr.Value_Add_Reason__c = 'Core';
    pr.pse__Project_Status__c = 'Red';
    pr.Executive_Summary__c = 'Customer - Resource Constraint';
    pr.Customer_Contact_1__c = c.Id;
    pr.pse__Project_Manager__c = cPm.Id;
    insert pr;

    
    Messages_Transactions__c mt = new Messages_Transactions__c();
    mt.Message_Type__c = 'Routing';
    mt.Project_Name__c = pr.Id;
    mt.Attachment_Types_Receive__c = 'CCD;CCDA';
    mt.Attachment_Types_Send__c = 'CCDA;CCR';
    mt.Connectivity__c = 'None';
    mt.Delivery_Option__c = 'Standard';
    mt.IG_Version__c = 'Directories 4.6 IG 2015-10-14';
    mt.Message_Version__c = '10.6';
    mt.On_Ramp__c = 'XDR';
    mt.Transactions__c = 'NEW';
    mt.ACR__c = 'ACR 2013-05-01';
    
    insert mt;
    
    mt.Message_Type__c = 'Benefit';
    update mt;
    
    delete mt;
   
}
}