@isTest
public class SfdcS2SMocks {
    public class SfdcS2SOAuthMock implements System.StubProvider {
        public Object handleMethodCall(Object stubbedObject, String stubbedMethodName,
                                       Type returnType, List<Type> listOfParamTypes, 
                                       List<String> listOfParamNames,
                                       List<Object> listOfArgs) {                                                                    
            return null;                                    
        }
    }
    
    public class SfdcS2SRemoteCalloutUtilMock implements System.StubProvider {
        public Object handleMethodCall(Object stubbedObject, String stubbedMethodName,
                                       Type returnType, List<Type> listOfParamTypes, 
                                       List<String> listOfParamNames,
                                       List<Object> listOfArgs) { 
                                           
       		if (stubbedMethodName.equals('executeRemoteQuery')) {
            	List<Object> records = new List<Object>();
                Map<String, Object> record = new Map<String, Object>();
                record.put('Body', 'abc');
                record.put('isRichText', false);
                records.add(record);
                return records;
            }                            
            return null;                                    
        }
    }
    

}