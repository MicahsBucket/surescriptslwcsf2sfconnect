public class SfdcS2SOAuth {
    
  public String id{get;set;}
    public String issued_at{get;set;}
    public String instance_url{get;set;}
    public String signature{get;set;}
    public String access_token{get;set;}  
    public String refresh_token{get;set;} 
    public String callbackResponse{get;set;}
        
   
    public SfdcS2SOAuth postTokenEndpoint(String url, String body, boolean doUpdate) {
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url+'/services/oauth2/token');
        req.setMethod('POST');
        req.setBody(body);

        Http h = new Http();
        Integer statusCode = 200;
        if (doCallout) {
           HttpResponse res = h.send(req); 
           callbackResponse = res.getBody(); 
           statusCode = res.getStatusCode();
        } 
        
        SfdcS2SOAuth oauth = null;
        if (statusCode == 200) {
            oauth = deserialize(callbackResponse);
            if (doUpdate) {
               updateTokens(oauth); 
            }
        }
        
        return oauth;
    }  
    
    //can't do actual callout while running tests
    @TestVisible private boolean doCallout = true;
    
    @TestVisible private static SfdcS2SOAuth deserialize(String callbackResponse) {
        return (SfdcS2SOAuth)JSON.deserialize(callbackResponse, SfdcS2SOAuth.class);
    }

    @TestVisible private static void updateTokens(SfdcS2SOAuth oauth) {
              
        SFDC_S2S_OAuth2Tokens__c tokens = SFDC_S2S_OAuth2Tokens__c.getOrgDefaults();
        if (oauth.refresh_token != null) {
            //refresh token flow does not return refresh token in the response
            //so make sure we don't null out the valid value
            tokens.RefreshToken__c = oauth.refresh_token;
            tokens.InstanceUrl__c = oauth.instance_url;
        	upsert tokens;
        }
       
    }    
}