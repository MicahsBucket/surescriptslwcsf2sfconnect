/**
 * RESTRequest
 * @description Generic class providing methods for issuing RESTful request to an external service.
 * @author  James Loghry
 * @date  4/13/2016
 */
public class RESTRequest{

    public static String sendRequest(String body, String method, String httpPath){
		Http h = new Http();
		HttpRequest req = new HttpRequest();
		req.setHeader('Content-Type','application/json');
		req.setEndpoint('callout:CPI_Service'+httpPath);

		if(!String.isEmpty(body)){
			req.setBody(body);
		}
		req.setMethod(method);

		//Adjust the timeout (currently save metrics is taking a longtime on an initial save.)
		req.setTimeout(120000);

		System.debug('RESTRequest Endpoint: ' + req.getEndpoint());
		System.debug('RESTRequest Method: ' + req.getMethod());
		System.debug('RESTRequest Body: ' + req.getBody());

		HttpResponse response = h.send(req);

		if(response.getStatusCode() < 200 || response.getStatusCode() > 299){
			String error = response.getBody();
			System.debug('JWL: error: ' + error);
			try{
				ErrorResponse err = (ErrorResponse)JSON.deserialize(error,ErrorResponse.class);
				System.debug('JWL: err: ' + err);
				error = err.message;
			}catch(Exception e){
			}
			throw new CPIException('An error occurred with a callout: ' + error);
		}

        return response.getBody();
	}

	public static String sendGET(String httpPath){
		return sendRequest(null,'GET',httpPath);
	}

	public static String sendPOST(String httpPath, String body){
		return sendRequest(body,'POST',httpPath);
	}

	public class ErrorResponse{
		public String message {get; set;}
	}
}