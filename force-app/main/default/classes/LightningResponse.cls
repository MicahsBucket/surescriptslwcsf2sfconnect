/**
 * LightningResponse
 * @description A wrapper class for any Apex that is called through Lightning Components.
 * Used for properly reporting success and error conditions (typically errors will result in ISE / Gacks, which are not helpful.)
 * @author  Demand Chain Systems (James Loghry)
 * @serialDate  4/10/2017
 */
public class LightningResponse {

	@AuraEnabled
    public String jsonResponse {get;set;}
    @AuraEnabled
    public List<String> errors {get;set;}
    @AuraEnabled
    public String state {get;set;}

    public LightningResponse() {
        this.errors = new List<String>();
        this.state = 'SUCCESS';
    }

    public LightningResponse(Exception e){
    	this();
    	if(e != null){
    		this.state = 'ERROR';
    		this.errors.add(e.getMessage());
            System.debug('Exception:  ' + e.getMessage());
            System.debug('Exception stack trace: ' + e.getStackTraceString());
    	}
    }
}