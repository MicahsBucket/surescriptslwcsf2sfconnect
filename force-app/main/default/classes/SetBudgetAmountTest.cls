/*
  Purpose:
        The test class for the SetBudgetAmountController
        
  Initiative: IconATG FinancialForce PSA Implementation 
  Author:     William Rich
  Company:    IconATG
  Contact:    william.rich@iconatg.com
  Created:    6/31/2014
*/

@isTest(seeAllData=true)
private class SetBudgetAmountTest {

    static testMethod void testBudgetAmount() {
        
        pse__Proj__c project = createProject();
        
        ApexPages.currentPage().getParameters().put('id', project.Id);
        ApexPages.StandardController stdController = new ApexPages.StandardController(project);
                
        SetBudgetAmountController baController = new SetBudgetAmountController(stdController); 
        baController.setBudgetAmount();
        
        // Has message about no budgets and milestones
        List<ApexPages.Message> messages = ApexPages.getMessages();
        System.assertEquals(1, messages.size());
        
        pse__Budget__c budget = createBudget(project);
        baController.setBudgetAmount();
        
        // Has additional message about no milestones
        messages = ApexPages.getMessages();
        System.assertEquals(2, messages.size());

        pse__Milestone__c milestone = createMilestone(project);
        baController.setBudgetAmount();
        
        pse__Budget__c budget_1 = [
            select pse__Amount__c
            from pse__Budget__c
            where Id = :budget.Id
        ];
        
        System.assertEquals(milestone.pse__Milestone_Amount__c, budget_1.pse__Amount__c);
        
        pse__Proj__c project_1 = [
            select pse__Planned_Hours__c
            from pse__Proj__c
            where Id = :project.Id
        ];
        
        System.assertEquals(milestone.pse__Planned_Hours__c, project_1.pse__Planned_Hours__c);
        
        baController.cancel();
    }
    
    static pse__Proj__c createProject() {
        pse__Proj__c project = new pse__Proj__c(
            Name='Test Project',
            pse__Is_Active__c=true,
            pse__Is_Billable__c=true,
            pse__Start_Date__c = Date.today().toStartOfWeek(),
            pse__End_Date__c = Date.today().toStartOfWeek().addMonths(6)
        );
        
        insert project;
        return project;
    }
    
    private static pse__Budget__c createBudget(pse__Proj__c project) {
        pse__Budget__c budget = new pse__Budget__c(
            Name = 'Test Budget',
            pse__Effective_Date__c = Date.today(),
            pse__Amount__c = 200.0,
            pse__Type__c = 'Customer Purchase Order',
            pse__Project__c = project.Id
        );
        insert budget;
        return budget;
    }
    
    private static pse__Milestone__c createMilestone(pse__Proj__c project) {
        pse__Milestone__c milestone = new pse__Milestone__c(
            Name = 'Test Milestone',
            pse__Target_Date__c = project.pse__End_Date__c,
            pse__Project__c = project.Id,
            pse__Milestone_Amount__c = 100.0,
            pse__Planned_Hours__c = 40.0
        );
        insert milestone;
        return milestone;
    }
}