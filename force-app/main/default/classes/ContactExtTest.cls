/*
	12/21/2016  MD@IC  Created (00140354)
*/
@isTest
private class ContactExtTest{
	
	@isTest static void tester(){
		/*Profile p = [SELECT Id FROM Profile WHERE Name = 'Customer Community Plus POC' LIMIT 1];
		Apex_Code_Settings__c settings = Apex_Code_Settings__c.getOrgDefaults();
		settings.Community_Profile_Ids__c = p.Id;
		upsert settings;*/

		Account a = new Account(
			Name = 'Test Inc.',
			Account_Manager__c = UserInfo.getUserId());
		insert a;

		Test.setCurrentPage(new PageReference('/apex/Contact'));
		ContactExt ext = new ContactExt(new ApexPages.StandardController(new Contact()));
		System.assertEquals('new', ext.mode, 'mode should be "new"');

		ext.con.AccountId = a.Id;

		// errors
		ext.saveAndReview();

		ext.con.FirstName = 'test';
		ext.con.LastName = 'testerson';
		ext.con.Title = 'T';
		ext.con.Email = 'test@ic.com';
		ext.con.Phone = '5555555555';
		ext.con.Compliance_Contact__c = true;
		ext.saveAndReview();
		System.assert(ext.con.Id != null, 'contact was not inserted: ' + ApexPages.getMessages());
		System.assertEquals('view', ext.mode, 'mode should be "view"');

		ext.con.Tier__c = 'Tier 1';
		ext.editContact();
		ext.saveAndReview();
		ext.con.MailingStreet = '2000 Waterview Dr.';
		ext.con.MailingCity = 'Hamilton';
		ext.con.MailingState = 'NJ';
		ext.con.MailingPostalCode = '08691';
		ext.saveAndReview();

		ext.cancelContact();

		//PageReference pgRef = ext.redirect();
	}
	
}