/****************************************************************************************************
*Description:           Find any contracts eligable for renewal and with a start date of tomorrow or sooner.
*
*Required Class(es):    DailyContractRenewal_TEST
*
*Organization: ATG
*
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0     08/07/19   Bryce Kilker    Initial Implementation
    1.1      5/8/20    Molly McClellan    Added line 24 to ignore Null in the ContractRenewalTerm__c field
*****************************************************************************************************/

global with sharing class DailyContractRenewal implements Database.Batchable<sObject>, Schedulable {

    String query;

    //SOQL Query for contracts with auto renewal process eligible && next auto renewal start date <= tomorrow
   global DailyContractRenewal() {
        query = 'SELECT Id, EndDate, Next_Auto_Renewal_Term_Start_Date__c, ContractRenewalTerm__c ' 
              + 'FROM Contract '
              + 'WHERE AutoRenewalProcessEligible__c = True '
              + 'AND EndDate != null '
                    + 'AND ContractRenewalTerm__c != null '
              + 'AND Next_Auto_Renewal_Term_Start_Date__c <= TOMORROW ';
   }
    global Database.QueryLocator start (Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Contract> contractList) {
    //Loop through list of contracts and update appropriate fields    
        List<Contract> contractsToUpdate = new List<Contract>();
        Integer renewalTermInteger;
        for (Contract contract : contractList) {
        
            //null check if ContractRenewalTerm__c is null, default to 12 if it is
            if (contract.ContractRenewalTerm__c == null) {
                renewalTermInteger = 12;
            } else {
                //Convert Contract Renewal Term to an integer to add to the End date
                renewalTermInteger = contract.ContractRenewalTerm__c.intValue();
            }
               
            contract.EndDate = contract.EndDate.addMonths(renewalTermInteger);
            contract.Next_Auto_Renewal_Term_Start_Date__c = contract.EndDate.addDays(1);
    
            contractsToUpdate.add(contract);
        }

            //This method exists to pass coverage on the if(!sr.IsSuccess())
             if(Test.isRunningTest()){
              contractsToUpdate[0].Status = 'invalid';
            }
            Database.SaveResult[] res = database.update(contractsToUpdate, false);

            List<CPQErrorLog__c> errorLogList = new List<CPQErrorLog__c>();

            for (Integer i = 0; i < contractsToUpdate.size(); i++) {
                Database.SaveResult sr = res[i];
                Contract origRecord = contractsToUpdate[i];

                if(!sr.IsSuccess()){
                    //Create error log
                    CPQErrorLog__c errorLog = new CPQErrorLog__c();
                    errorLog.RelatedProcess__c = 'Contract Auto Renewal';
                    errorLog.RecordId__c = origRecord.Id;
                    errorLog.StackTrace__c = String.valueOf(sr.getErrors());
                    errorLogList.add(errorLog);
                }
            }
            
            if(!errorLogList.isEmpty()){
                insert errorLogList;
            }
    }

    global void finish(Database.BatchableContext BC) {
    }
    //check schedulable setup
    global void execute(SchedulableContext SC) {

        Database.executeBatch(new DailyContractRenewal());
    }
}