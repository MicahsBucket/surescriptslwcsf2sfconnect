/****************************************************************************************************
*Description:   	Class for Synchronizing Data from Parent to Child Project information
*					pse__Proj__c on way sync to Project_Read_Only__c
*Required Class(es):Sync_Test
*
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0    	11/20/2014	Justin Padilla      Initial Version
*****************************************************************************************************/
public class Sync
{
	public static void SyncRecords(Map<Id,pse__Proj__c> toProcess)
	{
		Map<Id,List<Project_Read_Only__c>> childSyncRecords = new Map<Id,List<Project_Read_Only__c>>();
		Set<Id> projIds = new Set<Id>();
		for (Id projId: toProcess.keySet())
		{
			projIds.add(projId);
		}
		//Get fields to sync
		Set<String> temp = Sync.getParentProjectSyncFields();
		//Replace any namespaces
		Map<String, String> parentSyncFieldMapping = new Map<String, String>(); //keyed from Parent field, data contains child field
		Set<String> parentSyncFieldNames = new Set<String>();
		for (string s: temp)
		{
			string newVal = s;
			if (s.split('__').size() > 2)
			{
				string[] fieldWithNameSpace = s.split('__');
				//system.debug('Sync - SyncRecords - fieldWithNameSpace: '+fieldWithNameSpace);
				newVal = fieldWithNameSpace[1]+'__'+fieldWithNameSpace[2]; //additional 2 so the __ is not included in the name
			}
			parentSyncFieldMapping.put(s,newVal);
			system.debug('Sync - SyncRecords - ParentSyncMap: '+s+' -> '+newVal);
			parentSyncFieldNames.add(newVal);
		}
		//Dynamically pull back the fields that we're syncing		
		string SOQL = 'SELECT Id, Project__c';
		//Manual Parent Sync Fields
		parentSyncFieldMapping.put('pse__Account__c', 'Account__c');
		parentSyncFieldMapping.put('pse__Opportunity__c', 'Opportunity__c');
		parentSyncFieldNames.add('Account__c');
		parentSyncFieldNames.add('Opportunity__c');		
		system.debug('Sync - Sync Records - parentSyncFieldMapping: '+parentSyncFieldMapping);
		system.debug('Sync - Sync Records - parentSyncFieldNames: '+parentSyncFieldNames);
		Set<String> validChildFieldNames = new Set<String>();
		for (string s: Sync.GetFieldNames('Project_Read_Only__c'))
		{
			if (parentSyncFieldNames.contains(s))
			{
				system.debug('Sync - SyncRecords - Project_Read_Only__c field found: '+s);
				for (string ps: parentSyncFieldMapping.keySet())
				{
					string mappedField = parentSyncFieldMapping.get(ps);
					if (mappedField == s)
					{
						SOQL += ', '+mappedField;
						validChildFieldNames.add(mappedField);
					}
				}
			}
		}
		SOQL += ' FROM Project_Read_Only__c WHERE Project__c IN: projIds';
		system.debug('Sync - SyncRecords - SOQL: '+SOQL);
		system.debug('Sync - SyncRecords - projIds: '+projIds);
		//Populate the mapping
		for (Project_Read_Only__c pro: database.query(SOQL))
		{
			if (pro.Project__c != null)
			{
				if (childSyncRecords.containsKey(pro.Project__c)) //Add to existing collection
				{
					List<Project_Read_Only__c> exist = childSyncRecords.get(pro.Project__c);
					exist.add(pro);
					childSyncRecords.remove(pro.Project__c);
					childSyncRecords.put(pro.Project__c,exist);
				}
				else //Create a new collection
				{
					List<Project_Read_Only__c> newCollection = new List<Project_Read_Only__c>();
					newCollection.add(pro);
					childSyncRecords.put(pro.Project__c,newCollection);
				}
			}
		}
		system.debug('Sync - SyncRecords - childSyncRecords: '+childSyncRecords);
		List<Project_Read_Only__c> toUpsert = new List<Project_Read_Only__c>();
		for (pse__Proj__c proj: toProcess.values())
		{
			Project_Read_Only__c found;
			if (childSyncRecords.containsKey(proj.Id)) //Child records were found and need to be updated
			{
				for (Project_Read_Only__c pro: childSyncRecords.get(proj.Id))
				{
					for (string s: parentSyncFieldMapping.keySet())
					{
						//Special Case for Name translation to Name__c
						if (s == 'Name')
						{
							pro.put('Name__c',proj.get(s));
						}
						else if (validChildFieldNames.contains(parentSyncFieldMapping.get(s)))
						{
							string FieldName = parentSyncFieldMapping.get(s);
							pro.put(FieldName,proj.get(s));
							system.debug('Sync - SyncRecords - Project Read Only Update: Field: '+parentSyncFieldMapping.get(s)+' Value: '+proj.get(s));
						}
					}
					toUpsert.add(pro);
				}
			}
			else //No child record exists - create a new one
			{
				Project_Read_Only__c newPRO = new Project_Read_Only__c();
				newPro.Project__c = proj.Id;
				for (string s: parentSyncFieldMapping.keySet())
				{
					//if (validChildFieldNames.contains(parentSyncFieldMapping.get(s))) newPRO.put(parentSyncFieldMapping.get(s),proj.get(s));
					//Special Case for Name translation to Name__c
					if (s == 'Name')
					{
						newPro.put('Name__c',proj.get(s));
					}
					else if (validChildFieldNames.contains(parentSyncFieldMapping.get(s)))
					{
						string FieldName = parentSyncFieldMapping.get(s);
						newPro.put(FieldName,proj.get(s));
						system.debug('Sync - SyncRecords - Project Read Only Update: Field: '+parentSyncFieldMapping.get(s)+' Value: '+proj.get(s));
					}
				}
				toUpsert.add(newPRO);
			}
		}
		system.debug('Sync - SyncRecords - toUpsert: '+toUpsert);
		if (!toUpsert.isEmpty()) upsert(toUpsert);
	}
	
	public static Set<String> getParentProjectSyncFields()
    {
        Set<String> fieldNames = new Set<String>();
        List<Schema.FieldSetMember> fsFields = SObjectType.pse__Proj__c.FieldSets.SyncFields.getFields();
        if (fsFields != null && !fsFields.isEmpty())
        {
                for (Schema.FieldSetMember f :fsFields)
                {
                        if (!fieldNames.contains(f.getFieldPath()))
                        {
                                fieldNames.add(f.getFieldPath());
                        }
                }
        }
        return fieldNames;
    }
    
    public static List<String> GetFieldNames(String objectname)
    {
		List<String> retval = new List<String>();
        Map<String,Schema.SObjectField> fields;
        //Use GlobalDescribe to get a list of all available Objects
        Map<String,Schema.Sobjecttype> gd = Schema.getGlobalDescribe();
        Set<String> objectKeys = gd.keySet();
        for (String objectKey:objectKeys) //Iterate through all of the objects until we get to the one we need
        {
        	if (objectKey == objectname.toLowerCase()) //Object exists, get field names
            {
            	Schema.SObjectType systemObjectType = gd.get(objectKey);
                Schema.DescribeSObjectResult r = systemObjectType.getDescribe();
                Map<String, Schema.SObjectField> M = r.fields.getMap();
                Set <String>fieldNames = M.keySet();
                fields = new Map<String,Schema.SObjectField>();
                //Create a copy of the Map with the Display Name, and Properties for retreival in the Selection
                for (String fieldName:fieldNames)
                {
                	//For each field, Add to List
                    Schema.SObjectField field = M.get(fieldName);
                    Schema.DescribeFieldResult fieldDesc = field.getDescribe();                                     
                    //retval.add(fieldDesc.getLabel()); //Adds the Salesforce UI label to listing
                    //if (fieldDesc.isUpdateable())
                    if (!fieldDesc.isCalculated()) retval.add(fieldDesc.getName());
                    fields.put(fieldDesc.getLabel(),field);
                }
            }
        }                               
        return retval;
     }
}