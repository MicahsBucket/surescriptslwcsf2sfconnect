/*
    05/04/15    SB @ IC    00114842
*/
global class SeverityOneBusinessHours implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT Severity_1_Business_Hours__c FROM Case WHERE IsClosed = false AND Priority = \'Severity 1 (Use sparingly)\' AND FLAGS__Initial_Response__c = null');
    }
    
    global void execute(Database.BatchableContext bc,list<sObject> scope) {
        update CaseSeverityUtil.checkBusinessHours((list<Case>)scope);
    }
    
    global void finish(Database.BatchableContext bc) {
    }
}