/****************************************************************************************************
*Description:   	This is the test Class for Synchronizing Data from Parent to Child Project Information
*					pse__Proj__c on way sync to Project_Read_Only__c. ProjectSyncToProjectReadOnly trigger
*					and Sync class are coverred in this class
*
*Required Class(es):Sync_Test
*
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0    	11/20/2014	Justin Padilla      Initial Version
*****************************************************************************************************/
@isTest
private class Sync_Test {

    static testMethod void TriggerTest()
	{
       //Create an Account
       
       //Create an Opportunity
       
       //Create a Project
       
       //Update a Project
       
       //Delete a Project
    }
}