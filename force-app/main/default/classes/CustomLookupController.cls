/****************************************************************************************************
*Description:           This is the controller for the CustomLookup component
*
*Required Class(es):    CustomLookupController_TEST
*
*Organization: Rainmaker-LLC
*
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0     05/11/2015   Justin Padilla     Initial Implementation
*****************************************************************************************************/
public class CustomLookupController
{
	public string RecordId{get;set;} //Source Id
	public string FieldValue{get;set;} //Container to store the selected Field for Save
	public string FieldValueDisplay{get;set;} //Value Displayed on the VF Page if populated
	public string LookupId{get;set;} //Id for the Lookup field - used for linking - do not change
	public string PageURL{get{return URL.getSalesforceBaseUrl().toExternalForm()+URL.getCurrentRequestUrl().getPath()+'?'+URL.getCurrentRequestUrl().getQuery()+'&pageType=popup';}set;} //Page URL for the popup window - self reference with popup applied
	public string BaseURL{get{return URL.getSalesforceBaseUrl().toExternalForm();}set;} //Base URL - Used for linking, do not change
	public boolean isPopup{get;set;} //Determines if the window being displayed is a popup
	public List<String> columns{get;set;} //Columns to display on popup window - and query for
	public list<Sobject> records{get;set;} //Listing of records to display in popup
	public string recordCount{get{if (records != null)return string.valueOf(records.size());return 'None';}set;} //Record Count matching criteria
	public Sobject workingSobject{get;set;} //Source object
	public boolean refreshPage{get;set;} //Refresh parent window after save has completed
	
	/*********************************** Component Attribute assignments *********************************************************/
	private string pSourceType;
	public string getSourceType(){return pSourceType;}
	public void setSourceType(string s){if (pSourceType == null){pSourceType= s;setUp();}}
	
	private string pFieldLabel;
	public string getFieldLabel(){return pFieldLabel;}
	public void setFieldLabel(string s){pFieldLabel = s;}
	
	private string pFieldAPIName;
	public string getFieldAPIName(){return pFieldAPIName;}
	public void setFieldAPIName(string s){if (pFieldAPIName == null){pFieldAPIName = s;setUp();}}
	
	private string pField2Display;
	public string getField2Display(){return pField2Display;}
	public void setField2Display(string s){if (pField2Display == null){pField2Display = s;setUp();}}
	
	private string precordObject;
	public string getrecordObject(){return precordObject;}
	public void setrecordObject(string s){if (precordObject == null){precordObject = s;setUp();}}
	
	private string precordDisplayColumns;
	public string getrecordDisplayColumns(){return precordDisplayColumns;}
	public void setrecordDisplayColumns(string s){if (precordDisplayColumns == null){precordDisplayColumns = s;system.debug('precordDisplayColumns -  Setter: '+precordDisplayColumns);}}
	
	private string precordFilters;
	public string getrecordFilters(){return precordFilters;}
	public void setrecordFilters(string s){if (precordFilters == null){precordFilters = s;system.debug('precordDisplayColumns -  Setter: '+precordFilters);}}
	
	private string precordWhere;
	public string getrecordWhere(){return precordWhere;} //Where portion of the records lookup SOQL *Parameter
	public void setrecordWhere(string s){if (precordWhere == null){precordWhere = s;system.debug('precordWhere -  Setter: '+precordWhere);}} //setUp();
		
	public boolean InitialSettersFinished()
	{
		boolean retval = false;
		if (getSourceType() != null && getFieldAPIName() != null && getField2Display() != null && getrecordObject() != null) retval = true;
		return retval;
	}
	
	public boolean PopupSettersFinished()
	{
		boolean retval = false;
		if (getrecordObject() != null && getrecordDisplayColumns() != null && getrecordFilters() != null && getrecordWhere() != null) retval = true;
		return retval;
	}

	public void setUp()
	{
		system.debug('InitialSettersFinished: '+InitialSettersFinished());
		if (!InitialSettersFinished()) return;
		//Initialize
		isPopup = false;
		refreshPage = false;
		records = new List<SobjecT>();
		columns = new List<String>();
		if (apexpages.currentPage().getParameters().get('Id') != '')
		{
			RecordId = apexpages.currentPage().getParameters().get('Id');
			//Read in Display columns for processing
			if (precordDisplayColumns != null)
			{
				for (string s: precordDisplayColumns.split(';'))
				{
					columns.add(s.trim());
				}
			}			
		}
		if (apexpages.currentPage().getParameters().get('pageType') == 'popup') isPopup = true;
		//Setup
		Set<String> soqlfields = new Set<String>();
		string workingSOQL = 'SELECT Id, '+pFieldAPIName+', ';
		soqlfields.add('Id');
		soqlfields.add(pFieldAPIName);
		if (precordFilters != null)
		{
			for (string s: precordFilters.split(';'))
			{
				if (!soqlfields.contains(s))
				{
					workingSOQL += s+', ';
					soqlfields.add(s);
				}
			}
		}
		if (workingSOQL.endsWith(', ')) workingSOQL = workingSOQL.substring(0,workingSOQL.lastIndexOf(', '));
		workingSOQL += ' FROM '+pSourceType+' WHERE ID =: RecordId';
		system.debug('workingSOQL: '+workingSOQL);
		for (Sobject c: database.query(workingSOQL))
		{
			workingSobject = c;
		}
		system.debug('pFieldAPIName: '+pFieldAPIName);
		if (workingSobject.get(pFieldAPIName) != null)
		{
			string tempfieldValue = string.valueOf(workingSobject.get(pFieldAPIName));
			LookupId = tempfieldValue;
			system.debug('tempfieldValue: '+tempfieldValue);
			//There's a reference already set, Attempt to gather the Information to display
			string tempSOQL = 'SELECT Id, '+pField2Display+' FROM '+precordObject+' WHERE Id=:tempfieldValue';
			system.debug('tempSOQL: '+tempSOQL);
			for (Sobject t: database.query(tempSOQL))
			{
				FieldValueDisplay = string.valueOf(t.get(pField2Display));
			}
		}
		system.debug('workingSobject: '+workingSobject);
		if (isPopup) getRecords();
	}
	
	//Collects records to display in the popup window
	public void getRecords()
	{
		//string filter = string.valueOf(workingSobject.get('Contract_Category__c'));
		string filter1;
		string filter2;
		string filter3;
		string filter4;
		string filter5;
		string[] filterFields = precordFilters.split(';');
		for (integer i=0;i<5;i++)
		{
			if (i == 0 && filterFields.size() > 0) filter1 = string.valueOf(workingSobject.get(filterFields[0]));
			if (i == 1 && filterFields.size() >= 2) filter2 = string.valueOf(workingSobject.get(filterFields[1]));
			if (i == 2 && filterFields.size() >= 3) filter3 = string.valueOf(workingSobject.get(filterFields[2]));
			if (i == 3 && filterFields.size() >= 4) filter4 = string.valueOf(workingSobject.get(filterFields[3]));
			if (i == 4 && filterFields.size() >= 5) filter5 = string.valueOf(workingSobject.get(filterFields[4]));
		}

		Set<string> fieldsUsed = new Set<String>();
		string recordSOQL = 'SELECT Id, ';
		fieldsUsed.add('Id');
		for (string s: columns)
		{
			if (!fieldsUsed.contains(s))
			{
				recordSOQL += s+', ';
				fieldsUsed.add(s);
			}
		}
		if (recordSOQL.endsWith(', ')) recordSOQL = recordSOQL.substring(0, recordSOQL.lastIndexOf(', '));
		recordSOQL +=' FROM '+precordObject;
		if (getrecordWhere() != null && getrecordWhere() != '') recordSOQL += ' WHERE '+getrecordWhere();
		system.debug('recordSOQL: '+recordSOQL);
		system.debug('filter1: '+filter1);
		for (sObject cd: database.query(recordSOQL))
		{
			records.add(cd);
		}
	}
	
	//Updates Source Record with Id value from popup selection
	public void UpdateRecord()
	{
		if (FieldValue != null && FieldValue != '')
		{
			workingSobject.put(pFieldAPIName, FieldValue);
			update(workingSobject);
			refreshPage = true;
		}
	}
	//Nullifies the Source Record value
	public void ClearValue()
	{
		workingSobject.put(pFieldAPIName, null);
		update(workingSobject);
		refreshPage = true;
	}
}