/****************************************************************************************************
*Description:           This is the test class for the CustomLookupController controller that handles
*                       the CustomLookup component
*
*Required Class(es):    N/A
*
*Organization: Rainmaker-LLC
*
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0     05/28/2015   Justin Padilla     Initial Implementation
*****************************************************************************************************/
@isTest
private class CustomLookupController_TEST
{
    static testMethod void ControllerTest()
    {
    	List<Contract> contracts = new List<Contract>([SELECT Id, Contract_Template_Name__c FROM Contract WHERE AccountId != null LIMIT 5]);
    	List<Contract_Document__c> contractDocuments = new List<Contract_Document__c>([SELECT Id FROM Contract_Document__c LIMIT 5]);
    	if (!contracts.isEmpty() && !contractDocuments.isEmpty())
    	{
    		//Set a Contract_Template_Name__c lookup id to provide additional code coverage
    		contracts[0].Contract_Template_Name__c = contractDocuments[0].Id;
    		update(contracts[0]);
    		//Set the Contract Id 
    		Apexpages.currentPage().getParameters().put('id',contracts[0].Id);
    		//Call the Controller
	        CustomLookupController page = new CustomLookupController();
	        //Set Attribute Values
	        page.setSourceType('Contract');
	        page.setFieldLabel('Contract Template Name');
	        page.setFieldAPIName('Contract_Template_Name__c');
	        page.setField2Display('Name');
	        page.setrecordObject('Contract_Document__c');
	        page.setrecordDisplayColumns('Name;Contract_Category__c;Template_Type__c;Document_Description__c;Definition_Helptext__c');
	        page.setrecordWhere('Contract_Category__c =: filter1 AND Active__c = true');
	        page.setrecordFilters('Contract_Category__c');
	        
	        page.setUp();
	        
	        if (page.PopupSettersFinished()) page.getRecords();
	        
	        page.ClearValue();
	        page.FieldValue = contractDocuments[0].Id;
	        page.UpdateRecord();
	        
	        //Additional Gets provided by component
	        string temp1 = page.getFieldLabel();
	        string temp2 = page.recordCount;
	        string temp3 = page.PageURL;
	        string temp4 = page.BaseURL;
	        
    	}       
    }
}