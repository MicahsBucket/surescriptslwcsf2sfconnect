/**
 * CPIMockResponse
 * @description Serves as a mock response for the various CPI Metrics and Rates RESTful callouts an their unit tests.
 * @author  Demand Chain Systems (James Loghry)
 * @date  4/10/2017
 */
@isTest
public class CPIMockResponse implements HttpCalloutMock {

    public List<Object> responseObjects {get; set;}
    public List<String> errors {get; set;}

    public CPIMockResponse(List<Object> responseObjects){
    	this(responseObjects,new List<String>());
    }

    public CPIMockResponse(List<Object> responseObjects,List<String> errors){
        System.debug('JWL: constructor: ' + responseObjects);
        this.responseObjects = responseObjects;
        this.errors = errors;
    }

    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse resp = new HttpResponse();
        resp.setHeader('Content-Type','application/json');

        System.debug('JWL: responseObjects: ' + responseObjects);

        if(errors != null && !errors.isEmpty() && !String.isEmpty(errors.get(0))){
        	resp.setBody(errors.get(0));
        	resp.setStatusCode(400);
        }else{
        	Object responseObject = responseObjects.get(0);
        	resp.setBody(JSON.serialize(responseObject));
        	resp.setStatusCode(200);
        }

        if(responseObjects != null && !responseObjects.isEmpty()){
            responseObjects.remove(0);
        }

        if(errors != null && !errors.isEmpty()){
            errors.remove(0);
        }

        return resp;
    }
}