public with sharing class SfdcS2SOAuth2Controller {

    private String clientId;
    private String clientSecret;
    private String loginUrl;
    private String redirectUrl;
    private SfdcS2SOAuth oauth = new SfdcS2SOAuth();
    
    public SfdcS2SOAuth2Controller() {
        SFDC_S2S_OAuth_Settings__mdt[] settings = [SELECT ClientId__c, ClientSecret__c, LoginUrl__c, RedirectUrl__c FROM SFDC_S2S_OAuth_Settings__mdt LIMIT 1];
        clientId = settings[0].ClientId__c;
        clientSecret = settings[0].ClientSecret__c;
        loginUrl = settings[0].LoginUrl__c;
        redirectUrl = settings[0].RedirectUrl__c;
    }
    
    public PageReference authorize() {
        return new PageReference(loginUrl + '/services/oauth2/authorize?'+'response_type=code&client_id='+
            clientId+'&redirect_uri='+redirectUrl+'&scope=api%20refresh_token%20chatter_api');
    }
    
    public PageReference refresh() {
        SFDC_S2S_OAuth2Tokens__c tokens = SFDC_S2S_OAuth2Tokens__c.getOrgDefaults();
        
        //let's make sure we already have a refresh token, if not let's restart the authorize flow
        if (tokens.RefreshToken__c == null) {
            return authorize();
        } else {
            refreshAccessToken(true);
            return null;
        }
    }
    
    public SfdcS2SOAuth refreshAccessToken(boolean doUpdate) {
        SFDC_S2S_OAuth2Tokens__c tokens = SFDC_S2S_OAuth2Tokens__c.getOrgDefaults();
        String body = 'grant_type=refresh_token&client_id=' + clientId +
                '&refresh_token=' + tokens.RefreshToken__c + '&client_secret=' + clientSecret;   
        return oauth.postTokenEndpoint(tokens.InstanceUrl__c, body, true);
    }

    public PageReference callback() {
        String code = ApexPages.currentPage().getParameters().get('code'); 
        String body = 'grant_type=authorization_code&client_id=' + clientId +
            '&redirect_uri=' + redirectUrl + '&client_secret=' + clientSecret + '&code=' + code;
                     
        oauth.postTokenEndpoint(loginUrl, body, true);
        return null;
    }
    
    public String getAuthStatus() {
        return oauth.callbackResponse;
    }
    
    @TestVisible private void setSfdcS2SOAuthMock(SfdcS2SOAuth oauthMock) {
        oauth = oauthMock;
    }
}