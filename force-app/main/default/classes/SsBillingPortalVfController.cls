public class SsBillingPortalVfController {

    private final string authUrl;
    private final string clientId;
    private final string audienceUrl;
    private final string certificateName;
    private final Contract contract;
    
    // 
    // ?id={!Contract.Id}
    // &authUrl=https://localhost:44347/oauth/authenticate
    // &client_id=3MVG9ahGHqp.k2_yukQLm_CJdzZhpwhONzfXn.eazqU_VeLItSJXukzSETvHd.TojaPp2AhlKqdxLTpOT5QYk
    // &audience_url=https://test.salesforce.com
    // &certificate_Name=BillingPortalCert
    // &product=PeteCDM
    // 

    public SsBillingPortalVfController(ApexPages.StandardController stdController) {
        authUrl = 'authUrl';
  	    clientId = ApexPages.currentPage().getParameters().get('client_id'); 
        audienceUrl = AudienceUrl();
        certificateName = 'BillingPortalCert';
        
        // unit tests barf if addFields is called
        if (!Test.isRunningTest()) {
            stdController.addFields(new List<String>{'P_Drive_Link__c', 'Surescripts_Products_Services__c'});
        }
        
        this.contract = (Contract)stdController.getRecord();
    }
    
    public string GetProduct() {
        String products = this.GetProducts();
        return products.length() > 0 ? products.split(',').get(0) : '';
    }
       
    public string GetProducts() {
        //
        // Currently supported products: CDM, Routing
        // 
        
        List<String> products = new List<String>();
        
        if(contract.Surescripts_Products_Services__c == Null) {
            return '';
        }
        
        String[] selectedProducts = contract.Surescripts_Products_Services__c.split(';');
        
        For(String product: selectedProducts) {
            if(this.IsCdmProduct(product)) {
                products.add('CDM');
            } else if(this.IsRoutingProduct(product)) {
                products.add('ERX');
            } else if (this.IsRoutingIncentivesProduct(product)) {
                products.add('ERXINCT');
            } else if(this.IsRecordLocatorAndExchangeProduct(product)) {
                products.add('RLE');
            } else if(this.IsRecordLocatorAndExchangeDataRightsProduct(product)) {
                products.add('RLE_DR');
            } else if(this.IsEligibilityProduct(product)) {
                products.add('ELIGIBILITY');
            } else if(this.IsFaxGatewayProduct(product)) {
                products.add('ERXFAX');
            } else if(this.IsEpaProduct(product)) {
                products.add('EPA');
            } else if(this.IsEpaIncentivesProduct(product)) {
                products.add('EPAINCT');
            } else if(this.IsEligibilityHubsProduct(product)) {
                products.add('ELIGHUB');
            } else if(this.IsEligibilityIncentivesProduct(product)) {
                products.add('ELIGINCT');
            } else if(this.IsEpaHubsProduct(product)) {
                products.add('EPAHUB');
            } else if(this.IsSpeProduct(product)) {
                products.add('SPE');
            } else if(this.IsSpeHubsProduct(product)) {
                products.add('SPEHUB');
            } else if(this.IsSpeIncentivesProduct(product)) {
                products.add('SPEINCT');
            }
        }
        
        return string.join(products, ',');
    }
    
    public string GetAllProducts() {
        return contract.Surescripts_Products_Services__c != null
            ? contract.Surescripts_Products_Services__c
            : '';
    }
    
    // Note that apex string comparisons are case insensitive for "==" operator comparisons
    // However "indexOf" and other such methods are case sensitive

    private Boolean IsCdmProduct(String product) {
        return product.toLowerCase().indexOf('cdm', 0) != -1
            || product.toLowerCase().indexOf('clinical direct messaging', 0) != -1;
    }
    
    private Boolean IsRoutingProduct(String product) {
        return product == 'erx (e-prescribing)';
    }
    
    private Boolean IsRoutingIncentivesProduct(String product) {
        return product == 'erx incentive (e-prescribing)';
    }
    
    private Boolean IsRecordLocatorAndExchangeProduct(String product) {
        return product.toLowerCase().indexOf('rle', 0) != -1
            && product.toLowerCase().indexOf('record locator & exchange', 0) != -1;
    }
    
    private Boolean IsRecordLocatorAndExchangeDataRightsProduct(String product) {
        return product.toLowerCase().indexOf('rle', 0) != -1
            && product.toLowerCase().indexOf('data rights', 0) != -1;
    }
    
    private Boolean IsEligibilityProduct(String product) {
        return product == 'eligibility';
    }
    
    private Boolean IsFaxGatewayProduct(String product) {
        return product.toLowerCase().indexOf('fax gateway', 0) != -1;
    }
    
    private Boolean IsEpaProduct(String product) {
        return product.toLowerCase().indexOf('electronic prior authorization', 0) != -1;
    }
    
    private Boolean IsEpaHubsProduct(String product) {
        return product.toLowerCase().indexOf('epa - hubs', 0) != -1;
    }
    
    private Boolean IsEpaIncentivesProduct(String product) {
        return product == 'ePA Incentives';
    }
    
    private Boolean IsEligibilityHubsProduct(String product) {
        return product == 'Hubs Billing – Eligibility';
    }
	
	private Boolean IsEligibilityIncentivesProduct(String product) {
        return product == 'eligibility incentives';
    }
	
	private Boolean IsSpeProduct(String product) {
        return product == 'SP - Enroll (Specialty Patient Enrollment)';
    }
	
	private Boolean IsSpeHubsProduct(String product) {
        return product == 'SPE - Hubs';
    }
	
	private Boolean IsSpeIncentivesProduct(String product) {
        return product == 'SPE - Incentives';
    }
    
    public string GetUserEmail() {
    	String userName = UserInfo.getUserName();
        User activeUser = [Select Email From User where Username = : userName limit 1];
        
        return activeUser.Email;    
    }
    
    public string GetAccessToken() {
        string user_id = UserInfo.getUserName();
        System.debug(user_id);
        string accessToken = SsBillingPortalAccessTokenGenerator.GetAccessToken(audienceUrl, clientId, authUrl, user_id, certificateName);

        return accessToken;
    }
    
    public string GetPDriveLink() {
   		if(contract.P_Drive_Link__c == Null){
    		return Null;
    	}
    	
    	return contract.P_Drive_Link__c.replace('\\', '\\\\');
    }
    
    public string GetProfile() {
    	Profile p = [Select Name from Profile where Id =: userinfo.getProfileid()];
		string pname = p.name;    
        
        return pname;        
    }  
    
    public string GetContractNumber() {
    	if(contract.Master_Contract_Record_Number__c == Null){
    		return Null;
    	}
    	
    	Contract c = [Select contractNumber from contract where Id =: contract.Master_Contract_Record_Number__c];
		string contractNumber = c.ContractNumber;    
        
        return contractNumber;        
    } 
    
    private static Boolean isSandBox;
    
    private static string AudienceUrl() {
    	
    	if ( isSandbox == null ){
            isSandbox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
        }
        
        return (isSandbox) ? 'https://test.salesforce.com' : 'https://login.salesforce.com' ;    	    
    }
}