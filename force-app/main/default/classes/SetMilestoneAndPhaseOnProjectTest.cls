/*
  Purpose:
        This trigger sets the Current Milestone & Phase on the project
        
  Initiative: IconATG FinancialForce PSA Implementation 
  Author:     William Rich
  Company:    IconATG
  Contact:    william.rich@iconatg.com
  Created:    5/21/2014
*/

@isTest(seeAllData=false)
private class SetMilestoneAndPhaseOnProjectTest {

    static testMethod void testMilestoneAndPhase() {
        pse__Region__c region = new pse__Region__c(
            Name = 'Test Region'
        );
        insert region;
        
        pse__Proj__c project = createProject(region);
        String projectPhase = 'Planning';
        pse__Milestone__c milestone = createMilestone(projectPhase, project);
        
        Test.startTest();
        
        milestone.pse__Status__c = 'Open';
        milestone.Actual_Start_Date__c = Date.today();
        update milestone;
        
        pse__Proj__c project_1 = [
            select
                Current_Milestone__c,
                Current_Phase__c
            from pse__Proj__c
            where Id = :project.Id
        ];
        
        System.assertEquals(milestone.Id, project_1.Current_Milestone__c);
        System.assertEquals(projectPhase, project_1.Current_Phase__c);
        
        Test.stopTest();
    }
    
    static pse__Proj__c createProject(pse__Region__c region) {
        pse__Proj__c project = new pse__Proj__c(
            Name = 'Test Project',
            pse__Is_Active__c = true,
            pse__Is_Billable__c = true,
            pse__Allow_Timecards_Without_Assignment__c = true,
            pse__Allow_Expenses_Without_Assignment__c = true,
            pse__Start_Date__c = Date.today().toStartOfWeek(),
            pse__End_Date__c = Date.today().toStartOfWeek().addMonths(6),
            pse__Region__c = region.Id
        );
        
        insert project;
        return project;
    }
    
    static pse__Milestone__c createMilestone(String projectPhase, pse__Proj__c project) {
        pse__Milestone__c milestone = new pse__Milestone__c(
            Name = 'Test Milestone',
            pse__Project__c = project.Id,
            Related_Phase__c = projectPhase,
            pse__Target_Date__c = project.pse__End_Date__c,
            pse__Status__c = 'Planned'
        );
        insert milestone;
        return milestone;
    }
}