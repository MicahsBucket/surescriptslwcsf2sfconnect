@istest
public with sharing class ShareFilesControllerTEST {
    @isTest(seeAllData=true)
    public static void relatedFilesTEST() {
        List<Attachment> attList = [SELECT Id, ParentId, Name, ContentType, BodyLength, Body, Description, CreatedDate, CreatedBy.Name , IsPartnerShared FROM Attachment WHERE ParentId != null AND IsPartnerShared = true LIMIT 10];
        Set<Id> CaseIdSet = new Set<Id>();
        for(Attachment att: attList){
            CaseIdSet.add(att.ParentId);
        }
        List<ContentDocumentLink> cdlList = [SELECT Id, LinkedEntityId, ContentDocumentId  FROM ContentDocumentLink WHERE LinkedEntityId IN: CaseIdSet];
        if(cdlList.size() > 0){
            ShareFilesController.relatedFiles(cdlList[0].LinkedEntityId);
        }
        else{
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.LinkedEntityId = attList[0].ParentId;
            cdl.ContentDocumentId = [SELECT Id FROM ContentDocument LIMIT 1].Id;
            insert cdl;
            List<ContentDocumentLink> cdlList2 = [SELECT Id, LinkedEntityId, ContentDocumentId  FROM ContentDocumentLink WHERE Id =: cdl.Id];
            system.debug(cdlList2);
            
            ShareFilesController.relatedFiles(cdlList2[0].LinkedEntityId);
        }
        
    }

    @isTest(seeAllData=true)
    public static void saveFileTEST() {
        List<Attachment> attList = [SELECT Id, ParentId, Name, ContentType, BodyLength, Body, Description, CreatedDate, CreatedBy.Name , IsPartnerShared FROM Attachment WHERE ParentId != null AND IsPartnerShared = true LIMIT 10];
        Set<Id> CaseIdSet = new Set<Id>();
        for(Attachment att: attList){
            CaseIdSet.add(att.ParentId);
        }
        List<ContentDocumentLink> cdlList = [SELECT Id, LinkedEntityId, ContentDocumentId  FROM ContentDocumentLink WHERE LinkedEntityId IN: CaseIdSet];
        if(cdlList.size() > 0){
            ShareFilesController.saveFile(cdlList[0].LinkedEntityId, 'Test File', 'test This', false);
        }
        else{
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.LinkedEntityId = attList[0].ParentId;
            cdl.ContentDocumentId = [SELECT Id FROM ContentDocument LIMIT 1].Id;
            insert cdl;
            
            ShareFilesController.saveFile(cdl.LinkedEntityId, 'Test File', 'test This', false);
        }
    }

    @isTest(seeAllData=true)
    public static void deleteFilesTEST() {
        List<Attachment> attList = [SELECT Id, ParentId, Name, ContentType, BodyLength, Body, Description, CreatedDate, CreatedBy.Name , IsPartnerShared FROM Attachment WHERE ParentId != null AND IsPartnerShared = true LIMIT 10];
        List<Id> AttachmentIdSet = new List<Id>();
        for(Attachment att: attList){
            AttachmentIdSet.add(att.Id);
        }
        ShareFilesController.deleteFiles(AttachmentIdSet);
    }

    @isTest(seeAllData=true)
    public static void updateFileSharingTEST() {
        List<Attachment> attList = [SELECT Id, ParentId, Name, ContentType, BodyLength, Body, Description, CreatedDate, CreatedBy.Name , IsPartnerShared FROM Attachment WHERE ParentId != null AND IsPartnerShared = true LIMIT 10];
        // List<Id> AttachmentIdSet = new List<Id>();
        // for(Attachment att: attList){
        //     AttachmentIdSet.add(att.Id);
        // }
       ShareFilesController.updateFileSharing(attList[0].Id, true);
       ShareFilesController.updateFileSharing(attList[1].Id, false);
       ShareFilesController.updateAllFileSharing(attList[0].ParentId);
       ShareFilesController.updateAllFileSharing(attList[1].ParentId);
    }
}
