public class SsBillingPortalAccessTokenGenerator {
    
	Public static string GetAccessToken(
        string url_origin, 
        string client_id, 
        string redirect_url, 
        string user_id,
        string certificate_name) 
    { 
         Map<String, String> m = new Map<String, String>();
         m.put('grant_type', 'urn:ietf:params:oauth:grant-type:jwt-bearer');
         m.put('assertion', createToken(url_origin, client_id, redirect_url, user_id, certificate_name));
     
         HttpRequest req = new HttpRequest();
         req.setHeader('Content-Type','application/x-www-form-urlencoded');
         req.setEndpoint('callout:LocalCallback' +'/services/oauth2/token');
         req.setMethod('POST');
         req.setTimeout(10 * 1000);
         string requestBody = formEncode(m);
         System.debug('TokenGenerator debug: requestBody: ' + requestBody);
         req.setBody(requestBody);
          
         HttpResponse res = new Http().send(req);
         string responseBody = res.getBody();
         System.debug('TokenGenerator debug: responseBody: ' + responseBody);
         if (res.getStatusCode() >= 200 && res.getStatusCode() < 300) {
             return extractJsonField(responseBody, 'access_token');
         } else {
             throw new JwtException(responseBody);
         }
    }

    /**
     * Inspired from here: 
     * https://help.salesforce.com/articleView?id=remoteaccess_oauth_jwt_flow.htm&type=0
     * Even better example here: 
     * https://force201.wordpress.com/2014/09/20/an-apex-implementation-of-the-oauth-2-0-jwt-bearer-token-flow/
     */
    private static string createToken(
        string url_origin, 
        string client_id, 
        string redirect_url, 
        string user_id,
    	string certificate_name) 
    {
        string header = '{"alg":"RS256"}';
    	string token = '';
                          
        //Encode the JWT Header and add it to our string to sign
        token = base64UrlSafe(Blob.valueof(header));
        //Separate with a period    
        token += '.';
        token += getJWTClaims(url_origin, client_id, user_id);
                
        Blob data = Blob.valueOf(token);
        //
        // algorithmName: RSA-SHA256
        // input: JWT token build specific to Salesforce format and in the form of a binary array.
        // CertDevName: CPIMicroJwtFlow.  The name to find the certificate to sign the JWT token bytes.
        //
        
		Blob signedBytes = System.Crypto.signWithCertificate('RSA-SHA256', data, certificate_name);
        string signedPayLoad = base64UrlSafe(signedBytes);
        token += '.' + signedPayLoad;
        
        return token;
    }
    
    private static string getJWTClaims(string url_origin, string client_id, string user_id){
      string claimTemplate = '\'{\'"iss": "{0}", "sub": "{1}", "aud": "{2}", "exp": "{3}"\'}\'';        
           
      string claims = String.format(
                claimTemplate,
                new List<String>{
                    //client_id or consumer key
                	client_id,
                    user_id, 
                    url_origin, 
                    String.valueOf(( System.currentTimeMillis()/1000 ) + 10)
                }
            );
        
        string claimsPayload = base64UrlSafe(Blob.valueof(claims));
        
        return claimsPayload;
    }
    
    private static String formEncode(Map<String, String> m) {
         
         String s = '';
         for (String key : m.keySet()) {
            if (s.length() > 0) {
                s += '&';
            }
            s += key + '=' + EncodingUtil.urlEncode(m.get(key), 'UTF-8');
         }
         return s;
    }
    
    private static String extractJsonField(String body, String field) {        
        string result;
        
        try{        
            JSONParser parser = JSON.createParser(body);
            while (parser.nextToken() != null) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME
                    && parser.getText() == field) {
                        parser.nextToken();
                        result = parser.getText();
                    }
            }  
        }
        catch(Exception ex){   
            
            throw new JwtException(field + ' not found in response ' + body);           
        }  
        
        return result;
    }
    
    public class JwtException extends Exception {
    }
    
    private static string base64UrlSafe(Blob b) {         
        return EncodingUtil.base64Encode(b).replace('+', '-').replace('/', '_');
    }    
}