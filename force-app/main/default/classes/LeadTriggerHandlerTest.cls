@isTest
private class LeadTriggerHandlerTest {

    @isTest static void testUpdateConvertedLeadsContracts(){
        
        //Create a Lead
        Id leadRecType = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Non-eRx (value-add)').getRecordTypeId();
        Lead ld = new Lead(FirstName= 'Test A', 
                           LastName= 'A Last', 
                           Status= '1_Open', 
                           Company= 'TestExports LLC', 
                           RecordTypeId = leadRecType);
        
        Insert ld;
        
        //Create a contract with account as placeholder account
        Id accRecType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Other Entity').getRecordTypeId();
        Account newAcc = new Account(Name = 'NDA Placeholder Account', 
                                     RecordTypeId = accRecType, 
                                     Legal_name_of_entity__c = 'NDA Placeholder Account', 
                                     State_of_incorporation__c = 'VA');
        
        insert newAcc;
        
        List<Account> acc = [SELECT Id, Name 
                             FROM Account 
                             WHERE Name = 'NDA Placeholder Account'];
        
        Id contractRecType = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('1_NDA or BAA').getRecordTypeId();
        Contract con = new Contract(AccountId = acc[0].Id, 
                                    Status = '1_New Request', 
                                    Contract_Category__c = 'NDA', 
                                    Date_Customer_Requested_Contract__c =  System.today().addDays(2), 
                                    RecordTypeId = contractRecType);
        
        Insert con;
        
        //Convert Lead
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.SetLeadId(ld.Id);
        lc.setConvertedStatus('5_Lead Qualified (Converted)');
        
        Database.LeadConvertResult lcResult = Database.convertLead(lc);
        
        if(lcResult.isSuccess() && ld.ConvertedAccountId != null){
            List<Contract> conList = [Select Id, Name from Contract where AccountId =: ld.ConvertedAccountId];
            System.assertEquals(con.AccountId, ld.ConvertedAccountId);
            System.assertEquals(1, conList.size());
        }
    }
}