@IsTest(SeeAllData=true)

public with sharing class CaseOwnerCopy {
 
    static TestMethod void CaseOwnerCopy() {
 
        // Grab two Users
        User[] users = [select Id from User where isActive=true limit 2 ];
        User u1 = users[0];
        User u2 = users[1];
 
        // Create a Case
        System.debug('Creating Case');
        Case x1 = new Case(Origin = 'Email', Subject = 'TEST Case', Status = 'New Case', OwnerId = u1.Id);
        insert x1;
 
        // Test: Case Owner Copy should be set to user 1
        Case x2 = [select id, OwnerId, Case_Owner_Copy__c from Case where Id = :x1.Id];
        System.assertEquals(u1.Id, x2.OwnerId);
        System.assertEquals(u1.Id, x2.Case_Owner_Copy__c);
 
        // Modify Owner
        x2.OwnerId = u2.Id;
        update x2;
 
        // Test: Case Owner Copy  should be set to user 2
        Case x3 = [select id, OwnerId, Case_Owner_Copy__c from Case where Id = :x2.Id];
        System.assertEquals(u2.Id, x3.OwnerId);
        System.assertEquals(u2.Id, x3.Case_Owner_Copy__c);
    }
}