public class SfdcS2SRemoteCalloutUtil {
	private static final String DATA_QUERY_URI = '/services/data/v40.0/query/?q=';
    
    @TestVisible private boolean doCallout = true;
    
    public List<Object> executeRemoteQuery(String query, SfdcS2SOAuth oauth) {
 
        HttpRequest req = new HttpRequest();
        req.setEndpoint(oauth.instance_url + DATA_QUERY_URI + EncodingUtil.urlEncode(query, 'UTF-8'));
        req.setMethod('GET');
        req.setHeader('Authorization', 'Bearer '+oauth.access_token);

        Http h = new Http();
        
        Integer statusCode = 200;
        String body = '{}';
        
        if (doCallout) {
        	HttpResponse res = h.send(req); 
       		statusCode = res.getStatusCode();
            body = res.getBody();
        }
        
        return statusCode == 200 ? deserialize(body) : null;
    }   
   
    @TestVisible private List<Object> deserialize(String body) {
        Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(body);
            
        Integer totalSize = (Integer) results.get('totalSize');
            
        if (totalSize > 0) {
        	return (List<Object>) results.get('records');
        }
        
        return null;
    }
}