public with sharing class AutoCaseTeamMember {
//Accept Batch Processing of Creating a standard object based off of the custom object Case_Team_Member__c
	public static void InsertNew(Case_Team_Members__c[] ctm)
	{
		for (Case_Team_Members__c member:ctm)
		{
			//Create Standard Object
			CaseTeamMember temp = new CaseTeamMember();
			//User ID
			temp.MemberId = member.Portal_User__c;
			//Case ID
			temp.ParentId = member.Case_Relationship__c;
			//Role
				//Go through Roles and See if CaseTeamMember has value of custom object
				//if not, use participant RoleId
				List<CaseTeamRole> role = new List<CaseTeamRole>();
				if (member.Participant_Role__c <> null)
				{
					//throw new CustomException(member.Participant_Role__c);
					role = [SELECT Id,Name FROM CaseTeamRole WHERE CaseTeamRole.Name = :member.Participant_Role__c];
				}
				if (role.size() == 0)
				{
					//Role was not found, Default to Participant Role
					role = [SELECT Id,Name FROM CaseTeamRole WHERE CaseTeamRole.Name = 'Participant'];
				}
			temp.TeamRoleId = role[0].Id;
			insert(temp);
		}
	}
	public static void CleanUp(Case_Team_Members__c[] remove)
	{
		for (Case_Team_Members__c ctm:remove)
		{
			List<CaseTeamMember> removal = [SELECT Id FROM CaseTeamMember WHERE CaseTeamMember.ParentId = :ctm.Case_Relationship__c AND CaseTeamMember.MemberId = :ctm.Portal_User__c];
			for (CaseTeamMember del:removal)
			{
				delete(del);
			}
		}
	}
	static testMethod void myTest() 
	{
		//Create new Case_Team_Members__c
		Case_Team_Members__c tctm = new Case_Team_Members__c();
		//Create new Case
		Case tcase = new Case();
		insert(tcase);
		
		//Get a Profile for the User
		//Profile p = [SELECT Id FROM Profile WHERE name='Partner Portal User'];
		/*
		//Get a Role for the Contact
		UserRole r = [SELECT Id FROM UserRole WHERE name='Rx Pharmacy Partner User'];
		*/
		//UserRole r = [SELECT Id FROM UserRole WHERE name='Partner Portal'];
		//Create Contact
		//Contact tcontact = new Contact();
		//tcontact.LastName = 'test';
		//insert(tcontact);
		//Create New User
		/*
		User tuser = new User(alias = 'test123', email='test123@noemail.com',
      	emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
      	localesidkey='en_US', country='United States',
      	timezonesidkey='America/Los_Angeles', username='test123@noemail.com');
      	tuser.profileid = p.Id;
      	tuser.UserRoleId = r.Id;
      	tuser.
      	tuser.ContactId = tcontact.Id;
		insert(tuser);		
		*/	

		//Retreieve an Active User not already assigned to the CaseTeamMember Object
		User u = [SELECT
		User.Id
		FROM User
		WHERE User.IsActive = true LIMIT 1];
		
		List<CaseTeamMember> toDelete = [SELECT
		CaseTeamMember.Id,
		CaseTeamMember.MemberId
		FROM CaseTeamMember
		WHERE CaseTeamMember.MemberId =: u.Id];
		if (toDelete != null && toDelete.size() > 0)
			delete(toDelete);
		
		//Assign user to the ctm
		tctm.Portal_User__c = u.Id;		
		//Assign Properties
		tctm.Case_Relationship__c = tcase.Id;
		//tctm.Portal_User__c = tuser.Id;
		//tctm.Portal_User__c = '00580000003YrQV';
		tctm.Participant_Role__c = 'Participant';
		List<Case_Team_Members__c> test = new List<Case_Team_Members__c>();
		test.add(tctm);
		//Test Insert
		//Begin Insert Trigger
		insert(test);
		//InsertNew(test);
		//Test CleanUp
		//CleanUp(test);
		//Begin Delete Trigger
		delete(test);
	}
	public class CustomException extends Exception {}
}