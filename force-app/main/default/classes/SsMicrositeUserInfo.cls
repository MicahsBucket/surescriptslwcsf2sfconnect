@RestResource(urlMapping='/SSMicro/*')
global with sharing class SsMicrositeUserInfo {

       
    @HttpGet
    global static string doGet() {
        return GetProfile();
    }
    
    private static string GetProfile() {
    	Profile p = [Select Name from Profile where Id =: userinfo.getProfileid()];
		string pname = p.name;    
        
        return pname;        
    }
}