@isTest
public class SsBillingPortalAccessControllerTests {
    
    // Certifiate previously created for signing the AccessToken
    // using Setup -> Security Controls -> Certificate and Key Management
    private static final String PRE_CREATED_CERTIFICATE_NAME = 'Billing_Portal';
    private static final String FAKE_TOKEN = 'fakeToken';
    private static final String CONNECTED_APP_CUSTOMER_KEY = '3MVG9dPGzpc3kWydu8HR9tG2ByNgb95wa6hiZOJWDGcrFnCEDot3_HqoAoYhJ5PuObVtAeWBRiyDHIIpRH3jZ';
    private static final String SUPPORTED_PRODUCT_SERVICES_LIST =
        'CDM (Clinical Direct Messaging);eRx (E-Prescribing);eRx Incentive (E-Prescribing);Eligibility;RLE (Record Locator & Exchange);RLE - Data Rights;Fax Gateway;ePA (Electronic Prior Authorization);ePA Incentives;Hubs Billing – Eligibility;ePA - Hubs;Speciality Pharmacy Enrollment;SPE - Hubs;SPE - Incentives;';

    private class Mock implements HttpCalloutMock {
 
        public HTTPResponse respond(HTTPRequest req) {
             
            HTTPResponse res = new HTTPResponse();
            System.assertEquals('POST', req.getMethod());
            System.assert(req.getBody().contains('grant_type'), req.getBody());
            System.assert(req.getBody().contains('assertion'), req.getBody());
             
            res.setStatusCode(200);
            res.setBody('{"scope":"api","access_token":"' + FAKE_TOKEN + '"}');
 
            return res;
        }
    }

    private class MockFail404 implements HttpCalloutMock {
 
        public HTTPResponse respond(HTTPRequest req) {
             
            HTTPResponse res = new HTTPResponse();
            System.assertEquals('POST', req.getMethod());
            System.assert(req.getBody().contains('grant_type'), req.getBody());
            System.assert(req.getBody().contains('assertion'), req.getBody());
             
            res.setStatusCode(404);
            res.setBody('Failed');
 
            return res;
        }
    }

    private class MockFail204 implements HttpCalloutMock {
 
        public HTTPResponse respond(HTTPRequest req) {
             
            HTTPResponse res = new HTTPResponse();
            System.assertEquals('POST', req.getMethod());
            System.assert(req.getBody().contains('grant_type'), req.getBody());
            System.assert(req.getBody().contains('assertion'), req.getBody());
             
            res.setStatusCode(204);
            res.setBody('Failed');
 
            return res;
        }
    }

    private class MockFail204EmptyBody implements HttpCalloutMock {
 
        public HTTPResponse respond(HTTPRequest req) {
             
            HTTPResponse res = new HTTPResponse();
            System.assertEquals('POST', req.getMethod());
            System.assert(req.getBody().contains('grant_type'), req.getBody());
            System.assert(req.getBody().contains('assertion'), req.getBody());
             
            res.setStatusCode(204);
            res.setBody('');
 
            return res;
        }
    }
    
    public static testMethod void testMicroAccessController() {
                       
        ApexPages.currentPage().getParameters().put('client_id', CONNECTED_APP_CUSTOMER_KEY);
        ApexPages.currentPage().getParameters().put('audience_url', 'https://test.salesforce.com');
        ApexPages.currentPage().getParameters().put('authUrl', 'https://localhost:44344/oauth/authenticate');
        ApexPages.currentPage().getParameters().put('returnUrl', 'https://localhost:44347/home');
        ApexPages.currentPage().getParameters().put('certificate_Name', 'BillingPortalCert');
        
        Contract contract = new Contract();
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        Test.setMock(HttpCalloutMock.class, new Mock());
        Test.StartTest();
        
        string token = controller.GetAccessToken();
        System.assertEquals(FAKE_TOKEN, token);
        
        Test.stopTest();

    }
    
    public static testMethod void testssBillingPortalVfController_GetProfile() {
               
        Contract contract = new Contract();
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        string profile = controller.GetProfile();

        Profile p = [Select Name from Profile where Id =: userinfo.getProfileid()];
                string pname = p.name;    
        
        
        System.assertEquals(pname, profile);
    }
    
    public static testMethod void testssBillingPortalVfController_GetContractNumber() {
               
        Contract contract = new Contract();
        contract.Master_Contract_Record_Number__c = Null;
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        string contractNumber = controller.GetContractNumber();

        System.assertEquals(Null, contractNumber);
    }
    
    public static testMethod void testssBillingPortalVfController_GetEmail() {
               
        Contract contract = new Contract();
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        string email = controller.GetUserEmail();

        String userName = UserInfo.getUserName();
        User activeUser = [Select Email From User where Username = : userName limit 1];
        
        System.assertEquals(activeUser.Email, email);
    }
    
    public static testMethod void testssBillingPortalVfController_GetProduct_ValidProducts() {
        
        Contract contract = new Contract();
        contract.Surescripts_Products_Services__c = 'CDM (Clinical Direct Messaging);eRx (E-Prescribing)';
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        string product = controller.GetProduct();
        System.assertEquals('CDM', product);
    }
    
    public static testMethod void testssBillingPortalVfController_GetProduct_NoProducts() {
        
        Contract contract = new Contract();
        contract.Surescripts_Products_Services__c = Null;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        string product = controller.GetProduct();
        System.assertEquals(0, product.length());
    }
    
    public static testMethod void testMicroAccessControllerException404() {        
              
        ApexPages.currentPage().getParameters().put('client_id', CONNECTED_APP_CUSTOMER_KEY);
        ApexPages.currentPage().getParameters().put('audience_url', 'https://test.salesforce.com');
        ApexPages.currentPage().getParameters().put('authUrl', 'https://localhost:44344/oauth/authenticate');
        ApexPages.currentPage().getParameters().put('returnUrl', 'https://localhost:44347/home');
        ApexPages.currentPage().getParameters().put('certificate_Name', 'BillingPortalCert');
        
        Contract contract = new Contract();
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        Test.setMock(HttpCalloutMock.class, new MockFail404());
        Test.StartTest();
        
        try{
            string token = controller.GetAccessToken();
        }
        catch(SsBillingPortalAccessTokenGenerator.JwtException ex){        
            Test.stopTest();
            return;
        }
        
        System.assert(false, 'Should not make it here');
        Test.stopTest();       
        
        Test.stopTest();

    }
    
    public static testMethod void testMicroAccessControllerException204() {        
              
        ApexPages.currentPage().getParameters().put('client_id', CONNECTED_APP_CUSTOMER_KEY);
        ApexPages.currentPage().getParameters().put('audience_url', 'https://login.salesforce.com');
        ApexPages.currentPage().getParameters().put('authUrl', 'https://localhost:44344/oauth/authenticate');
        ApexPages.currentPage().getParameters().put('returnUrl', 'https://localhost:44347/home');
        ApexPages.currentPage().getParameters().put('certificate_Name', 'BillingPortalCert');
        
        Contract contract = new Contract();
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        Test.setMock(HttpCalloutMock.class, new MockFail204());
        Test.StartTest();
        
        try{
            string token = controller.GetAccessToken();
        }
        catch(SsBillingPortalAccessTokenGenerator.JwtException ex){   
            Test.stopTest();
            return;
        }
        
        System.assert(false, 'Should not make it here');
        Test.stopTest();       

    }
    
    public static testMethod void testMicroAccessControllerException204EmptyBody() {        
              
        ApexPages.currentPage().getParameters().put('client_id', CONNECTED_APP_CUSTOMER_KEY);
        ApexPages.currentPage().getParameters().put('audience_url', 'https://test.salesforce.com');
        ApexPages.currentPage().getParameters().put('authUrl', 'https://localhost:44344/oauth/authenticate');
        ApexPages.currentPage().getParameters().put('returnUrl', 'https://localhost:44347/home');
        ApexPages.currentPage().getParameters().put('certificate_Name', 'BillingPortalCert');
        
        Contract contract = new Contract();
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        Test.setMock(HttpCalloutMock.class, new MockFail204EmptyBody());
        Test.StartTest();
        
        string token = controller.GetAccessToken();
        System.assertEquals(Null, token);
        
        Test.stopTest();

    }
    
    public static testMethod void testssMicrositeUserInfo_WebService_GetProfile() {       
        string profile = SsMicrositeUserInfo.doGet();

        Profile p = [Select Name from Profile where Id =: userinfo.getProfileid()];
        string pname = p.name;
        
        System.assertEquals(pname, profile);
    }
    
    public static testMethod void testssBillingPortalVfController_GetProducts_ValidProducts() {
        Contract contract = new Contract();
        contract.Surescripts_Products_Services__c = SUPPORTED_PRODUCT_SERVICES_LIST;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        List<String> products = controller.GetProducts().split(',');
        
        System.assertEquals(13, products.size());
        System.assertEquals('CDM', products.get(0));
        System.assertEquals('ERX', products.get(1));
        System.assertEquals('ERXINCT', products.get(2));
        System.assertEquals('ELIGIBILITY', products.get(3));
        System.assertEquals('RLE', products.get(4));
        System.assertEquals('RLE_DR', products.get(5));
        System.assertEquals('ERXFAX', products.get(6));
        System.assertEquals('EPA', products.get(7));
        System.assertEquals('EPAINCT', products.get(8));
        System.assertEquals('ELIGHUB', products.get(9));
        System.assertEquals('EPAHUB', products.get(10));
        System.assertEquals('SPEHUB', products.get(11));
        System.assertEquals('SPEINCT', products.get(12));
        // System.assertEquals('SPEINCT', products.get(13));
    }
    
    public static testMethod void testssBillingPortalVfController_GetProducts_InvalidProducts() {
        Contract contract = new Contract();
        contract.Surescripts_Products_Services__c = 'DNE (Unexpected Product Type)';
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        System.assertEquals(0, controller.GetProducts().length());
    }
    
    public static testMethod void testssBillingPortalVfController_GetProducts_CorrectlyParsesCDM() {
        Contract contract = new Contract();
        contract.Surescripts_Products_Services__c = 'CDM (Clinical Direct Messaging)';
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        List<String> products = controller.GetProducts().split(',');
        
        System.assertEquals(1, products.size());
        System.assertEquals('CDM', products.get(0));
    }
    
    public static testMethod void testssBillingPortalVfController_GetProducts_CorrectlyParsesRouting() {
        Contract contract = new Contract();
        contract.Surescripts_Products_Services__c = 'eRx (E-Prescribing)';
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        List<String> products = controller.GetProducts().split(',');
        
        System.assertEquals(1, products.size());
        System.assertEquals('ERX', products.get(0));
    }
    
    public static testMethod void testssBillingPortalVfController_GetProducts_CorrectlyParsesRoutingIncentives() {
        Contract contract = new Contract();
        contract.Surescripts_Products_Services__c = 'eRx Incentive (E-Prescribing)';
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        List<String> products = controller.GetProducts().split(',');
        
        System.assertEquals(1, products.size());
        System.assertEquals('ERXINCT', products.get(0));
    }
    
    public static testMethod void testssBillingPortalVfController_GetProducts_CorrectlyParsesEligibility() {
        Contract contract = new Contract();
        contract.Surescripts_Products_Services__c = 'Eligibility';
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        List<String> products = controller.GetProducts().split(',');
        
        System.assertEquals(1, products.size());
        System.assertEquals('ELIGIBILITY', products.get(0));
    }
    
    public static testMethod void testssBillingPortalVfController_GetProducts_CorrectlyParsesRle() {
        Contract contract = new Contract();
        contract.Surescripts_Products_Services__c = 'RLE (Record Locator & Exchange)';
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        List<String> products = controller.GetProducts().split(',');
        
        System.assertEquals(1, products.size());
        System.assertEquals('RLE', products.get(0));
    }
    
    public static testMethod void testssBillingPortalVfController_GetProducts_CorrectlyParsesRleDataRights() {
        Contract contract = new Contract();
        contract.Surescripts_Products_Services__c = 'RLE - Data Rights';
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        List<String> products = controller.GetProducts().split(',');
        
        System.assertEquals(1, products.size());
        System.assertEquals('RLE_DR', products.get(0));
    }
    
    public static testMethod void testssBillingPortalVfController_GetProducts_CorrectlyParsesFaxGateway() {
        Contract contract = new Contract();
        contract.Surescripts_Products_Services__c = 'Fax Gateway';
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        List<String> products = controller.GetProducts().split(',');
        
        System.assertEquals(1, products.size());
        System.assertEquals('ERXFAX', products.get(0));
    }
    
    public static testMethod void testssBillingPortalVfController_GetProducts_CorrectlyParsesEpa() {
        Contract contract = new Contract();
        contract.Surescripts_Products_Services__c = 'ePA (Electronic Prior Authorization)';
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        List<String> products = controller.GetProducts().split(',');
        
        System.assertEquals(1, products.size());
        System.assertEquals('EPA', products.get(0));
    }

    public static testMethod void testssBillingPortalVfController_GetProducts_CorrectlyParsesEpaHubs() {
        Contract contract = new Contract();
        contract.Surescripts_Products_Services__c = 'ePA - Hubs';
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        List<String> products = controller.GetProducts().split(',');
        
        System.assertEquals(1, products.size());
        System.assertEquals('EPAHUB', products.get(0));
    }

    public static testMethod void testssBillingPortalVfController_GetProducts_CorrectlyParsesSPE() {
        Contract contract = new Contract();
        contract.Surescripts_Products_Services__c = 'SP - Enroll (Specialty Patient Enrollment)';
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        List<String> products = controller.GetProducts().split(',');
        
        System.assertEquals(1, products.size());
        System.assertEquals('SPE', products.get(0));
    }

    public static testMethod void testssBillingPortalVfController_GetProducts_CorrectlyParsesSpeHubs() {
        Contract contract = new Contract();
        contract.Surescripts_Products_Services__c = 'SPE - Hubs';
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        List<String> products = controller.GetProducts().split(',');
        
        System.assertEquals(1, products.size());
        System.assertEquals('SPEHUB', products.get(0));
    }

    public static testMethod void testssBillingPortalVfController_GetProducts_CorrectlyParsesSpeIncentives() {
        Contract contract = new Contract();
        contract.Surescripts_Products_Services__c = 'SPE - Incentives';
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        List<String> products = controller.GetProducts().split(',');
        
        System.assertEquals(1, products.size());
        System.assertEquals('SPEINCT', products.get(0));
    }
    
    public static testMethod void testssBillingPortalVfController_GetProducts_Empty() {
        Contract contract = new Contract();
        contract.Surescripts_Products_Services__c = Null;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        System.assertEquals(0, controller.GetProducts().length());
    }
    
    public static testMethod void testssBillingPortalVfController_GetAllProducts_HasProducts() {
        Contract contract = new Contract();
        
        String expectedProducts = 'CDM (Clinical Direct Messaging);eRx (E-Prescribing);Eligibility;RLE (Record Locator & Exchange)';
        contract.Surescripts_Products_Services__c = expectedProducts;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        System.assertEquals(expectedProducts, controller.GetAllProducts());
    }
    
    public static testMethod void testssBillingPortalVfController_GetAllProducts_NoProducts() {
        Contract contract = new Contract();
        
        contract.Surescripts_Products_Services__c = Null;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        SsBillingPortalVfController controller = new SsBillingPortalVfController(sc);
        
        System.assertEquals('', controller.GetAllProducts());
    }
}