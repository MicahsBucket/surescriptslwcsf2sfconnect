/**
 * CPIMetricsCtrl
 * @description Lightning component controller class for displaying / submitting CPI Metrics information
 * @author  Demand Chain Systems (James Loghry)
 * @date  4/10/2017
 */
public with sharing class CPIMetricsCtrl {

	@AuraEnabled
	public static LightningResponse getContract(String recordId) {
		LightningResponse lr = new LightningResponse();
		try{
			Contract result =
				[Select
					Account.Id,
					Account.Name,
					ContractNumber,
					Id,
					StartDate,
					EndDate,
					Renewal_Term_End_Date__c,
					CPI_Rate_Types__c,
					CPI_Effective_Date__c,
					Routing_Type__c
				 From
				 	Contract
				 Where
				 	Id =:recordId];
			lr.jsonResponse = JSON.serialize(result);
		}catch(Exception ex){
			lr = new LightningResponse(ex);
		}
		return lr;
	}

	@AuraEnabled
	public static LightningResponse getMetrics(String contractId,YearsWrapper yearsWrapper,Decimal yearLength, boolean readOnly){
		System.debug('JWL: in get metrics');
		LightningResponse lr = new LightningResponse();
		try{
			System.debug('ContractId: ' + contractId);
			Contract currentContract = [Select AccountId,CPI_Effective_Date__c,EndDate From Contract Where Id = :contractId];

			//Contract currentContract = (Contract)JSON.deserialize(currentContractJson,Contract.class);
			yearsWrapper = getYearsWrapper(
				currentContract
				,yearLength
				,yearsWrapper
			);
			System.debug('JWL: year 0 quarters 111: ' + yearsWrapper.years.get(0).quarters);

			//CurrentYearWrapper currentYear = new CurrentYearWrapper(years);
			CPIMetricResults results = new CPIMetricResults(yearsWrapper);
			readOnly |= results.readOnly;
			System.debug('JWL: year 0 quarters 222: ' + yearsWrapper.years.get(0).quarters);
			results.label = yearsWrapper.getLabel();
			//results.quarterLabels = CPIUtils.getQuarterLabels(yearsWrapper.selectedYear,'M/YYYY');

			List<YearWrapper> currentYears = yearsWrapper.years;
			QuarterWrapper firstQuarter = currentYears.get(0).quarters.get(0);
			List<QuarterWrapper> lastYearQuarters = currentYears.get(currentYears.size()-1).quarters;
			QuarterWrapper lastQuarter = lastYearQuarters.get(lastYearQuarters.size()-1);
			System.debug('JWL: year 0 quarters 333: ' + yearsWrapper.years.get(0).quarters);

			//currentContract.AccountId = '1000test';
			System.debug(LoggingLevel.ERROR,'JWL: before metrics callout');

			String accountId = ((String)currentContract.AccountId).left(15);
			List<CPIRESTRequest.CPIMetric> metrics = CPIRESTRequest.getMetrics(accountId,firstQuarter.startDate,lastQuarter.endDate);
			System.debug('JWL: after metrics callout');
			metrics.sort();

			System.debug('CPIM: metrics size: ' + metrics.size());
			Date quarterStartDate = getQuarterStartDate();

			for(CPIRESTRequest.CPIMetric metric : metrics){
				Date metricStartDate = Date.valueOf(metric.startDate);
				metric.readOnly = readOnly || metricStartDate < quarterStartDate;
				results.addMetric(metric);
			}

			System.debug('JWL: year 0 quarters 444: ' + yearsWrapper.years.get(0).quarters);

			/*for(CPIMetricCategory category : results.categories){
				for(CPIMetric currentMetric : category.metrics){
					for(CPIMetricYear metricYear : currentMetric.years){
						for(YearWrapper yw : yearsWrapper.years){
							if(metricYear.year == yw.year){
								List<QuarterWrapper> quartersRemaining = new List<QuarterWrapper>();
								for(QuarterWrapper qw : yw.quarters){
									boolean foundQuarter = false;
									for(CPIRESTRequest.CPIMetric metric : metricYear.quarters){
										Date metricStart = Date.valueOf(metric.startDate);
										Date metricEnd = Date.valueOf(metric.endDate);
										foundQuarter = (metricStart >= qw.startDate && metricEnd <= qw.endDate);
									}

									if(foundQuarter){
										quartersRemaining.add(qw);
									}
								}
								yw.quarters = quartersRemaining;
							}
						}
					}
				}
			}*/

			System.debug('JWL: year 0 quarters 555: ' + yearsWrapper.years.get(0).quarters);

			System.debug('JWL: results: ' + results);
			lr.jsonResponse = JSON.serialize(results);
		}catch(Exception ex){
			System.debug('JWL: ex message: ' + ex.getMessage());
			System.debug('JWL: ex: ' + ex.getCause());
			System.debug('JWL: stacktrace: ' + ex.getStackTraceString());
			lr = new LightningResponse(ex);
		}
		return lr;
	}

	@AuraEnabled
	public static LightningResponse getRates(String contractId,YearsWrapper yearsWrapper,Decimal yearLength, boolean readOnly){
		LightningResponse lr = new LightningResponse();
		try{
			System.debug('JWL: in get rates, contractId: ' + contractId);
			System.debug('JWL: yearLength: ' + yearLength);
			System.debug('JWL: readOnly: ' + readOnly);
			Contract currentContract =
				[Select
					AccountId,
					CPI_Effective_Date__c,
					EndDate,
					CPI_Rate_Types__c,
					CPI_Eligibility_Type__c
				 From
				 	Contract
				 Where
				 	Id = :contractId];

			yearsWrapper = getYearsWrapper(
				currentContract
				,yearLength
				,yearsWrapper
			);
			system.debug('JWL: after getYearsWRapper');

			CPIRateResults results = new CPIRateResults(yearsWrapper);
			//CurrentYearWrapper yearWrapper = new CurrentYearWrapper(years);
			results.label = yearsWrapper.getLabel();
			readOnly |= results.readOnly;
			System.debug('JWL: results label 118 ' + results.label);

			List<YearWrapper> currentYears = yearsWrapper.years;
			QuarterWrapper firstQuarter = currentYears.get(0).quarters.get(0);
			List<QuarterWrapper> lastYearQuarters = currentYears.get(currentYears.size()-1).quarters;
			QuarterWrapper lastQuarter = lastYearQuarters.get(lastYearQuarters.size()-1);
			//String accountId = currentContract.AccountId; //change to currentContract.AccountId after testing
			String accountId = ((String)currentContract.AccountId).left(15);
			//currentContract.AccountId = '1000test';
			List<CPIRESTRequest.CPIRate> rates = CPIRESTRequest.getRates(accountId,firstQuarter.startDate,lastQuarter.endDate);
			System.debug('JWL: rates: ' + rates);

			List<String> contractRateTypePicklistEntries =
				currentContract.CPI_Rate_Types__c != null ?
					currentContract.CPI_Rate_Types__c.split(';') : new List<String>();

			System.debug('JWL: contract type: ' + contractRateTypePicklistEntries);


			//Below we are doing a few things
			//1) Calling out to the CPI metrics service to get a list of all rate types.
			//2) Creating a map of rate type ids to rate types.  This will help determine where the CPI Rates
			//that are returned from the get rates web service call go in the UI
			//3) Creating a map of rate type labels to rate types.  This will help determine default values of CPI Rates
			//that are populated via the "CPI Eligibility Rate Type" field because no rates were found in the web servie call.
			Map<String,CPIRESTREquest.CPIRateType> labelToRateTypeMap = new Map<String,CPIRESTREquest.CPIRateType>();
			Map<Integer,CPIRESTREquest.CPIRateType> rateTypeIdToRateTypeMap = new Map<Integer,CPIRESTRequest.CPIRateType>();

			String rateTypeStr = CPIRESTRequest.getRateTypes();
			List<CPIRESTRequest.CPIRateType> rateTypes =
				(List<CPIRESTREquest.CPIRateType>)JSON.deserialize(rateTypeStr,List<CPIRESTRequest.CPIRateType>.class);

			for(CPIRESTREquest.CPIRateType rateType : rateTypes){
				rateTypeIdToRateTypeMap.put(rateType.rateTypeId,rateType);
			}

			List<CPIRESTRequest.CPIRateType> contractRateTypes = new List<CPIRESTRequest.CPIRateType>();
			for(String contractRateTypePLE : contractRateTypePicklistEntries){
				boolean foundRateType = false;
				for(CPIRESTRequest.CPIRateType rateType : rateTypes){
					if(contractRateTypePLE == rateType.type){
						if(contractRateTypePLE == 'Eligibility'){
							if(rateType.label == ('Eligibility ' + currentContract.CPI_Eligibility_Type__c)){
								contractRateTypes.add(rateType);
								foundRateType = true;
							}
						}else{
							contractRateTypes.add(rateType);
							foundRateType = true;
						}
					}
				}

				if(!foundRateType){
					throw new CPIException('Could not find matching CPI Rate for ' + contractRateTypePLE);
				}
			}


			for(YearWrapper yearWrapper : yearsWrapper.years){
				for(QuarterWrapper qw : yearWrapper.quarters){
					Datetime startDT = Datetime.newInstance(qw.startDate.year(),qw.startDate.month(),qw.startDate.day());
					String qwStartDate = startDT.format('yyyy-MM-dd');
					String qwEndDate = Datetime.newInstance(qw.endDate.year(),qw.endDate.month(),qw.endDate.day()).format('yyyy-MM-dd');
					String qwStartDateFormatted = startDT.format('yyyy-MM-dd\'T00:00:00\'');


					for(CPIRESTRequest.CPIRateType rateType : contractRateTypes){
						//CPIRESTRequest.CPIRateType rateType = labelToRateTypeMap.get(rateTypeLabel);

						//if(rateType == null){
						//	throw new CPIException('Could not find a matching rate type for ' + rateTypeLabel);
						//}

						boolean foundRate = false;
						for(Integer i=0; i < rates.size() && !foundRate; i++){
							foundRate |= (rateType.rateTypeId == rates[i].rateTypeId && rates[i].startDate == qwStartDateFormatted);
						}

						/**
						 * If the CPI Metrics web service call did not return a rate for the given rate type
						 * and time period, then create a default CPI Rate.
						 */
						if(!foundRate){
							CPIRESTRequest.CPIRate rate = new CPIRESTRequest.CPIRate();
							//rate.rateTypeId = rateType.rateTypeId;
							rate.startDate = qwStartDate;
							rate.endDate = qwEndDate;
							rate.accountRate = null;
							rate.rateTypeId = rateType.rateTypeId;
							rate.salesforceAccountId = accountId;
							rates.add(rate);
						}
					}
				}

			}
			System.debug('JWL: rates again: ' + rates);

			Date quarterStartDate = getQuarterStartDate();

			Map<Integer,CPIRate> ratesMap = new Map<Integer,CPIRate>();
			for(CPIRESTRequest.CPIRate rate : rates){
				Date rateStartDate = Date.valueOf(rate.startDate);
				rate.readOnly = readOnly || rateStartDate < quarterStartDate;

				CPIRate currentRate = ratesMap.get(rate.rateTypeId);
				if(currentRate == null){
					currentRate = new CPIRate(rate,rateTypeIdToRateTypeMap);
					ratesMap.put(rate.rateTypeId,currentRate);
				}else{
					currentRate.addRate(rate);
				}
			}
			System.debug('JWL: ratesMap: ' + ratesMap);
			results.rates = ratesMap.values();
			results.rates.sort();

			for(CPIRate rate : results.rates){
				rate.years.sort();
			}

			System.debug('JWL: results: ' + results);

			lr.jsonResponse = JSON.serialize(results);
		}catch(Exception ex){
			System.debug('JWL ex: ' + ex);
			lr = new LightningResponse(ex);
		}
		return lr;
	}

	public class CPIRateYear implements Comparable {
		@AuraEnabled public List<CPIRESTRequest.CPIRate> quarters {get;set;}
		@AuraEnabled public Integer year {get; set;} public Date startOfYear {get;private set;}
		//@AuraEnabled public boolean readOnly {get; set;}
		private boolean yearReadOnly {get; set;}

		public CPIRateYear(Integer year){
			this.year = year;
			this.startOfYear = Date.newInstance(year,1,1);
			this.quarters = new List<CPIRESTRequest.CPIRate>();
			this.yearReadOnly = true;
			//this.readOnly = true;
		}

		public void addRate(CPIRESTRequest.CPIRate rate){
			Integer insertIndex = null;

			this.yearReadOnly &= rate.readOnly;
			rate.yearReadOnly = this.yearReadOnly;

			if(!this.quarters.isEmpty()){
				for(Integer i=0; i < this.quarters.size() && insertIndex == null; i++){
					this.quarters[i].yearReadOnly = this.yearReadOnly;
					if(rate.startDate < this.quarters[i].startDate){
						insertIndex = i;
					}
				}
			}

			//this.readOnly &= rate.readOnly;

			if(insertIndex == null){
				this.quarters.add(rate);
			}else{
				this.quarters.add(insertIndex,rate);
			}
		}

		public Integer compareTo(Object compareTo) {
	        CPIRateYear compareToRY = (CPIRateYear)compareTo;
	        return
	        	(year == compareToRY.year ?
	        		0 :
	        		(year > compareToRY.year ?
	        			1 : - 1
	        		)
	        	);
	    }
	}

	@AuraEnabled
	public static LightningResponse saveRates(String contractId, String ratesJson, String ywJson){
		LightningResponse response = new LightningResponse();
		try{
			if(!isProfileEditable()){
				throw new CPIException('Insufficient Access: Profile cannot edit CPI Metrics');
			}

			//List<CPIRESTRequest.CPIRate> ratesToUpdate = new List<CPIRESTRequest.CPIRate>();

			List<CPIRESTRequest.CPIRateSave> rates = (List<CPIRESTRequest.CPIRateSave>)JSON.deserialize(ratesJson,List<CPIRESTRequest.CPIRateSave>.class);

			Id currentUserId = UserInfo.getUserId();
			for(CPIRESTRequest.CPIRateSave rate : rates){
				rate.updatedBySalesforceUser = currentUserId;
			}

			YearsWrapper yw = (YearsWrapper)JSON.deserialize(ywJson,YearsWrapper.class);

			//Save rates to the warehouse via the webservice.
			String batchId = contractId + '_' + Datetime.now().format('yyyy-MM-dd_HH_mm');
			CPIRESTRequest.saveRates(batchId, rates);

			//Call back to the warehouse to retrive the latest data.
			response = getRates(contractId,yw,yw.years.size(),false);
		}catch(Exception e){
			response = new LightningResponse(e);
		}
		return response;
	}

	@AuraEnabled
	public static LightningResponse saveMetrics(String contractId,String metricsJson,String ywJson){
		LightningResponse response = new LightningResponse();
		try{
			if(!isProfileEditable()){
				throw new CPIException('Insufficient Access: Profile cannot edit CPI Metrics');
			}

			List<CPIRESTRequest.CPIMetricSave> metrics = (List<CPIRESTRequest.CPIMetricSave>)JSON.deserialize(metricsJson,List<CPIRESTRequest.CPIMetricSave>.class);

			Id currentUserId = UserInfo.getUserId();
			for(CPIRESTRequest.CPIMetricSave metric : metrics){
				metric.updatedBySalesforceUser = currentUserId;
			}

			YearsWrapper yw = (YearsWrapper)JSON.deserialize(ywJson,YearsWrapper.class);

			//Save rates to the warehouse via the webservice.
			String batchId = contractId + '_' + Datetime.now().format('yyyy-MM-dd_HH_mm');
			CPIRESTRequest.saveMetrics(batchId, metrics);

			//Call back to the warehouse to retrive the latest data.
			response = getMetrics(contractId,yw,yw.years.size(),false);

			/*List<CPIRESTRequest.CPIMetricSave> metricsToUpdate = new List<CPIRESTRequest.CPIMetricSave>();

			List<CPIMetricSave> metrics = (List<CPIMetricSave>)JSON.deserialize(metricsJson,List<CPIMetricSave>.class);

			YearsWrapper yw = (YearsWrapper)JSON.deserialize(ywJson,YearsWrapper.class);

			String currentUserId = UserInfo.getUserId().substring(0,15);
			Date today = Date.today();
			for(CPIMetricSave metric : metrics){
				CPIRESTRequest.CPIMetricSave current = metric.metric;

				

				Date currentStartDate = Date.valueOf(current.startDateString);
				current.updatedBySalesforceUser = currentUserId;

				if(currentStartDate >= today){
					metricsToUpdate.add(current);
				}

				if(metric.quarters != null){
					for(CPIRESTRequest.CPIMetricSave quarter : metric.quarters){
						currentStartDate = Date.valueOf(quarter.startDateString);
						if(currentStartDate >= today){
							quarter.updatedBySalesforceUser = current.updatedBySalesforceUser;
							quarter.goal = current.goal;
							quarter.weight = current.weight;
							metricsToUpdate.add(quarter);
						}
					}
				}
			}
			//Save rates to the warehouse via the webservice.
			String batchId = contractId + '_' + Datetime.now().format('yyyy-MM-dd:HH:mm');
			CPIRESTRequest.saveMetrics(batchId,metricsToUpdate);

			//Call back to the warehouse to retrive the latest data.
			response = getMetrics(contractId,yw,yw.years.size(),false);
			*/
		}catch(Exception e){
			response = new LightningResponse(e);
		}
		return response;
	}

	private static YearsWrapper getYearsWrapper(Contract currentContract, Decimal recurringYearLength, YearsWrapper previousYearsWrapper){
		Date startDate = currentContract.CPI_Effective_Date__c;
		Date endDate = currentContract.EndDate;

		Decimal selectedYear = Date.today().year();
		if(previousYearsWrapper != null){
			if(endDate != null){
				return previousYearsWrapper;
			}

			List<YearWrapper> years = previousYearsWrapper.years;
			endDate = Date.newInstance(Integer.valueOf(years.get(years.size()-1).year)+1,1,1).addDays(-1);
			selectedYear = previousYearsWrapper.selectedYear;
			if(selectedYear > years.get(0).year && selectedYear < years.get(years.size()-1).year){
				return previousYearsWrapper;
			}


			if(selectedYear <= years.get(0).year && startDate.year() < Integer.valueOf(selectedYear)){
				startDate = Date.newInstance(Integer.valueOf(selectedYear),1,1);
				endDate = Date.newInstance(startDate.year()+Integer.valueOf(recurringYearLength),1,1).addDays(-1);
			}else if(selectedYear >= years.get(years.size()-1).year){
				startDate = Date.newInstance(Integer.valueOf(selectedYear),1,1);
				endDate = Date.newInstance(startDate.year()+Integer.valueOf(recurringYearLength),1,1).addDays(-1);
			}

			System.debug('JWL: getting new years wrapper');
			previousYearsWrapper = new YearsWrapper(startDate,endDate,selectedYear);
			return previousYearsWrapper;
		}

		//selectedYear = Date.today().year();
		if(selectedYear < startDate.year()){
			selectedYear = startDate.year();
		}

		if(endDate == null){
			Date firstDayOfLastYear = Date.newInstance(Date.today().year()-1,1,1);
			if(startDate < firstDayOfLastYear){
				startDate = firstDayOfLastYear;
			}

			endDate = Date.newInstance(startDate.year()+Integer.valueOf(recurringYearLength),1,1).addDays(-1);
		}else{
			if(selectedYear > endDate.year()){
				selectedYear = endDate.year();
			}
		}

		YearsWrapper ans = new YearsWrapper(startDate,endDate,selectedYear);
		return ans;
	}

	private static Date getQuarterStartDate(){
		Date today = Date.today();
		Integer currentMonth = today.month();
		Integer currentQuarterStartMonth = ((currentMonth - 1) / 3) * 3;
		return Date.newInstance(today.year(),currentQuarterStartMonth,1);
	}

	private static boolean isProfileEditable(){
		Profile p = [Select Name From Profile Where Id = :UserInfo.getProfileId()];
		List<CPI_Editable_Profile__mdt> editableProfileSetting = [Select DeveloperName From CPI_Editable_Profile__mdt Where Label = :p.Name];
		return !editableProfileSetting.isEmpty();
	}

	public class CPIRate implements Comparable{
		@AuraEnabled public String rateLabel {get; set;}
		@AuraEnabled public Integer sortOrder {get; set;}
		@AuraEnabled public Integer max {get; set;}
		@AuraEnabled public Integer min {get; set;}
		@AuraEnabled public List<CPIRateYear> years {get; set;}
		//public Integer cpiRateId {get; set;}
		private Map<Integer,CPIRateYear> intToYearMap {get; set;}

		public CPIRate(CPIRESTRequest.CPIRate rate, Map<Integer,CPIRESTRequest.CPIRateType> rateTypeMap){
			this.years = new List<CPIRateYear>();
			this.intToYearMap = new Map<Integer,CPIRateYear>();


			CPIRESTRequest.CPIRateType rateType = rateTypeMap.get(rate.rateTypeId);
			this.rateLabel = rateType.label;
			this.sortOrder = rateType.rateTypeId;
			this.min = rateType.min;
			this.max = rateType.max;

			addRate(rate);
		}

		public void addRate(CPIRESTRequest.CPIRate rate){
			Integer rateStartDate = Date.valueOf(rate.startDate).year();

			CPIRateYear year = this.intToYearMap.get(rateStartDate);
			if(year == null){
				year = new CPIRateYear(rateStartDate);
				this.years.add(year);
				this.intToYearMap.put(rateStartDate,year);
			}

			year.addRate(rate);
		}

		public Integer compareTo(Object compareTo) {
	        CPIRate compareToRY = (CPIRate)compareTo;
	        return
	        	(sortOrder == compareToRY.sortOrder ?
	        		0 :
	        		(sortOrder > compareToRY.sortOrder ?
	        			1 : - 1
	        		)
	        	);
	    }
	}

	public class CPIMetric{
		@AuraEnabled public List<CPIMetricYear> years {get; set;}
		@AuraEnabled public String metricLabel {get; set;}
		private Map<Integer,CPIMetricYear> intToYearMap {get; set;}
		@AuraEnabled public String salesforceDisplayName {get; set;}

		public CPIMetric(CPIRESTRequest.CPIMetric metric){
			this.metricLabel = metric.metricLabel;
			this.salesforceDisplayName = metric.salesforceDisplayName;
			this.years = new List<CPIMetricYear>();
			this.intToYearMap = new Map<Integer,CPIMetricYear>();
			addMetric(metric);
		}

		public void addMetric(CPIRESTRequest.CPIMetric metric){
			Integer metricStartDate = Date.valueOf(metric.startDate).year();

			CPIMetricYear year = this.intToYearMap.get(metricStartDate);
			if(year == null){
				year = new CPIMetricYear(metricStartDate);
				this.years.add(year);
				this.intToYearMap.put(metricStartDate,year);
			}

			year.addMetric(metric);
		}
	}

	public class CPIMetricCategory{
		@AuraEnabled public String categoryName {get; set;}
		@AuraEnabled public List<CPIMetric> metrics {get; set;}
		private Map<Integer,CPIMetric> intToMetricMap {get;set;}

		public CPIMetricCategory(CPIRESTRequest.CPIMetric metric){
			this.metrics = new List<CPIMetric>();
			this.intToMetricMap = new Map<Integer,CPIMetric>();
			this.categoryName = metric.cpiCategory;
			addMetric(metric);
		}

		public void addMetric(CPIRESTRequest.CPIMetric metric){
			CPIMetric currentMetric = intToMetricMap.get(metric.metricTypeId);

			if(currentMetric == null){
				currentMetric = new CPIMetric(metric);
				intToMetricMap.put(metric.metricTypeId,currentMetric);
				this.metrics.add(currentMetric);
			}else{
				currentMetric.addMetric(metric);
			}
		}
	}

	public class CPIMetricYear{
		@AuraEnabled public List<CPIRESTRequest.CPIMetric> quarters {get;set;}
		@AuraEnabled public Integer year {get; set;}
		public Date startOfYear {get;private set;}

		public CPIMetricYear(Integer year){
			this.year = year;
			this.startOfYear = Date.newInstance(year,1,1);
			this.quarters = new List<CPIRESTRequest.CPIMetric>();
		}

		public void addMetric(CPIRESTRequest.CPIMetric metric){

			Integer insertIndex = null;

			if(!this.quarters.isEmpty()){
				for(Integer i=0; i < this.quarters.size() && insertIndex == null; i++){
					if(metric.startDate < this.quarters[i].startDate){
						insertIndex = i;
					}
				}
			}

			if(insertIndex == null){
				this.quarters.add(metric);
			}else{
				this.quarters.add(insertIndex,metric);
			}
		}
	}

	public class CPIMetricResults{
		@AuraEnabled public YearsWrapper yearsWrapper {get; set;}
		@AuraEnabled public List<CPIMetricCategory> categories {get; set;}
		@AuraEnabled public String label {get; set;}
		@AuraEnabled public boolean readOnly {get; set;}

		private Map<String,CPIMetricCategory> categoryMap {get; set;}

		public CPIMetricResults(YearsWrapper yearsWrapper){
			this.yearsWrapper = yearsWrapper;
			this.categories = new List<CPIMetricCategory>();
			this.categoryMap = new Map<String,CPIMetricCategory>();
			this.readOnly = !isProfileEditable();
		}

		public void addMetric(CPIRESTRequest.CPIMetric metric){
			CPIMetricCategory category = categoryMap.get(metric.cpiCategory);
			if(category == null){
				category = new CPIMetricCategory(metric);
				this.categoryMap.put(category.categoryName,category);
				this.categories.add(category);
			}else{
				category.addMetric(metric);
			}
		}
	}

	public class CPIRateResults{
		@AuraEnabled public List<CPIRate> rates {get; set;}
		@AuraEnabled public String label {get; set;}
		@AuraEnabled public YearsWrapper yearsWrapper {get; set;}
		@AuraEnabled public boolean readOnly {get; set;}

		public CPIRateResults(YearsWrapper yearsWrapper){
			this.rates = new List<CPIRate>();
			this.yearsWrapper = yearsWrapper;
			this.readOnly = !isProfileEditable();
		}
	}

	public class YearsWrapper{
		@AuraEnabled public Decimal selectedYear {get; set;}
		@AuraEnabled public List<YearWrapper> years {get; set;}

		public YearsWrapper(Date startDate, Date endDate,Decimal selectedYear){
			this.selectedYear = selectedYear;
			this.years = new List<YearWrapper>();

			Set<Integer> quarterMonths = new Set<Integer>{1,4,7,10};

			System.debug('JWL: startDate: ' + startDate);
			System.debug('JWL: endDate: ' + endDate);

			if(startDate > endDate){
				return;
			}

			Date currentDate = startDate;
			YearWrapper currentYearWrapper = null;
			while(currentDate < endDate){
				System.debug('JWL: currentDate: ' + currentDate);
				Integer currentDateYear = currentDate.year();
				if(currentYearWrapper == null || currentDate.month() == 1){
					currentYearWrapper = new YearWrapper(currentDateYear);
					years.add(currentYearWrapper);
				}

				Date quarterStartDate = currentDate;
				Integer currentDateMonth = currentDate.month();
				boolean customQuarter = false;

				System.debug('JWL: currentDateMonth: ' + currentDateMonth);
				System.debug('JWL: currentDate: ' + currentDate.day());
				if(!quarterMonths.contains(currentDateMonth) || currentDate.day() != 1){
					customQuarter = true;
					while(!quarterMonths.contains(currentDateMonth)){
						currentDateMonth--;
					}
					quarterStartDate = Date.newInstance(Integer.valueOf(currentYearWrapper.year),currentDateMonth,1);
				}

				Integer nextQuarterMonth = ((((currentDate.month() - 1) / 3) + 1) * 3) + 1;
				Integer nextQuarterYear = currentDate.year();

				if(nextQuarterMonth >= 13){
					nextQuarterMonth = 1;
					nextQuarterYear++;
				}

				Date nextQuarterStartDate = Date.newInstance(nextQuarterYear,nextQuarterMonth,1);
				Date quarterEndDate = nextQuarterStartDate.addDays(-1);
				currentYearWrapper.addQuarter(currentDate,quarterEndDate,customQuarter);

				if(nextQuarterMonth == 1){
					System.debug('JWL: before advancing to new year...');
					System.debug('JWL: quarters of this year: ' + currentYearWrapper.quarters);
				}

				System.debug('JWL: nextQuarterStartDate: ' + nextQuarterStartDate);

				currentDate = nextQuarterStartDate;
			}
		}

		public String getLabel(){
			return years.isEmpty() ? '' : (years.size() == 1 ? years.get(0).year + '' : years.get(0).year + ' - ' + years.get(years.size()-1).year);
		}
	}

	public class YearWrapper{
		@AuraEnabled public Decimal year {get; set;}
		@AuraEnabled public List<QuarterWrapper> quarters {get; set;}

		public YearWrapper(Integer year){
			this.year = year;
			this.quarters = new List<QuarterWrapper>();
		}

		public void addQuarter(Date startDate, Date endDate, boolean customQuarter){
			this.quarters.add(new QuarterWrapper(startDate, endDate, customQuarter));
		}
	}

	public class QuarterWrapper{
		public Date startDate {get; set;}
		public Date endDate {get; set;}
		public String label {get; set;}
		public String startDateString {get; set;}

		public QuarterWrapper(Date startDate, Date endDate, boolean customQuarter){
			Datetime dt = Datetime.newInstance(startDate.year(),startDate.month(),startDate.day());
			this.startDate = startDate;
			this.startDateString = dt.format('yyyy-MM-dd');
			this.endDate = endDate;

			Integer month = dt.month();
			if(month <= 3){
				this.label = 'Q1 ';
			}else if(month <= 6){
				this.label = 'Q2 ';
			}else if(month <= 9){
				this.label = 'Q3 ';
			}else{
				this.label = 'Q4 ';
			}

			if(customQuarter){
				this.label += dt.format('M/d/yyyy');
			}else{
				this.label += dt.format('M/yyyy');
			}
		}
	}

	public class CPIMetricSave{
		@AuraEnabled
		public CPIRESTRequest.CPIMetricSave metric {get; set;}
		@AuraEnabled
		public List<CPIRESTRequest.CPIMetricSave> quarters {get;set;}
	}
}