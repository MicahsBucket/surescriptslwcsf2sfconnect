@isTest(SeeAllData=true)
public class SfdcS2SCaseSyncQueueableTest {
    
    private static SfdcS2SRemoteCalloutUtil calloutUtil;
    private static SfdcS2SOAuth2Controller controller = new SfdcS2SOAuth2Controller();
    
    static {
        calloutUtil = (SfdcS2SRemoteCalloutUtil) Test.createStub(SfdcS2SRemoteCalloutUtil.class, new SfdcS2SMocks.SfdcS2SRemoteCalloutUtilMock());
        SfdcS2SCaseSyncQueueable.calloutUtil = calloutUtil;
        SfdcS2SOAuth oauth = (SfdcS2SOAuth) Test.createStub(SfdcS2SOAuth.class, new SfdcS2SMocks.SfdcS2SOAuthMock());
        controller.setSfdcS2SOAuthMock(oauth);
        SfdcS2SCaseSyncQueueable.controller = controller;
    }
    
       
    @isTest static void testDeserialize() {
        String testData = '{ "totalSize": 1, "done": true, "records": [ { "attributes": { "type": "FeedItem", "url": "/services/data/v40.0/sobjects/FeedItem/0D5f4000005tk7tCAA" }, "Body": "<p>feed item inserted on 11pm</p>", "CreatedDate": "2017-08-08T06:53:33.000+0000" } ] }';
        SfdcS2SRemoteCalloutUtil callout = new SfdcS2SRemoteCalloutUtil();
        List<Object> records = callout.deserialize(testData);
        System.assert(records != null);
        System.assertEquals(1, records.size());
    }
    
    
    @isTest static void testExecuteRemoteQuery() {
        
        SfdcS2SCaseSyncQueueable sync = new SfdcS2SCaseSyncQueueable();
        sync.execute(null);
        
        SfdcS2SRemoteCalloutUtil callout = new SfdcS2SRemoteCalloutUtil();
        callout.doCallout = false;
        
        SfdcS2SOAuth oauth = new SfdcS2SOAuth();
        oauth.access_token = 'abc';
        oauth.instance_url = 'https://test.org';
        System.assert(callout.executeRemoteQuery('SELECT Id From Case', oauth) == null);
    }
    
    
    @isTest static void testInsertFeedItem() {
        List<Case> cases = SfdcS2SCaseSyncQueueable.getOpenCasesFromMyOrg();
        if (cases.size() > 0) {
            FeedItem newFeedItem = SfdcS2SCaseSyncQueueable.insertFeedItem(cases.get(0).Id, 'foobar', false);
            System.assert(newFeedItem != null);
            System.assert(newFeedItem.Id == null);
        }
    }
    
    @isTest static void testIsNewFeedItem() {
        FeedItem item1 = new FeedItem(Body = 'foo');
        List<FeedItem> items = new List<FeedItem>();
        
        System.assertEquals(true, SfdcS2SCaseSyncQueueable.isNewFeedItem(item1, items));
        
        items.add(item1);
        System.assertEquals(false, SfdcS2SCaseSyncQueueable.isNewFeedItem(item1, items));
    }
}