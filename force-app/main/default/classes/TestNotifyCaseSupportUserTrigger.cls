@isTest
Private Class TestNotifyCaseSupportUserTrigger
{
    public static testMethod void TestCaseCommentForEmail()
    {
     Test.startTest();
     
     Case cs = new Case();
     cs.Email_address__c = 'test@test.com';
     cs.Subject = 'test1';
     insert cs;
     CaseComment ccm = new CaseComment(ParentId=cs.id);
     ccm.IsPublished = true; 
     ccm.CommentBody = 'test1';
     insert ccm;
     ccm.CommentBody = 'test2';
     update ccm;
     
     Test.StopTest();
    }

}