@isTest
public class SfdcS2SOAuth2ControllerTest {
	
    private static SfdcS2SOAuth2Controller controller = new SfdcS2SOAuth2Controller();
    
    static {
        SfdcS2SOAuth oauth = (SfdcS2SOAuth) Test.createStub(SfdcS2SOAuth.class, new SfdcS2SMocks.SfdcS2SOAuthMock());
        controller.setSfdcS2SOAuthMock(oauth);
    }
    
    @isTest static void testAuthorize() {
       
        PageReference reference = controller.authorize();
        String url = reference.getUrl();
        System.assert(url != null);
        System.assert(url.contains('/services/oauth2/authorize'));
    }
    
    @isTest static void testRefreshNoData() {
        PageReference reference = controller.refresh();
        System.assert(reference != null);
    }
    
    @isTest(SeeAllData=true) static void testRefresh() {
        SFDC_S2S_OAuth2Tokens__c tokens = SFDC_S2S_OAuth2Tokens__c.getOrgDefaults();
        if (tokens != null) {
           System.assert(controller.refresh() == null); 
        }
    }
    
    @isTest static void testCallback() {
        System.assert(controller.callback() == null);
        System.assert(controller.getAuthStatus() == null);
    }
   
    @isTest(SeeAllData=true) static void testUpdateTokens() {
        SFDC_S2S_OAuth2Tokens__c tokens = SFDC_S2S_OAuth2Tokens__c.getOrgDefaults();
        if (tokens != null) {
            //load the existing data
            String refreshToken = tokens.RefreshToken__c;
            String instanceUrl = tokens.InstanceUrl__c;
            System.assert(refreshToken != null);
            System.assert(instanceUrl != null);
            
            //make an update with dummy access token and invalid refresh token
            SfdcS2SOAuth oauth = new SfdcS2SOAuth();
            System.assert(oauth.id == null);
            System.assert(oauth.issued_at == null);
            System.assert(oauth.instance_url == null);
            System.assert(oauth.signature == null);
            
            oauth.refresh_token = null;
            oauth.instance_url = instanceUrl;
            
            SfdcS2SOAuth.updateTokens(oauth);
            tokens = SFDC_S2S_OAuth2Tokens__c.getOrgDefaults();
            //null refresh token shouldn't override the valid one
            System.assertEquals(tokens.RefreshToken__c, refreshToken);
            
            //reset 
            oauth.refresh_token = refreshToken;
            SfdcS2SOAuth.updateTokens(oauth);
            tokens = SFDC_S2S_OAuth2Tokens__c.getOrgDefaults();
            System.assertEquals(tokens.RefreshToken__c, refreshToken);
        }
    }
    
    @isTest static void testDeserializeOAuth() {
        String testData = '{"access_token":"00Df40000002Uyi!AQQAQPWlN2HJI.XwoffN.xXyBEa.0XEdRTdsSaeUzMv4yakEPQMuMPP44yzRdNi_i6g2TNBCiRQGBgbgnaoZAERmGFdcva.D","refresh_token":"5Aep861UTWIWNgl0keW09e6e4Gho7fX6HHoGuCKPNoTZ6QvlZYwkO6.1oSNqJ42wJvwMfAEnli0DGQomCcI09ge","signature":"B3wx/SeQ1eTSYAYOLbb/Nx59GIuZsJj6NQBlbpmcSA8=","scope":"refresh_token chatter_api api","instance_url":"https://sc-labs-dev-1-dev-ed.my.salesforce.com","id":"https://login.salesforce.com/id/00Df40000002UyiEAE/005f4000000MFXWAA4","token_type":"Bearer","issued_at":"1502145327524"}"';
        SfdcS2SOAuth oauth = SfdcS2SOAuth.deserialize(testData);
        System.assert(oauth != null);
        System.assertEquals(oauth.access_token, '00Df40000002Uyi!AQQAQPWlN2HJI.XwoffN.xXyBEa.0XEdRTdsSaeUzMv4yakEPQMuMPP44yzRdNi_i6g2TNBCiRQGBgbgnaoZAERmGFdcva.D');  
        
        testData = '{"foo":"bar"}';
        oauth = SfdcS2SOAuth.deserialize(testData);
        System.assert(oauth != null);
        System.assert(oauth.access_token == null);
    }
    
    @isTest static void testPostTokenEndpoint() {
        SfdcS2SOAuth oauth = new SfdcS2SOAuth();
        oauth.doCallout = false;
        oauth.callbackResponse = '{}';
       	oauth = oauth.postTokenEndpoint('http://test.org', 'dummy', false);
        System.assert(oauth.access_token == null);
    }
    
    
}