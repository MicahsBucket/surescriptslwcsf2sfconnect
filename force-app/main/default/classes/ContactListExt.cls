/*
	Purpose: used to override the contact list page on the community layout.

	01/10/2017  MD@IC  Created (00140354)
	01/19/2017  MD@IC  Add sortable columns, field set support, finish restyling page (00140354)
*/
public with sharing class ContactListExt{

	public ApexPages.StandardSetController ctrl{get;set;}
	public Boolean mustReviewContacts{get;set;}
	public String contactId{get;set;}
	public List<String> fields{get;set;}
	public String sortField{get;set;}
	public String newSortField{get;set;}
	public String sortOrder{get;set;}
	public Id accountId{get;set;}

	public ContactListExt(){
		User u = [SELECT Id, ContactId, Contact.AccountId FROM User WHERE Id = :UserInfo.getUserId()];
		accountId = u.ContactId != null ? u.Contact.AccountId : null;

		init();
	}

	public void init(){
		Set<String> fieldSet = new Set<String>{'Id'};
		for(Schema.FieldSetMember f : SObjectType.Contact.FieldSets.Community_Override_List.getFields())
			fieldSet.add(f.getFieldPath());
		fields = new List<String>(fieldSet);

		sortField = 'Name';
		sortOrder = 'ASC';

		if(accountId != null){
			ctrl = new ApexPages.StandardSetController(getQueryLocator());
			setMustReviewContacts();
		}
	}

	public void setMustReviewContacts(){
		List<Contact> contacts = [SELECT Id FROM Contact WHERE AccountId = :accountId AND Contact_Status__c = 'Active' AND Reviewed_by_Customer__c = false LIMIT 1];
		mustReviewContacts = !contacts.isEmpty();
	}

	public List<Contact> getContacts(){
		return ctrl.getRecords();
	}

	public PageReference newContact(){
		PageReference pgRef = Page.Contact;
		pgRef.getParameters().put('mode', 'new');
		return pgRef;
	}

	/*public PageReference editContact(){
		PageReference pgRef = Page.Contact;
		pgRef.getParameters().put('mode', 'edit');
		pgRef.getParameters().put('id', contactId);
		System.assert(contactId != null, 'n');
		return pgRef;
	}*/

	public PageReference viewContact(){
		PageReference pgRef = Page.Contact;
		pgRef.getParameters().put('mode', 'view');
		pgRef.getParameters().put('id', contactId);
		return pgRef;
	}

	public void sort(){
		if(newSortField == sortField){
			if(sortOrder == 'ASC') sortOrder = 'DESC';
			else sortOrder = 'ASC';
		}
		else{
			sortField = newSortField;
			sortOrder = 'ASC';
		}
		ctrl = new ApexPages.StandardSetController(getQueryLocator());
	}

	public Database.QueryLocator getQueryLocator(){
		System.assert(accountId != null, 'no accountId');
		return Database.getQueryLocator('SELECT ' + String.join(fields, ',') + ' FROM Contact WHERE AccountId = \'' + accountId + '\' AND Contact_Status__c = \'Active\' ORDER BY ' + sortField + ' ' + sortOrder + (sortOrder == 'DESC' ? ' NULLS LAST' : ''));
	}
}