/**
 * CPIMetricsCtrlTest
 * @description Test class for CPIMetricsCtrl
 * @author  Demand Chain Systems (James Loghry)
 * @date  4/10/2017
 */
@isTest
private class CPIMetricsCtrlTest{

	@testsetup
	static void setup(){
		Id recordTypeId = [Select Id From RecordType Where DeveloperName = 'CPI_Contract'].Id;

		Account a = new Account(
			Name='Test'
			,Legal_name_of_entity__c = 'Test'
			,State_of_Incorporation__c = 'MN');
		insert a;

		Contract c = new Contract(
			AccountId = a.Id
			,CPI_Effective_Date__c = Date.newInstance(2017,4,5)
			,CPI_Rate_Types__c = 'Eligibility;Loyalty;CPI'
			,CPI_Eligibility_Type__c = 'Percentage'
			,StartDate = Date.newInstance(2017,4,5)
			,EndDate = Date.newInstance(2023,12,31)
			,RecordTypeId = recordTypeId
		);
		insert c;
	}

	static testmethod void testGetContract(){
		LightningResponse response = null;
		Test.startTest();
		Id currentContractId = [Select Id From Contract].Id;
		response = CPIMetricsCtrl.getContract(currentContractId);
		Test.stopTest();

		System.assertNotEquals(null,response,'Received null response');

		Contract resultingContract = (Contract)JSON.deserialize(response.jsonResponse,Contract.class);
		System.assertEquals(currentContractId,resultingContract.Id);
	}

	static testmethod void testGetContractError(){
		LightningResponse response = null;
		Test.startTest();
		response = CPIMetricsCtrl.getContract(null);
		Test.stopTest();

		System.assertNotEquals(null,response,'Received null response');
		System.assertNotEquals(null,response.errors);
		System.assert(!response.errors.isEmpty(),'Errors is');
	}

	static testmethod void testGetMetricsException(){
		Id currentContractId = [Select Id From Contract].Id;
		Test.setMock(HttpCalloutMock.class,new CPIMockResponse(null,new List<String>{'error'}));
		Test.startTest();
		LightningResponse response = CPIMetricsCtrl.getMetrics(currentContractId,  null, 3, false);
		Test.stopTest();
		System.assertEquals(1,response.errors.size());
	}

	static testmethod void testGetRatesException(){
		Id currentContractId = [Select Id From Contract].Id;
		Test.setMock(HttpCalloutMock.class,new CPIMockResponse(null,new List<String>{'error'}));
		Test.startTest();
		LightningResponse response = CPIMetricsCtrl.getRates(currentContractId,  null, 3, false);
		Test.stopTest();

		System.assertEquals(1,response.errors.size());
	}

	static testmethod void testGetMetrics(){
		Date startDate = Date.newInstance(2052,1,1);
		Date endDate = startDate.addMonths(3).addDays(-1);

		Id accountId = [Select Id From Account].Id;

		List<CPIRESTRequest.CPIMetric> metrics = new List<CPIRESTRequest.CPIMetric>();
		for(Integer i=0; i < 11; i++){
			CPIRESTRequest.CPIMetric metric = new CPIRESTRequest.CPIMetric();
			metric.metricId = i;
			metric.metricTypeId = i;
			startDate = startDate.addMonths(3);
			endDate = startDate.addMonths(3).addDays(-1);
			metric.startDate = Datetime.newInstance(startDate.year(),startDate.month(),startDate.day()).format('yyyy-MM-dd');
			metric.endDate = Datetime.newInstance(endDate.year(),endDate.month(),endDate.day()).format('yyyy-MM-dd');
			metric.hasGoal = true;
			metric.hasWeight = true;
			metric.goal = 100;
			metric.weight = 100;
			metric.salesforceAccountId = accountId;
			metric.cpiCategory = 'RX';
			metrics.add(metric);
		}

		Id currentContractId = [Select Id From Contract].Id;

		LightningResponse response = null;
		Test.setMock(HttpCalloutMock.class,new CPIMockResponse(new List<Object>{metrics}));

		Test.startTest();
		System.debug('JWL: before metrics');
		System.debug(LoggingLevel.ERROR,'JWL2: before metrics');
		response = CPIMetricsCtrl.getMetrics(currentContractId,null,3,false);
		System.debug('JWL: after metrics');
		Test.stopTest();

		System.assertNotEquals(null,response);
		System.assertNotEquals(null,response.jsonResponse);
		CPIMetricsCtrl.CPIMetricResults results = (CPIMetricsCtrl.CPIMetricResults)JSON.deserialize(response.jsonResponse,CPIMetricsCtrl.CPIMetricResults.class);
		System.assert(!results.categories.isEmpty());
	}

	static testmethod void testGetRatesNoneExisting(){
		Date startDate = Date.newInstance(2052,1,1);
		Date endDate = startDate.addMonths(3).addDays(-1);

		Id accountId = [Select Id From Account].Id;

		List<CPIRESTREquest.CPIRateType> rateTypes = new List<CPIRESTRequest.CPIRateType>();
		CPIRESTRequest.CPIRateType rateType = new CPIRESTRequest.CPIRateType();
		rateType.label = 'CPI';
		rateType.mandatory = true;
		rateType.max = 2;
		rateType.min = 0;
		rateType.rateTypeId = 1;
		rateType.type = 'CPI';
		rateTypes.add(rateType);
		CPIRESTRequest.CPIRateType rateType2 = new CPIRESTRequest.CPIRateType();
		rateType2.label = 'Eligibility Percentage';
		rateType2.mandatory = true;
		rateType2.max = 2;
		rateType2.min = 0;
		rateType2.rateTypeId = 2;
		rateType2.type = 'Eligibility';
		rateTypes.add(rateType2);
		CPIRESTRequest.CPIRateType rateType3 = new CPIRESTRequest.CPIRateType();
		rateType3.label = 'Loyalty';
		rateType3.mandatory = true;
		rateType3.max = 2;
		rateType3.min = 0;
		rateType3.rateTypeId = 3;
		rateType3.type = 'Loyalty';
		rateTypes.add(rateType3);

		List<CPIRESTRequest.CPIRate> rates = new List<CPIRESTRequest.CPIRate>();
		Id currentContractId = [Select Id From Contract].Id;
		LightningResponse response = null;
		Test.setMock(HttpCalloutMock.class,new CPIMockResponse(new List<Object>{rates,rateTypes}));

		Test.startTest();
		response = CPIMetricsCtrl.getRates(currentContractId, null, 3, false);
		Test.stopTest();

		System.assertNotEquals(null,response);
		System.assertNotEquals(null,response.jsonResponse);
		CPIMetricsCtrl.CPIRateResults results = (CPIMetricsCtrl.CPIRateResults)JSON.deserialize(response.jsonResponse,CPIMetricsCtrl.CPIRateResults.class);
		System.assert(!results.rates.isEmpty());
	}

	static testmethod void testGetRates(){
		Date startDate = Date.newInstance(2052,1,1);
		Date endDate = startDate.addMonths(3).addDays(-1);

		Id accountId = [Select Id From Account].Id;

		List<CPIRESTREquest.CPIRateType> rateTypes = new List<CPIRESTRequest.CPIRateType>();
		CPIRESTRequest.CPIRateType rateType = new CPIRESTRequest.CPIRateType();
		rateType.label = 'CPI';
		rateType.mandatory = true;
		rateType.max = 2;
		rateType.min = 0;
		rateType.rateTypeId = 1;
		rateType.type = 'CPI';
		rateTypes.add(rateType);
		CPIRESTRequest.CPIRateType rateType2 = new CPIRESTRequest.CPIRateType();
		rateType2.label = 'Eligibility Percentage';
		rateType2.mandatory = true;
		rateType2.max = 2;
		rateType2.min = 0;
		rateType2.rateTypeId = 2;
		rateType2.type = 'Eligibility';
		rateTypes.add(rateType2);
		CPIRESTRequest.CPIRateType rateType3 = new CPIRESTRequest.CPIRateType();
		rateType3.label = 'Loyalty';
		rateType3.mandatory = true;
		rateType3.max = 2;
		rateType3.min = 0;
		rateType3.rateTypeId = 3;
		rateType3.type = 'Loyalty';
		rateTypes.add(rateType3);

		List<CPIRESTRequest.CPIRate> rates = new List<CPIRESTRequest.CPIRate>();
		for(Integer i=0; i < 11; i++){
			CPIRESTRequest.CPIRate rate = new CPIRESTRequest.CPIRate();
			rate.rateId = String.valueOf(i);
			startDate = startDate.addMonths(3);
			endDate = startDate.addMonths(3).addDays(-1);
			rate.startDate = Datetime.newInstance(startDate.year(),startDate.month(),startDate.day()).format('yyyy-MM-dd');
			rate.endDate = Datetime.newInstance(endDate.year(),endDate.month(),endDate.day()).format('yyyy-MM-dd');
			rate.accountRate = 100;
			rate.rateTypeId = 1;
			rate.salesforceAccountId = accountId;
			rates.add(rate);
		}

		Id currentContractId = [Select Id From Contract].Id;

		LightningResponse response = null;
		Test.setMock(HttpCalloutMock.class,new CPIMockResponse(new List<Object>{rates,rateTypes}));

		Test.startTest();
		response = CPIMetricsCtrl.getRates(currentContractId, null, 3, false);
		Test.stopTest();

		System.assertNotEquals(null,response);
		System.assertNotEquals(null,response.jsonResponse);
		CPIMetricsCtrl.CPIRateResults results = (CPIMetricsCtrl.CPIRateResults)JSON.deserialize(response.jsonResponse,CPIMetricsCtrl.CPIRateResults.class);
		System.assert(!results.rates.isEmpty());
	}

	static testmethod void testSaveMetrics(){
		Date startDate = Date.newInstance(2052,1,1);
		Date endDate = startDate.addMonths(3).addDays(-1);

		Id accountId = [Select Id From Account].Id;

		List<CPIRESTRequest.CPIMetric> metrics = new List<CPIRESTRequest.CPIMetric>();
		for(Integer i=0; i < 11; i++){
			CPIRESTRequest.CPIMetric metric = new CPIRESTRequest.CPIMetric();
			metric.metricId = i;
			metric.metricTypeId = i;
			startDate = startDate.addMonths(3);
			endDate = startDate.addMonths(3).addDays(-1);
			metric.startDate = Datetime.newInstance(startDate.year(),startDate.month(),startDate.day()).format('yyyy-MM-dd');
			metric.endDate = Datetime.newInstance(endDate.year(),endDate.month(),endDate.day()).format('yyyy-MM-dd');
			metric.hasGoal = true;
			metric.hasWeight = true;
			metric.goal = 100;
			metric.weight = 100;
			metric.salesforceAccountId = accountId;
			metric.cpiCategory = 'RX';
			metrics.add(metric);
		}

		Id currentContractId = [Select Id From Contract].Id;

		LightningResponse response = null;
		Test.setMock(HttpCalloutMock.class,
			new CPIMockResponse(
				new List<Object>{
					metrics
					,'SUCCESS'
					,metrics
					,'SUCCESS'
				}
			)
		);

		Test.startTest();
		System.debug('JWL: sending first callout');
		response = CPIMetricsCtrl.getMetrics(currentContractId, null, 3, false);
		CPIMetricsCtrl.CPIMetricResults GETResults = (CPIMetricsCtrl.CPIMetricResults)JSON.deserialize(response.jsonResponse,CPIMetricsCtrl.CPIMetricResults.class);
		GETResults.categories.get(0).metrics.get(0).years.get(0).quarters.get(0).goal = 50;

		List<CPIRESTRequest.CPIMetricSave> metricSaves = new List<CPIRESTRequest.CPIMetricSave>();
		for(CPIMetricsCtrl.CPIMetricCategory category : GETResults.categories){
			for(CPIMetricsCtrl.CPIMetric metric : category.metrics){
				for(CPIMetricsCtrl.CPIMetricYear year : metric.years){
					for(CPIRESTRequest.CPIMetric quarter : year.quarters){
						CPIRESTRequest.CPIMetricSave metricSave = new CPIRESTRequest.CPIMetricSave();
						metricSave.goal = quarter.goal;
						metricSave.weight = quarter.weight;
						metricSave.metricId = quarter.metricId;
						metricSaves.add(metricSave);
					}
				}
			}
		}
		String metricSaveJson = JSON.serialize(metricSaves);

		Date thisYear = Date.newInstance(Date.today().year(),1,1);
		Date startDateYW = thisYear.addYears(-1);
		Date endDateYW = thisYear.addYears(1).addDays(-1);
		CPIMetricsCtrl.YearsWrapper yw = new CPIMetricsCtrl.YearsWrapper(startDateYW,endDateYW,thisYear.year());
		String ywJson = JSON.serialize(yw);

		LightningResponse yearResponse = CPIMetricsCtrl.saveMetrics(currentContractId,metricSaveJson,ywJson);
		Test.stopTest();

		System.assertNotEquals(null,yearResponse);
		System.assertNotEquals(null,yearResponse.jsonResponse);
	}

	static testmethod void testSaveRates(){
		Date startDate = Date.newInstance(2052,1,1);
		Date endDate = startDate.addMonths(3).addDays(-1);

		Id accountId = [Select Id From Account].Id;

		List<CPIRESTREquest.CPIRateType> rateTypes = new List<CPIRESTRequest.CPIRateType>();
		CPIRESTRequest.CPIRateType rateType = new CPIRESTRequest.CPIRateType();
		rateType.label = 'CPI';
		rateType.mandatory = true;
		rateType.max = 2;
		rateType.min = 0;
		rateType.rateTypeId = 1;
		rateType.type = 'CPI';
		rateTypes.add(rateType);
		CPIRESTRequest.CPIRateType rateType2 = new CPIRESTRequest.CPIRateType();
		rateType2.label = 'Eligibility Percentage';
		rateType2.mandatory = true;
		rateType2.max = 2;
		rateType2.min = 0;
		rateType2.rateTypeId = 2;
		rateType2.type = 'Eligibility';
		rateTypes.add(rateType2);
		CPIRESTRequest.CPIRateType rateType3 = new CPIRESTRequest.CPIRateType();
		rateType3.label = 'Loyalty';
		rateType3.mandatory = true;
		rateType3.max = 2;
		rateType3.min = 0;
		rateType3.rateTypeId = 3;
		rateType3.type = 'Loyalty';
		rateTypes.add(rateType3);

		List<CPIRESTRequest.CPIRate> rates = new List<CPIRESTRequest.CPIRate>();
		for(Integer i=0; i < 11; i++){
			CPIRESTRequest.CPIRate rate = new CPIRESTRequest.CPIRate();
			rate.rateId = String.valueOf(i);
			startDate = startDate.addMonths(3);
			endDate = startDate.addMonths(3).addDays(-1);
			rate.startDate = Datetime.newInstance(startDate.year(),startDate.month(),startDate.day()).format('yyyy-MM-dd');
			rate.endDate = Datetime.newInstance(endDate.year(),endDate.month(),endDate.day()).format('yyyy-MM-dd');
			rate.accountRate = 100;
			rate.rateTypeId = 1;
			rate.salesforceAccountId = accountId;
			rates.add(rate);
		}

		Id currentContractId = [Select Id From Contract].Id;
		Contract currentContract = (Contract)JSON.deserialize(
			CPIMetricsCtrl.getContract(currentContractId).jsonResponse,
			Contract.class
		);
		String currentContractJson = JSON.serialize(currentContract);

		LightningResponse response = null;
		Test.setMock(HttpCalloutMock.class,
			new CPIMockResponse(
				new List<Object>{
					rates
					,rateTypes
					,'SUCCESS'
					,rates
					,rateTypes
					,'SUCCESS'
				}
			)
		);

		Test.startTest();
		System.debug('JWL: sending first callout');
		response = CPIMetricsCtrl.getRates(currentContractId, null, 3, false);
		CPIMetricsCtrl.CPIRateResults GETResults = (CPIMetricsCtrl.CPIRateResults)JSON.deserialize(response.jsonResponse,CPIMetricsCtrl.CPIRateResults.class);
		GETResults.rates.get(0).years.get(0).quarters.get(0).accountRate = 50;


		List<CPIRESTRequest.CPIRateSave> rateSaves = new List<CPIRESTRequest.CPIRateSave>();
		for(CPIMetricsCtrl.CPIRate rate : GETResults.rates){
			for(CPIMetricsCtrl.CPIRateYear year : rate.years){
				for(CPIRESTRequest.CPIRate quarter : year.quarters){
					CPIRESTRequest.CPIRateSave rateSave = new CPIRESTRequest.CPIRateSave();
					rateSave.salesforceAccountId = quarter.salesforceAccountId;
					rateSave.accountRate = quarter.accountRate;
					rateSave.startDate = quarter.startDate;
					rateSave.endDate = quarter.endDate;
					rateSave.rateTypeId = quarter.rateTypeId;
					rateSave.rateId = quarter.rateId;
					rateSaves.add(rateSave);
				}
			}
		}
		String rateSaveJson = JSON.serialize(rateSaves);

		Date thisYear = Date.newInstance(Date.today().year(),1,1);
		Date startDateYW = thisYear.addYears(-1);
		Date endDateYW = thisYear.addYears(1).addDays(-1);
		CPIMetricsCtrl.YearsWrapper yw = new CPIMetricsCtrl.YearsWrapper(startDateYW,endDateYW,thisYear.year());
		String ywJson = JSON.serialize(yw);


		LightningResponse yearResponse = CPIMetricsCtrl.saveRates(currentContractId,rateSaveJson,ywJson);
		Test.stopTest();

		System.assertNotEquals(null,yearResponse);
		System.assertNotEquals(null,yearResponse.jsonResponse);
	}
}