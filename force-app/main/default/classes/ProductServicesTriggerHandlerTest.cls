@isTest 
public class ProductServicesTriggerHandlerTest {
   @isTest
   public static void updateProductServicesRecordTest() {
       //Create account to create contract 
       CPQ_TestsA.initialSetup();
       CPQ_TestsA.setupProducts();
       CPQ_TestsA.setupPricebookEntries();
       CPQ_TestsA.setupQuotes(false, false);
       
    //    CPQ_TestsA.setupContractForRenewal('Order');
       List<Product2> productList = [
           SELECT Id, Name, SBQQ__ChargeType__c, SBQQ__BillingType__c, SBQQ__BillingFrequency__c
           FROM Product2
           LIMIT 2
       ];

        List<SBQQ__Quote__c> quoteList = [
           SELECT Id, SBQQ__Account__c,
                (
                    SELECT Id, Name, SBQQ__Optional__c, SBQQ__Product__r.Product_Rollup__c, SBQQ__Quote__r.SBQQ__Account__c, NetPerUnitPrice__c, SBQQ__Quantity__c, Unit__c, SBQQ__Quote__c, PricingModel__c
                    FROM SBQQ__LineItems__r
                    WHERE SBQQ__Product__r.Product_Rollup__c != null 
                    AND SBQQ__Optional__c = false
                )
        FROM SBQQ__Quote__c
           LIMIT 10
       ];
       CPQ_testsA.setupQuoteLines(quoteList, productList, false, false);
        
      List<SBQQ__QuoteLine__c> quoteLine = [
           SELECT Id, Name, SBQQ__Optional__c, SBQQ__Product__r.Product_Rollup__c, SBQQ__Quote__r.SBQQ__Account__c, NetPerUnitPrice__c, SBQQ__Quantity__c, Unit__c, SBQQ__Quote__c, PricingModel__c
           FROM SBQQ__QuoteLine__c
           LIMIT 5
        ];

       //create Product Service record
       Products_Services__c productsServices = new Products_Services__c();
            productsServices.Account_Name__c = quoteList[0].SBQQ__Account__c;
            // productsServices.Contract__c = '80022000000AYZ4AAO';
            productsServices.Quote_Line__c = quoteLine[0].Id;
            // productsServices.RecordTypeId = '01280000000YNDQAA4';
            // productsServices.Surescripts_Product__c = '01t22000000l1goAAA';
            productsServices.Surescripts_Products_Services__c = 'CDM (Clinical Direct Messaging)';
            // productsServices.Unit__c = 'User';

        insert productsServices;

        List<Contract> testContractsList = new List<Contract>();

        //Create contract, link to quote
        Contract testContract1 = new Contract(
            Active__c = true,
            AccountId = quoteList[0].SBQQ__Account__c,
            Status = '1_New Request',
            SBQQ__Quote__c = quoteList[0].Id,
            z_Contract_Type__c = '2_Standalone Contract',
            Date_Customer_Requested_Contract__c = Date.today(),
            Next_Auto_Renewal_Term_Start_Date__c = Date.today(),
            Term_Type__c = 'Auto Renewal',
            EndDate = Date.today().addDays(365),
            StartDate = Date.today(),
            ContractRenewalTerm__c = 12,
            RecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('2_Standalone Contract').getRecordTypeId()
        );

    testContractsList.add(testContract1);
    Test.startTest();
        insert testContractsList;
        testContract1.Status = '5_Executed';
        update testContract1;
        System.assertEquals(productsServices.Surescripts_Products_Services__c, 'CDM (Clinical Direct Messaging)', 'we have a problem with the product/service update ');
    Test.stopTest();
    System.debug('after the insert ' + testContractsList);
    }
}