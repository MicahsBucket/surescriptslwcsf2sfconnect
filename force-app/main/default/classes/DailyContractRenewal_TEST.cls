/****************************************************************************************************
*Description:           This is the test class for the DailyContractRenewal class that handles
*                       updating contracts up for renewal
*
*Required Class(es):    N/A
*
*Organization: ATG
*
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0     08/09/19   Bryce Kilker     Initial Implementation
*****************************************************************************************************/

@isTest 
public with sharing class DailyContractRenewal_TEST {

   @testSetup 
   public static void initialSetup() {
       //Create account to create contract 
        Account acct = new Account(
            Name = 'Test Account', 
            State_of_Incorporation__c = 'Montana', 
            Legal_Name_of_entity__c = 'Test Inc.', 
            Legal_Entity_Type__c = 'corporation'
            );
        
        insert acct;

        List<Contract> testContractsList = new List<Contract>();

        //Create contract, supply formula field requirements for AutoRenewalProcessEligible__c
        Contract testContract1 = new Contract(
            Active__c = true,
            AccountId = acct.Id,
            Status = '1_New Request',
            z_Contract_Type__c = 'Standalone Contract',
            Date_Customer_Requested_Contract__c = Date.today(),
            Next_Auto_Renewal_Term_Start_Date__c = Date.today(),
            Term_Type__c = 'Auto Renewal',
            EndDate = Date.today().addDays(365),
            StartDate = Date.today(),
            ContractRenewalTerm__c = 12,
            RecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('2_Standalone Contract').getRecordTypeId()
        );

        testContractsList.add(testContract1);

        Contract testContract2 = new Contract(
            Active__c = true,
            AccountId = acct.Id,
            ContractRenewalTerm__c = null,
            Status = '1_New Request',
            z_Contract_Type__c = 'Standalone Contract',
            Date_Customer_Requested_Contract__c = Date.today(),
            Next_Auto_Renewal_Term_Start_Date__c = Date.today(),
            Term_Type__c = 'Auto Renewal',
            EndDate = null,
            StartDate = Date.today(),
            RecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('2_Standalone Contract').getRecordTypeId()
        );
        
        testContractsList.add(testContract2);

        Contract testContract3 = new Contract(
            Active__c = true,
            AccountId = acct.Id,
            ContractRenewalTerm__c = 12,
            Status = '1_New Request',
            z_Contract_Type__c = 'Standalone Contract',
            Date_Customer_Requested_Contract__c = Date.today(),
            Next_Auto_Renewal_Term_Start_Date__c = Date.today(),
            Term_Type__c = 'Auto Renewal',
            EndDate = Date.today().addDays(365),
            StartDate = Date.today(),
            RecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('1_NDA or BAA').getRecordTypeId()
        );
    
        testContractsList.add(testContract3);

        insert testContractsList;

       List<Contract> updateContractForRenewal = [
            SELECT Id, Name, Status, AutoRenewalProcessEligible__c
            FROM Contract
            WHERE Id = :testContract1.Id
            LIMIT 3
        ];

        for(Contract contract : updateContractForRenewal) {
            contract.Status  = '5_Executed';
        }

        update updateContractForRenewal;
   }

   static testMethod void testDailyContractRenewals() {
         Contract testContract = [
            SELECT Id, Name, Status, AutoRenewalProcessEligible__c, Next_Auto_Renewal_Term_Start_Date__c, EndDate
            FROM Contract
            WHERE RecordTypeId = :Schema.SObjectType.Contract.getRecordTypeInfosByName().get('2_Standalone Contract').getRecordTypeId()
            AND EndDate = :Date.today().addDays(365)
            LIMIT 1
        ];
            Test.startTest();

                DailyContractRenewal testBatch = new DailyContractRenewal();
                Id batchId = Database.executeBatch(testBatch, 150);
            Test.stopTest();
            Contract testContractResults = [
                SELECT Id, Name, Status, AutoRenewalProcessEligible__c, Next_Auto_Renewal_Term_Start_Date__c, EndDate
                FROM Contract
                WHERE Id = :testContract.Id
                LIMIT 1
            ];
            System.assertEquals(true, testContractResults.AutoRenewalProcessEligible__c , 'Something went wrong with the contract renewal term!');
   }

      static testMethod void testDailyContractRenewalsNotEligable() {
         Contract testContract = [
            SELECT Id, Name, Status, AutoRenewalProcessEligible__c
            FROM Contract
            WHERE RecordTypeId = :Schema.SObjectType.Contract.getRecordTypeInfosByName().get('1_NDA or BAA').getRecordTypeId()
            LIMIT 1
        ];

        testContract.Status = '	4_In Negotiation';

        update testContract;

        Test.startTest();
            DailyContractRenewal testBatch = new DailyContractRenewal();
            Id batchId = Database.executeBatch(testBatch, 150);
        Test.stopTest();
        Contract testContractResults = [
            SELECT Id, Name, Status, AutoRenewalProcessEligible__c, Next_Auto_Renewal_Term_Start_Date__c, EndDate
            FROM Contract
            WHERE Id = :testContract.Id
            LIMIT 1
        ];
        System.assertEquals(false, testContractResults.AutoRenewalProcessEligible__c , 'Something went wrong with the contract renewal term!');

   }

    //this case should hit the error method
      static testMethod void testDailyContractRenewalsNoEndDate() {
         Contract testContract = [
            SELECT Id, Name, Status, EndDate, AutoRenewalProcessEligible__c, StartDate
            FROM Contract
            WHERE EndDate = null
            LIMIT 1
        ];

        
        List<Contract> testList = new List<Contract>();
        testList.add(testContract);
        database.update(testList, false);
    
        
        Test.startTest();
            DailyContractRenewal testBatch = new DailyContractRenewal();
            Id batchId = Database.executeBatch(testBatch, 150);
        Test.stopTest();
        Contract testContractResults = [
            SELECT Id, Name, Status, EndDate
            FROM Contract
            WHERE Id = :testContract.Id
            LIMIT 1
        ];
        System.assertEquals(null, testContractResults.EndDate , 'Something went wrong with the contract renewal term!');
   }

//test schedulable by just checkign if the scheduled job is called
    static testMethod void dailyContractRenewalTest() {
        Test.StartTest();
            DailyContractRenewal sh1 = new DailyContractRenewal();      
            String sch = '0 0 23 * * ?';
            String cronID = system.schedule('Test check', sch, sh1);
            sh1.execute(null);
        Test.stopTest();
        CronTrigger cronTrigger_after = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :cronID];
        System.assertEquals('0 0 23 * * ?', cronTrigger_after.CronExpression, 'Problem with CronExpression! ');
    }
}