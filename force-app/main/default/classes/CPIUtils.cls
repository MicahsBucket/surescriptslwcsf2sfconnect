/**
 * CPIUtils
 * @description Utility class for various CPI methods.
 * @author  Demand Chain Systems (James Loghry)
 * @date  4/10/2017
 */
public class CPIUtils {

	public static String getFormattedDate(Date d, String format){
		Datetime dt = Datetime.newInstance(d.year(),d.month(),d.day());
		return dt.format(format);
	}

	//public static List<String> getQuarterLabels(Integer year, String format){
	//	List<String> labels = new List<String>();
	//	Date startDate = Date.newInstance(year,1,1);
	//	for(Integer i=0; i < 4; i++){
	//		Date currentDate = startDate.addMonths(i*3);
	//		labels.add(getFormatteddate(currentDate,format));
	//	}
	//	return labels;
	//}
}