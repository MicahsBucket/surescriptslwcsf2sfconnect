/*
	Purpose: Override the contact view/edit/new pages to add functionality to communities

	12/21/2016  MD@IC  Created (00140354)
	01/19/2017  MD@IC  Remove override functionality (00140354)
*/
public with sharing class ContactExt{

	public Contact con{get;set;}
	public ApexPages.StandardController ctrl{get;set;}
	//public Boolean displayOverride{get;set;}
	public String mode{get;set;}
	public String conQueryStr{get;set;}

	public String retURL{get;set;}
	public Boolean debug{get;set;}

    public ContactExt(ApexPages.StandardController ctrl){
    	this.ctrl = ctrl;
    	con = new Contact(Id = ctrl.getId());
	    getContact();
    	/*if(!Test.isRunningTest()){
    		ctrl.addFields(new List<String>{'Name','Reviewed_by_Customer__c'});
    		con = (Contact)ctrl.getRecord();
    	}
    	else{
    		con = new Contact(Id = ctrl.getId());
    		getContact();
    	}*/

    	//setDisplayOverride();

    	Map<String, String> params = ApexPages.currentPage().getParameters();
    	debug = params.get('debug') == '1';
    	if(params.containsKey('mode'))
    		mode = params.get('mode');
    	else
    		mode = con.Id == null ? 'new' : 'view';

    	if(params.containsKey('retURL'))
	    	retURL = params.get('retURL');
    }

    /*public PageReference redirect(){
    	String url;
    	String pageUrl = ApexPages.currentPage().getUrl();
    	if(displayOverride){
	    	if(pageUrl.contains('/003/e') || pageUrl.contains('/apex/ContactNew?'))
	    		url = '/apex/Contact?mode=new';
	    	else if(pageUrl.contains('/e?') || pageUrl.contains('/apex/ContactEdit?'))
	    		url = '/apex/Contact?id=' + con.Id + '&mode=edit';
    		if(String.isBlank(url))
    			return null;
	    	if(String.isNotBlank(retURL))
	    		url += '&retURL=' + retURL;
    	}
    	else{
	    	//if(con.Id == null)
	    	if(pageUrl.contains('/003/e') || pageUrl.contains('/apex/ContactNew?'))
	    		url = '/003/e?';
	    	else if(pageUrl.contains('/e?') || pageUrl.contains('/apex/ContactEdit?'))
	    		url = '/' + con.Id + '/e?';
	    	else
	    		url = '/' + con.Id + '?';
	    	url += 'nooverride=1';
	    	url += '&retURL=' + retURL;
    	}
    	PageReference pgRef = new PageReference(url);
    	pgRef.setRedirect(true);
    	return pgRef;
    }*/

    public void getContact(){
    	if(con != null && con.Id != null){
			if(String.isBlank(conQueryStr)){
				Set<String> fields = new Set<String>{'Id', 'Name', 'Reviewed_by_Customer__c', 'Tier__c'};
				/*List<Schema.FieldSetMember> fieldSetMembers = SObjectType.Contact.FieldSets.Community_Override_Details_View.getFields();
				fieldSetMembers.addAll(SObjectType.Contact.FieldSets.Community_Override_Details_Edit.getFields());
				fieldSetMembers.addAll(SObjectType.Contact.FieldSets.Community_Override_Additional_Info_View.getFields());
				fieldSetMembers.addAll(SObjectType.Contact.FieldSets.Community_Override_Additional_Info_Edit.getFields());
				fieldSetMembers.addAll(SObjectType.Contact.FieldSets.Community_Override_Roles.getFields());
				for(Schema.FieldSetMember f : fieldSetMembers)
					fields.add(f.getFieldPath());*/
				for(Schema.FieldSetMember f : SObjectType.Contact.FieldSets.Community_Override_Details_View.getFields())
					fields.add(f.getFieldPath());
				for(Schema.FieldSetMember f : SObjectType.Contact.FieldSets.Community_Override_Details_Edit.getFields())
					fields.add(f.getFieldPath());
				for(Schema.FieldSetMember f : SObjectType.Contact.FieldSets.Community_Override_Additional_Info_View.getFields())
					fields.add(f.getFieldPath());
				for(Schema.FieldSetMember f : SObjectType.Contact.FieldSets.Community_Override_Additional_Info_Edit.getFields())
					fields.add(f.getFieldPath());
				for(Schema.FieldSetMember f : SObjectType.Contact.FieldSets.Community_Override_Roles.getFields())
					fields.add(f.getFieldPath());
				conQueryStr = 'SELECT ' + String.join(new List<String>(fields), ',') + ' FROM Contact WHERE Id = \'' + con.Id + '\'';
			}
			con = Database.query(conQueryStr);
    	}
    }

    /*public void setDisplayOverride(){
        Apex_Code_Settings__c settings = Apex_Code_Settings__c.getOrgDefaults();
        Set<Id> profileIds = new Set<Id>();
        if(String.isNotBlank(settings.Community_Profile_Ids__c))
        	for(String s : settings.Community_Profile_Ids__c.split(','))
        		if(String.isNotBlank(s))
        			profileIds.add((Id)s.trim());
        User u = [SELECT Id, ProfileId FROM User WHERE Id = :UserInfo.getUserId()];
        displayOverride = profileIds.contains(u.ProfileId);

        // TODO: for testing -- remove
        //displayOverride = true;
    }*/

    public Boolean validate(){
    	Boolean err = false;
		if(con.Remove_Contact__c && String.isBlank(con.Reason_to_Remove_Contact__c)){
			err = true;
			con.Reason_to_Remove_Contact__c.addError('You must select a value');
		}
		if(con.Remove_Contact__c && con.Reason_to_Remove_Contact__c == 'Other' && String.isBlank(con.Remove_Contact_Other_Reason__c)){
			err = true;
			con.Remove_Contact_Other_Reason__c.addError('You must enter a value');
		}
    	return err;
    }

    public PageReference saveContact(){
    	if(!validate()){
	    	con.Last_updated_by_Community__c = Date.today();
	    	if(!con.Remove_Contact__c){
	    		con.Reason_to_Remove_Contact__c = null;
	    		con.Remove_Contact_Other_Reason__c = null;
	    	}
            if(mode == 'new')
                con.Reviewed_by_Customer__c = true;
	    	Database.UpsertResult result = Database.upsert(con, false);
	    	if(!result.isSuccess())
	    		for(Database.Error e : result.getErrors())
	    			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
	    	else if(String.isNotBlank(retURL)){
	    		return new PageReference(retURL);
	    	}
	    	else{
		    	getContact();
		    	mode = 'view';
		    	retURL = null;
	    	}
    	}
    	return null;
    }

    public void editContact(){
    	mode = 'edit';
    }

    public void cancelContact(){
    	getContact();
    	mode = 'view';
    }

    public void saveAndReview(){
    	con.Reviewed_by_Customer__c = true;
    	saveContact();
    	if(!ApexPages.getMessages().isEmpty())
    		con.Reviewed_by_Customer__c = false;
    }

    public PageReference cancelNew(){
    	return Page.ContactList;
    	//return new PageReference('/s/contact/Contact/00B80000008tJiSEAU');
    }

}