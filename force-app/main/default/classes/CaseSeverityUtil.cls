/*
    05/04/15    SB @ IC    00114842
*/
public class CaseSeverityUtil {
    
    public static list<Case> checkBusinessHours(list<Case> cases) {
        BusinessHours defaultHours = [SELECT Id FROM BusinessHours WHERE IsDefault = true];
        Boolean defaultWithin = BusinessHours.isWithin(defaultHours.Id,System.now());
        
        list<Case> changed = new list<Case>{};
        for (Case cs : cases) {
            if (defaultWithin && cs.Severity_1_Business_Hours__c != 'Inside') {
                cs.Severity_1_Business_Hours__c = 'Inside';
                changed.add(cs);
            } else if (!defaultWithin && cs.Severity_1_Business_Hours__c != 'Outside') {
                cs.Severity_1_Business_Hours__c = 'Outside';
                changed.add(cs);
            }
        }
        return changed;
    }
}