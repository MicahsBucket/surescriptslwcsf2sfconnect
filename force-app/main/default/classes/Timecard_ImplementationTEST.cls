public with sharing class Timecard_ImplementationTEST {
	//[JGP 9/14/2012] Test Class to unit test the Timecard_ImplementationTrigger
	static testMethod void Timecard_ImplementationTriggerTEST()
	{
		//Create an Account
		Account account = new Account();
		account.Name = 'Test';
		insert(account);
		//Create another Account
		Account wrong = new Account();
		wrong.Name = 'Wrong';
		insert(wrong);
		
		//Create An Implementation Certification
		Implementation_Certification__c impCert = new Implementation_Certification__c();
		impCert.Account__c = account.Id;
		insert(impCert);
		
		//Create a timecard to test trigger functionality
		Service_Timecard__c timecard = new Service_Timecard__c();
		timecard.Timecard_Date__c = date.today();
		timecard.Implementation_Certification__c = impCert.Id;
		timecard.Effort_Hours__c = 10;
		insert(timecard);
	}
}