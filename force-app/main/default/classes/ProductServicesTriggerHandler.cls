public with sharing class ProductServicesTriggerHandler {

    public static void updateProductServicesRecord(List<Contract> updatedContracts, Map<Id, Contract> oldContractMap) {

        //write a catch if there are no associated quotes
        Map<Id, Contract> eligibleContracts = new Map<Id, Contract>();

        // Process only the contract when TriggerProductServiceAutomation__c was turned to 'true'
        for (Contract contract : updatedContracts) {

            // Get the previous version of the contract
            Contract prevContract = oldContractMap.get(contract.Id);

            if (contract.TriggerProductServiceAutomation__c && !prevContract.TriggerProductServiceAutomation__c) {
                eligibleContracts.put(contract.Id, contract);
            }
        }

        //null check if there are no eligible contracts 
        if (eligibleContracts.size() > 0) {

            //loop through quote id's on eligibleContracts to pull the Id's
            Set<Id> quoteIds = new set<Id>();
            for (Contract contract : eligibleContracts.values()) {
                quoteIds.add(contract.SBQQ__Quote__c);
            }

            //Get a Map of quote Id's with associated eligible line items
            Map<Id, SBQQ__Quote__c> quotes = new Map<Id, SBQQ__Quote__c>([
                SELECT Id, SBQQ__Account__c,
                    (
                        SELECT Id, Name, SBQQ__Optional__c, SBQQ__Product__r.Product_Rollup__c, SBQQ__Quote__r.SBQQ__Account__c, NetPerUnitPrice__c, SBQQ__Quantity__c, Unit__c, SBQQ__Quote__c, PricingModel__c, ExcludefromContractPicklist__c
                        FROM SBQQ__LineItems__r
                        // WHERE SBQQ__Product__r.Product_Rollup__c != null 
                        // AND SBQQ__Optional__c = false
                    )
                FROM SBQQ__Quote__c
                WHERE Id IN :quoteIds
                //WHERE Id IN (SELECT SBQQ__Quote__c FROM )
            ]);

            //Create a map of Accound Id's and associated Product Service Records
            Map<Id, List<Products_Services__c>> accountProductServiceMap = new Map<Id, List<Products_Services__c>>();

            List<Account> PSList = [
                SELECT Id,
                    (
                        SELECT Surescripts_Product__c, Surescripts_Products_Services__c, ExcludefromContractPicklist__c
                        FROM Products_Services__r
                    )
                FROM Account
                WHERE Id = :quotes.values().SBQQ__Account__c
            ];

            for (Account account : PSList) {
                accountProductServiceMap.put(account.Id, account.Products_Services__r);
            }

            //Create a map of Contracts and Id's
            List<Contract> contractList = new List<Contract> ([
                SELECT Id, Name, SBQQ__Quote__c
                FROM Contract
                WHERE SBQQ__Quote__c = :quotes.values().Id
            ]);

            Map<Id, Contract> contractMap = new Map<Id, Contract>();

            for (Contract contract : contractList) {
                contractMap.put(contract.SBQQ__Quote__c, contract);
            }

            //Create map of Accounts and associated quotelines
            Map<Id, List<SBQQ__QuoteLine__c>> accountQuotelineMap = new Map<Id, List<SBQQ__QuoteLine__c>>();

            for (Contract contract : eligibleContracts.values()) {
                SBQQ__Quote__c relatedQuote = quotes.get(contract.SBQQ__Quote__c);
                for(SBQQ__QuoteLine__c quoteLine: relatedQuote.SBQQ__LineItems__r) {
                    if(accountQuoteLineMap.keySet().contains(quoteLine.SBQQ__Quote__r.SBQQ__Account__c)) {
                    accountQuoteLineMap.get(quoteLine.SBQQ__Quote__r.SBQQ__Account__c).add(quoteLine);
                    } else {
                        accountQuoteLineMap.put(quoteLine.SBQQ__Quote__r.SBQQ__Account__c, new List<SBQQ__QuoteLine__c>{quoteLine});
                    }
                }
            }
        
            Map<Id, Products_Services__c> PSToUpdate = New Map<Id, Products_Services__c>();
            Map<String, Products_Services__c> PSToInsert = New Map<String, Products_Services__c>();

            for (Id acctId : accountQuoteLineMap.keyset() ) {
                List<SBQQ__QuoteLine__c> qlList = accountQuoteLineMap.get(acctId);
                for (SBQQ__QuoteLine__c quoteLine : qlList) {

                    // Get contract id for the current quote line
                    Id contractId = contractMap.get(quoteLine.SBQQ__Quote__c).Id;

                    String productRollup = quoteLine.SBQQ__Product__r.Product_Rollup__c;

                    for (Products_Services__c ps : accountProductServiceMap.get(acctId)) {
                        
                        if (quoteline.SBQQ__Product__r.Product_Rollup__c != null && quoteline.SBQQ__Optional__c != true) {
                            if (ps.Surescripts_Products_Services__c == productRollup) {
                                ps.Contract__c = contractId;
                                ps.Quote_Line__c = quoteLine.Id;
                                ps.Surescripts_Product__c = quoteLine.SBQQ__Product__c;
                                ps.Surescripts_Products_Services__c = ps.Surescripts_Products_Services__c;
                                ps.Net_Per_Unit_Price__c = quoteLine.NetPerUnitPrice__c;
                                ps.Quantity__c = quoteLine.SBQQ__Quantity__c;
                                ps.PricingModel__c = quoteLine.PricingModel__c;
                                ps.Unit__c = quoteLine.Unit__c;
                                ps.ExcludefromContractPicklist__c = quoteLine.ExcludefromContractPicklist__c;
                                PSToUpdate.put(ps.Id, ps);
                            }
                        } 
                    }

                    if (quoteline.SBQQ__Product__r.Product_Rollup__c != null && quoteline.SBQQ__Optional__c != true) {
                        Products_Services__c newPS = new Products_Services__c(
                            Account_Name__c = acctId,
                            Contract__c = contractId,
                            Quote_Line__c = quoteLine.Id,
                            Surescripts_Product__c = quoteLine.SBQQ__Product__c,
                            Surescripts_Products_Services__c = productRollup,
                            Net_Per_Unit_Price__c = quoteLine.NetPerUnitPrice__c,
                            Quantity__c = quoteLine.SBQQ__Quantity__c,
                            PricingModel__c = quoteLine.PricingModel__c,
                            Unit__c = quoteLine.Unit__c,
                            ExcludefromContractPicklist__c = quoteLine.ExcludefromContractPicklist__c
                        );
                        PSToInsert.put(productRollup, newPS);  // FIXME? ---> It's going to take the last PS created for that Product roll-up
                    }
                }
            }

            List <Products_Services__c> psListToUpdate =  new List<Products_Services__c>();
            System.debug('MAP to Update ' + psListToUpdate);
            psListToUpdate.addAll(PSToUpdate.values());
            update psListToUpdate;

            // Map that contains contract Id and a set with the Product/Service strings
            Map<Id, Set<String>> contractIdProductServiceStringMap = new Map<Id, Set<String>>();
            
            for(Products_Services__c ps : psListToUpdate) {

                // Add updated Product/Services and relate them to the contract
                // Only add-it if the product service should not be excluded.
                if (!ps.ExcludefromContractPicklist__c) {

                    // Check if contract key already exists, if not add it
                    if (!contractIdProductServiceStringMap.containsKey(ps.Contract__c)) {
                        contractIdProductServiceStringMap.put(ps.Contract__c, new Set<String>());
                    }
                    // Add the Product/Service string to the set
                    contractIdProductServiceStringMap.get(ps.Contract__c).add(ps.Surescripts_Products_Services__c);
                }

                // Remove from the List of the Product Services to be inserted, the ones that were already added to the update list
                if (PSToInsert.keySet().contains(ps.Surescripts_Products_Services__c)) {
                    PSToInsert.remove(ps.Surescripts_Products_Services__c);
                }
            }

            // Insert the new Product/Services
            insert PSToInsert.values();
            System.debug('DML VALUES to Update ' + psListToUpdate);
            System.debug('DML Values to insert ' + PSToInsert.values());

            // Now add the new inserted Product/Services to the contract map 
            for (Products_Services__c ps : PSToInsert.values()) {

                // Only add-it if the product service should not be excluded.
                if (!ps.ExcludefromContractPicklist__c) {

                    // Check if contract key already exists, if not add it
                    if (!contractIdProductServiceStringMap.containsKey(ps.Contract__c)) {
                        contractIdProductServiceStringMap.put(ps.Contract__c, new Set<String>());
                    }
                    // Add the Product/Service string to the set
                    contractIdProductServiceStringMap.get(ps.Contract__c).add(ps.Surescripts_Products_Services__c);
                }
            }

            // Build a list of contracts that are going to be updated
            List<Contract> contractsToUpdate = new List<Contract>();
            for (Id contractId : contractIdProductServiceStringMap.keySet()) {
                contractsToUpdate.add(
                    new Contract(
                        Id = contractId,
                        Surescripts_Products_Services__c = String.join(new List<String>(contractIdProductServiceStringMap.get(contractId)), ';')
                    )
                );
            }
            
            // Updates the Product/Services multi-picklist value in the Contract
            System.debug('DML Contract to Update: ' + contractsToUpdate);
            update contractsToUpdate;
        }
    }
}