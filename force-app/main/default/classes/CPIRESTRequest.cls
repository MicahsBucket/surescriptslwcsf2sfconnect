/**
 * CPIRESTRequest
 * @description Utility class for issuing REST requests to CPI / Account Metrics service
 * @author  Demand Chain Systems (James Loghry)
 * @date  4/10/2017
 */
public with sharing class CPIRESTRequest {

	public static List<CPIMetric> getMetrics(String accountId,Date startDate,Date endDate){
		System.debug('JWL: in getMetrics');
		String result = sendCPIGETRequest('/api/v1/cpimetrics',accountId,startDate,endDate);
		System.debug('JWL: returning getMetrics');
		return (List<CPIMetric>)JSON.deserialize(result,List<CPIMetric>.class);
	}

	public static List<CPIRate> getRates(String accountId, Date startDate, Date endDate){
		String result = sendCPIGETRequest('/api/v1/cpirates',accountId,startDate,endDate);
		return (List<CPIRate>)JSON.deserialize(result,List<CPIRate>.class);
	}

	public static String saveMetrics(String batchId, List<CPIMetricSave> metrics){
		String requestUrl = '/api/v1/cpimetrics/?batchId='+batchId;
		return RESTRequest.sendPOST(requestUrl,JSON.serialize(metrics));
	}

	public static String saveRates(String batchId, List<CPIRateSave> rates){
		String requestUrl = '/api/v1/cpirates/?batchId='+batchId;
		return RESTRequest.sendPOST(requestUrl,JSON.serialize(rates));
	}

	public static String getRateTypes(){
		String requestUrl = '/api/v1/cpiratetypes';
		return RESTRequest.sendGET(requestUrl);
	}

	private static String sendCPIGETRequest(String prefixUrl,String accountId, Date startDate, Date endDate){
		if(String.isEmpty(accountId) || startDate == null || endDate == null){
			throw new CPIException('AccountId, StartDate, and EndDate are all required.');
		}
		String formatStr = 'yyyy-MM-dd';
		String startDateStr = CPIUtils.getFormattedDate(startDate,formatStr);
		String endDateStr = CPIUtils.getFormattedDate(endDate,formatStr);
		String requestUrl = prefixUrl+  '?salesforceaccountid=' + accountId + '&startDate=' + startDateStr + '&endDate=' + endDateStr;
		String response = RESTRequest.sendGET(requestUrl);
		System.debug('JWL: response: ' + response);
		System.debug('CPI GET Response: ' + response);
		return response;
	}

	public class CPIRateType {
		@AuraEnabled public Integer rateTypeId {get; set;}
		@AuraEnabled public String label {get; set;}
		@AuraEnabled public Integer min {get; set;}
		@AuraEnabled public Integer max {get; set;}
		@AuraEnabled public boolean mandatory {get; set;}
		@AuraEnabled public String type {get; set;}
	}

	public class CPIMetric implements Comparable{
		@AuraEnabled public String salesforceAccountId {get; set;}
		@AuraEnabled public Integer metricId {get; set;}
		@AuraEnabled public Integer metricTypeId {get; set;}
		@AuraEnabled public Integer format {get; set;}
		@AuraEnabled public String cpiCategory {get; set;}
		@AuraEnabled public String metricLabel {get; set;}
		@AuraEnabled public String salesforceDisplayName {get; set;}
		@AuraEnabled public Decimal weight {get; set;}
		@AuraEnabled public Decimal goal {get; set;}
		@AuraEnabled public Boolean hasWeight {get; set;}
		@AuraEnabled public Boolean hasGoal {get; set;}
		@AuraEnabled public String goalMathSymbol {get; set;}
		@AuraEnabled public String startDate {get; set;}
		@AuraEnabled public String endDate {get; set;}
		@AuraEnabled public String createdBySalesforceUser {get; set;}
		@AuraEnabled public String updatedBySalesforceUser {get; set;}
		@AuraEnabled public Integer sortOrder {get; set;}
		@AuraEnabled public Double validationMinValue {get; set;}
		@AuraEnabled public Double validationMaxValue {get; set;}
		@AuraEnabled public String label {get; set;}
		//Custom values added by James
		@AuraEnabled public boolean readOnly {get; set;}
		@AuraEnabled public boolean yearReadOnly {get; set;}

		public Integer compareTo(Object compareTo) {
	        CPIMetric compareToMY = (CPIMetric)compareTo;
	        Integer result =
	        	(sortOrder == compareToMY.sortOrder ?
	        		0 :
	        		(sortOrder > compareToMY.sortOrder ?
	        			1 : - 1
	        		)
	        	);

	        if(result == 0){
	        	Date thisDate = Date.valueOf(startDate);
	        	Date thatDate = Date.valueOf(compareToMY.startDate);
	        	result = 
	        		(thisDate == thatDate ?
	        			0 :
	        			(thisDate > thatDate ?
	        				1 : - 1
	        			)
	        	);
	        }
	        return result;
	    }
	}

	public class CPIRate{
		@AuraEnabled public String salesforceAccountId {get; set;}
		@AuraEnabled public Decimal accountRate {get; set;}
		@AuraEnabled public String startDate {get; set;}
		@AuraEnabled public String endDate {get; set;}
		@AuraEnabled public String updatedBySalesforceUser {get; set;}
		@AuraEnabled public boolean readOnly {get; set;}
		@AuraEnabled public boolean yearReadOnly {get; set;}
		@AuraEnabled public Integer rateTypeId {get; set;}
		@AuraEnabled public String rateId {get; set;}
	}

	public class CPIRateSave{
		@AuraEnabled public String salesforceAccountId {get; set;}
		@AuraEnabled public Decimal accountRate {get; set;}
		@AuraEnabled public String startDate {get; set;}
		@AuraEnabled public String endDate {get; set;}
		@AuraEnabled public String updatedBySalesforceUser {get; set;}
		@AuraEnabled public Integer rateTypeId {get; set;}
		@AuraEnabled public String rateId {get; set;}
	}

	public class CPIMetricSave{
		@AuraEnabled public Integer metricId {get; set;}
		@AuraEnabled public Decimal weight {get; set;}
		@AuraEnabled public Decimal goal {get; set;}
		@AuraEnabled public String updatedBySalesforceUser {get; set;}
		@AuraEnabled public String startDateString {get; set;}
	}
}