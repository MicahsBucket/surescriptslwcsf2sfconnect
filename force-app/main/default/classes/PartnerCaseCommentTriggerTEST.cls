@isTest
public with sharing class PartnerCaseCommentTriggerTEST {
    @isTest
    public static void PartnerCaseCommentTEST() {
        Case c = new Case();
        c.Subject = 'Test Case';
        insert c;

        List<PartnerNetworkConnection> pcList = [SELECT Id, ConnectionName, ConnectionStatus FROM PartnerNetworkConnection WHERE ConnectionStatus != 'Inactive'];

        PartnerNetworkRecordConnection pnc = new PartnerNetworkRecordConnection();
        pnc.ConnectionId = pcList[0].Id;
        pnc.LocalRecordId = c.id;
        insert pnc;

        Partner_Case_Comments__c pcc = new Partner_Case_Comments__c();
        pcc.Case__c = c.id;
        pcc.Partner_Comments__c = 'This is a test comment';
        insert pcc;
    }
}
