public class LeadTriggerHandler {
    
    public static List<Contract> listContracts = new List<Contract>();
    public static Set<Id> setIds = new Set<Id>();
    
    public static void updateConvertedLeadsContracts(List<Lead> convertedLeads){
        
        for(Lead l: convertedLeads){
            if(l.IsConverted && l.ConvertedAccountId != null){
                setIds.add(l.Id);
            }
        }
        
        List<Account> listAcc = [SELECT Id, Name 
                                 FROM Account 
                                 WHERE Name =: System.Label.Contracts_Placeholder_Account_Name];
        System.debug('Line 17 : ' +listAcc);
        
        Id NDARecTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get(System.Label.Contract_Record_Type_NDA).getRecordTypeId();
        System.debug('Line 20 : ' +NDARecTypeId);
        
        for(Lead ld : [SELECT Id, Name, ConvertedAccountId,
                           (SELECT Id, AccountId, Lead__c, RecordTypeId 
                            FROM Contracts__r 
                            Where RecordTypeId =: NDARecTypeId) 
                       FROM Lead 
                       WHERE Id IN: setIds]){
                           
            for(Contract c: ld.Contracts__r){
                if(listAcc.size() > 0){
                    if(c.AccountId == listAcc[0].Id){
                        c.AccountId = ld.ConvertedAccountId;
                        listContracts.add(c);
                    }
                }
            }
                           
        }
        System.debug('Line 38 : ' +listContracts);
        update listContracts;
    }
}