public with sharing class Timecard_Rollup_Calculations_TEST {

	static testMethod void Timecard_Rollup_Calculations_TriggerTEST()
	{
		//Create an Account
		Account account = new Account();
		account.Name = 'Test';
		insert(account);
		
		//Create An Implementation Certification
		Implementation_Certification__c impCert = new Implementation_Certification__c();
		impCert.Account__c = account.Id;
		insert(impCert);
		
		//Create A Billable Service Timecard
		Service_Timecard__c billable = new Service_Timecard__c();
		billable.Account__c = account.Id;
		billable.Implementation_Certification__c = impCert.Id;
		billable.Billable_Status__c = true;
		billable.Effort_Hours__c = 10;
		insert(billable);
		
		//Create A Non-Billable Service Timecard
		Service_Timecard__c nonbillable = new Service_Timecard__c();
		nonbillable.Account__c = account.Id;
		nonbillable.Implementation_Certification__c = impCert.Id;
		nonbillable.Billable_Status__c = false;
		nonbillable.Effort_Hours__c = 20;
		insert(nonbillable);
		
		//Delete Billable
		delete(billable);
		
		//Delete Non-Billable
		delete(nonbillable);
	}
}