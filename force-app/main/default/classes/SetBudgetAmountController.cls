/*
  Purpose:
        The controller for the SetBudgetAmount VF page. When the user clicks the SetBudget
        button a project, this controller sets the Budget amount to the sum of the
        Milestone amounts
        
  Initiative: IconATG FinancialForce PSA Implementation 
  Author:     William Rich
  Company:    IconATG
  Contact:    william.rich@iconatg.com
  Created:    6/31/2014
*/

public with sharing class SetBudgetAmountController {
    private ApexPages.StandardController stdController;
    private Id projectId;
    
    public SetBudgetAmountController(ApexPages.StandardController stdController) {
        this.stdController = stdController;
        this.projectId = ApexPages.currentPage().getParameters().get('id');
    }
    
    public PageReference setBudgetAmount() {
        List<pse__Budget__c> budgets = [
            select
                Id,
                pse__Amount__c
            from pse__Budget__c
            where pse__Project__c = :projectId
        ];
        
        List<AggregateResult> aggResults = [
            select 
                Count(Id) numberOfMilestones,
                sum(pse__Milestone_Amount__c) amount,
                sum(pse__Planned_Hours__c) plannedHours
            from pse__Milestone__c
            where pse__Project__c = :projectId
        ];
        
        Integer numberOfMilestones = 0;
        Decimal amount = 0.0;
        Decimal plannedHours = 0.0;
        if (!aggResults.isEmpty()) {
            numberOfMilestones = (Integer) aggResults.get(0).get('numberOfMilestones');
            amount = (Decimal) aggResults.get(0).get('amount');
            plannedHours = (Decimal) aggResults.get(0).get('plannedHours');
        }
        
        PageReference pageRef = null;
        if (budgets.isEmpty()) {
            String errorMsg = 'This project has no budget';
            if (numberOfMilestones == 0) {
                errorMsg += ' and no milestones';
            }
            errorMsg += '.';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, errorMsg));
        }
        else if (budgets.size() > 1) {
            String errorMsg = 'This project has ' + String.valueOf(budgets.size()) + ' budgets.';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, errorMsg));
        }
        else if (numberOfMilestones == 0) {
            String errorMsg = 'This project has no milestones.';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, errorMsg));
        }
        else {
        	if (amount != 0.0) {
        		System.debug('&&&&& setting budget amount to ' + amount);
                pse__Budget__c budget = budgets.get(0);
                budget.pse__Amount__c = amount;
                budget.pse__Status__c = 'Approved';
                budget.pse__Approved__c = true;
                budget.pse__Include_In_Financials__c = true;
                update budget;
        	}
            
            pse__Proj__c project = [
                select pse__Planned_Hours__c
                from pse__Proj__c
                where Id = :projectId
            ];
            
            project.pse__Planned_Hours__c = plannedHours;
            update Project;
            
            pageRef = this.stdController.cancel();
        }
        
        return pageRef;
    }
    
    public PageReference cancel() {
        return this.stdController.cancel();
    }
}