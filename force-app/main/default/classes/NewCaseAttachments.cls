public with sharing class NewCaseAttachments {
	//Process Bulk Trigger for Attachments
	public static void Process(Attachment[] attachments)
	{
		for (Attachment a:attachments)
		{
			List<Case> cs = new List<Case>();
			cs = [SELECT Id,Case.ContactId,OwnerId,Owner_Email2__c,Email_address__c,Contact_Email__c,Case.File_Attachment_Received__c FROM Case WHERE Case.Id = :a.ParentId];
			if (cs.size() > 0)//Identified that the attachment did take place on a case object
			{
				Case c = cs[0];
				if (c.File_Attachment_Received__c == null)
					c.File_Attachment_Received__c = 1;
				else
					c.File_Attachment_Received__c++;
				update(c);
			}
		}		
	}
	public class CustomException extends Exception {}
	
	static testMethod void myTest() {
	//Create Test Attachments
	List<Attachment> temp = new List<Attachment>();
	//Create a test Contact
	Contact contact = new Contact();
	contact.LastName = 'TEST';
	contact.Email = 'test@test.com';
	insert(contact);
	//Create a test Case
	Case cs = new Case();
	cs.ContactId = contact.Id;
	//cs.Owner_Email__c = 'test@email.com';
	cs.Email_address__c = 'test2@email.com';
	insert(cs);
		for (Integer i = 0; i < 5; i++)
		{
			Attachment t = new Attachment(Name = 'TestAttachment'+i);
			Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
       		t.body = bodyBlob;
			t.ParentId = cs.id;
			temp.add(t);
		}
		//test.startTest(); //Remove Governors for Testing
		insert(temp); //Add the attachments - causes trigger to execute
       	//test.stopTest(); //Place Governors back in place
	}   
}