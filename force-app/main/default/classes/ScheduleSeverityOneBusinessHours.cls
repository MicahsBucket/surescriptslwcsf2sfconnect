global class ScheduleSeverityOneBusinessHours implements Schedulable {
    
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new SeverityOneBusinessHours());
    }
}