@isTest
public with sharing class AttachmentTriggerTEST {
    @isTest
    public static void AttachmentInsertTEST() {
        Case c = new Case();
        c.Subject = 'Test Case';
        insert c;

        List<PartnerNetworkConnection> pcList = [SELECT Id, ConnectionName, ConnectionStatus FROM PartnerNetworkConnection WHERE ConnectionStatus != 'Inactive'];

        PartnerNetworkRecordConnection pnc = new PartnerNetworkRecordConnection();
        pnc.ConnectionId = pcList[0].Id;
        pnc.LocalRecordId = c.id;
        insert pnc;

        ContentVersion cd = new ContentVersion();
        cd.Title = 'TestCD';
        // cd.ParentId = c.Id;
        cd.Description = 'TestCD';
        cd.ContentUrl = 'www.google.com';
        // cd.VersionDate = Blob.ValueOf('this is the body');
        // cd.FileType = 'png';
        insert cd;

        Id cdId = [SELECT Id FROM ContentDocument LIMIT 1].Id;

        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = c.Id;
        cdl.ContentDocumentId = cdId;
        insert cdl;

        Attachment a = new Attachment();
        a.parentId = c.Id;
        a.Name = 'newAttachement';
        a.ContentType = 'png';
        a.Body = Blob.valueOf('this is the body');
        a.Description = 'newAttachement';
        insert a;
    }
}
