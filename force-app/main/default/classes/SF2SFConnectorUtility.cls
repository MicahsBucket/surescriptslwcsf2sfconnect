public without sharing class SF2SFConnectorUtility {
    
    @AuraEnabled(cacheable=true)
    public static List<PartnerNetworkConnection> getConnectionOptions() {
        return [SELECT Id, ConnectionName, ConnectionStatus FROM PartnerNetworkConnection WHERE ConnectionStatus != 'Inactive'];
        
    }

    @AuraEnabled(cacheable=true)
    public static CaseSharingInfoWrapper getExternalSharingRecords(Id recordId) {
        system.debug(recordId);
        PartnerNetworkRecordConnection pnrc = [SELECT Id, ConnectionId, Status, ParentRecordId, PartnerRecordId, StartDate, EndDate, SendEmails, SendOpenTasks, SendClosedTasks, RelatedRecords, LocalRecordId FROM PartnerNetworkRecordConnection WHERE LocalRecordId =: recordId];
        PartnerNetworkConnection pnc = [SELECT ContactId, AccountId, Id, ConnectionName, PrimaryContactId, ConnectionStatus, ResponseDate, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById FROM PartnerNetworkConnection WHERE Id =: pnrc.ConnectionId];
        CaseSharingInfoWrapper CSIW = new CaseSharingInfoWrapper();
        CSIW.PartnerName = pnc.ConnectionName;
        CSIW.SharingStatus = pnrc.Status;
        CSIW.SharingStartDate = pnrc.StartDate;
        CSIW.SharingResponseId = pnrc.PartnerRecordId;

        CaseSharingInfoWrapper sharingInfoToReturn = new CaseSharingInfoWrapper();
        if(CSIW.SharingStatus != 'Inactive'){
            sharingInfoToReturn = CSIW;
        }
        else{
            sharingInfoToReturn = null;
        }
        return sharingInfoToReturn;
    }

    
    
    @AuraEnabled
    public static void startSharing(Id recordId, Id ConnectionId, boolean shareFiles ){
        System.debug('recordId: '+recordId);
        System.debug('ConnectionId: '+ConnectionId);
        List<PartnerNetworkRecordConnection> connectList = new List<PartnerNetworkRecordConnection>();
        PartnerNetworkRecordConnection connect = new PartnerNetworkRecordConnection();
        connect.ConnectionId = ConnectionId;
        connect.LocalRecordId = recordId;
        if(shareFiles == true){
            connect.RelatedRecords = 'Case, CaseComment, Attachment';
        }
        else{
            connect.RelatedRecords = 'Case, CaseComment';
        }
        connectList.add(connect);
        if(!Test.isRunningTest()){ 
            upsert connectList;
        }
    }

    @AuraEnabled
    public static void stopSharing(Id recordId){
        System.debug('recordId: '+recordId);
        PartnerNetworkRecordConnection pnrc = [SELECT Id, ConnectionId, Status, ParentRecordId, PartnerRecordId, StartDate, EndDate, SendEmails, SendOpenTasks, SendClosedTasks, RelatedRecords FROM PartnerNetworkRecordConnection WHERE LocalRecordId =: recordId];
        if(!Test.isRunningTest()){
            delete pnrc;
        }
    }


    public class CaseSharingInfoWrapper{
        @AuraEnabled public string PartnerName;
        @AuraEnabled public string SharingStatus;
        @AuraEnabled public Datetime SharingStartDate;
        @AuraEnabled public string SharingResponseId;
    }

    public static string GetMIMEType(String fileType){
        System.debug(fileType);
        String returnValue = '';
        switch on fileType{
            when 'json' {
                returnValue = 'application/json';
            }
            when 'pdf' {
                returnValue = 'application/pdf';
            }
            when 'zip' {
                returnValue = 'application/zip';
            }
            when 'jpeg' {
                returnValue = 'image/jpeg';
            }
            when 'jpg' {
                returnValue = 'image/jpeg';
            }
            when 'png' {
                returnValue = 'image/png';
            }
            when 'txt' {
                returnValue = 'text/plain';
            }
            when 'csv' {
                returnValue = 'text/csv';
            }
            when 'html' {
                returnValue = 'text/html';
            }
            when 'xlsx' {
                returnValue = 'application/vnd.ms-excel';
            }
        }
        return returnValue;
    }
}
