public without sharing class ShareFilesController {
    @AuraEnabled
    public static List<AttachmentWrapper> relatedFiles(Id recordId) {
        List<Attachment> attList = [SELECT Id, ParentId, Name, ContentType, BodyLength, Body, Description, CreatedDate, CreatedBy.Name , IsPartnerShared FROM Attachment WHERE ParentId =: recordId];
        // return attList;
        
        List<ContentDocumentLink> cdlList = [SELECT Id, LinkedEntityId, ContentDocumentId  FROM ContentDocumentLink WHERE LinkedEntityId =: recordId];
        set<id> CDIDset = new set<id>();
        for(ContentDocumentLink cdl: cdlList){
            CDIDset.add(cdl.ContentDocumentId);
        }
        map<Id, Id> AttachementIdToContentDocumentId = new map<Id, Id>();
        List<ContentDocument> cdList = [SELECT Id, Title, LatestPublishedVersionId  FROM ContentDocument WHERE Id IN: CDIDset];
        for(Attachment a: attList){
            for(ContentDocument cd: cdList){
                system.debug(String.ValueOf(a.Name).substringBeforeLast('.'));
                system.debug(cd.Title);
                if(String.ValueOf(a.Name).substringBeforeLast('.') == cd.Title){
                    AttachementIdToContentDocumentId.put(a.Id, cd.LatestPublishedVersionId);
                }
            }
        }
        System.debug(AttachementIdToContentDocumentId);
        List<AttachmentWrapper> awList = new List<AttachmentWrapper>();
        for(Attachment a: attList){
            AttachmentWrapper aw = new AttachmentWrapper();
            aw.AttachmentId = a.Id;
            aw.AttachmentName = a.Name ;
            aw.AttachmentCreatedBy = a.CreatedBy.Name ;
            if(a.CreatedBy.Name == 'Connection User' ){
                aw.ShowToggle = false;
                if(SF2SFConnectorUtility.getExternalSharingRecords(a.ParentId) != null){
                    aw.AttachmentCreatedBy = a.CreatedBy.Name + ' (Shared From '+ SF2SFConnectorUtility.getExternalSharingRecords(a.ParentId).PartnerName+')';
                }else{
                    aw.AttachmentCreatedBy = a.CreatedBy.Name;
                }
            }else{
                if(SF2SFConnectorUtility.getExternalSharingRecords(a.ParentId) == null){
                    aw.ShowToggle = false; 
                }else{
                    aw.ShowToggle = true;
                }
                
                aw.AttachmentCreatedBy = a.CreatedBy.Name ;
            }
            aw.AttachmentCreateDate = a.CreatedDate;
            aw.AttachmentIsShared = a.IsPartnerShared;
            aw.AttachmentURL = URL.getSalesforceBaseUrl().toExternalForm() + '/servlet/servlet.FileDownload?file='+a.Id;
            if(AttachementIdToContentDocumentId.containsKey(a.id)){
                string oldURL = URL.getSalesforceBaseUrl().toExternalForm();
                string newURL = oldURL.replace('.my.salesforce.com', '--c.documentforce.com/sfc/servlet.shepherd/version/download/');
                aw.AttachmentContentDocumentDownloadLink = newURL+AttachementIdToContentDocumentId.get(a.Id) +'?asPdf=false&operationContext=CHATTER';
                aw.AttachmentContentDocumentRelatedId = AttachementIdToContentDocumentId.get(a.Id);
            }
            awList.add(aw);
        }
        return awList;
    }

    @AuraEnabled
    public static string saveFile(Id idParent, String strFileName, String base64Data, boolean shareWithConnection) {
        System.debug('shareWithConnection: '+ shareWithConnection);
        System.debug('idParent: '+ idParent);
        System.debug('strFileName: '+ strFileName);
        System.debug('base64Data: '+ base64Data);
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');

        if(shareWithConnection == false){
            List<PartnerNetworkRecordConnection> pnrcList = [SELECT Id, ConnectionId, Status, LocalRecordId, ParentRecordId, PartnerRecordId, 
                                                                StartDate, EndDate, SendEmails, SendOpenTasks, SendClosedTasks, RelatedRecords 
                                                                FROM PartnerNetworkRecordConnection
                                                                WHERE ParentRecordId =: idParent ORDER BY StartDate DESC LIMIT 1];
            system.debug(pnrcList);
            delete pnrcList;
        }
        
        // inserting file
        // ContentVersion cv = new ContentVersion();
        // cv.Title = strFileName;
        // cv.PathOnClient = '/' + strFileName;
        // cv.FirstPublishLocationId = idParent;
        // cv.VersionData = EncodingUtil.base64Decode(base64Data);
        // cv.IsMajorVersion = true;
        // insert cv;

        // ContentDocumentLink cdl = new ContentDocumentLink();
        // cdl.LinkedEntityId = idParent;
        // cdl.ContentDocumentId = cv.ContentDocumentId;
        // cdl.ShareType = 'V';
        // cdl.Visibility = 'AllUsers';
        // insert cdl;
        // return cv;
        return 'Success';
    }

    @AuraEnabled
    public static List<AttachmentWrapper> deleteFiles(List<Id> listOfFileIdsToDelete){
        map<Id, String> CaseIdtoAttachmentMap = new map<Id, String>();
        map<Id, List<Id>> CaseIdtoContentDocumentId = new map<Id, List<Id>>();
        map<Id, List<ContentDocument>> contentDocumentMap = new map<Id, List<ContentDocument>>();
        List<Attachment> attList = [SELECT Id, ParentId, Name, ContentType, BodyLength, Body, Description, CreatedDate  FROM Attachment WHERE Id IN: listOfFileIdsToDelete];
        System.debug(attList);

        for(Attachment a: attList){
            CaseIdtoAttachmentMap.put(a.ParentId, a.Name);
        }
        List<PartnerNetworkRecordConnection> pnrcList = [SELECT Id, ConnectionId, Status, LocalRecordId, ParentRecordId, PartnerRecordId, 
                                                            StartDate, EndDate, SendEmails, SendOpenTasks, SendClosedTasks, RelatedRecords 
                                                        FROM PartnerNetworkRecordConnection
                                                        WHERE LocalRecordId IN: listOfFileIdsToDelete ];
        system.debug(pnrcList);
        Set<Id> CDIDSet = new Set<Id>();
        List<ContentDocumentLink> cdlList = [SELECT Id, LinkedEntityId, ContentDocumentId  FROM ContentDocumentLink WHERE LinkedEntityId IN: CaseIdtoAttachmentMap.keyset()];
        if(cdlList.size() > 0){
            for(ContentDocumentLink cdl: cdlList){
                CDIDSet.add(cdl.ContentDocumentId);
            }
        }

        List<PartnerNetworkRecordConnection> pnrcListtoDelete = new List<PartnerNetworkRecordConnection>();
        for(PartnerNetworkRecordConnection pnrc: pnrcList){
            if(pnrc.Status == 'Sent' || pnrc.Status == 'Received'){
                pnrcListtoDelete.add(pnrc);
            }
        }
        if(pnrcListtoDelete.size() > 0){
            delete pnrcListtoDelete;
        }
        delete attList;
        List<ContentDocument> ContentDocumentsToDelete = new List<ContentDocument>();
        List<ContentDocument> cList = [SELECT Id, Title  FROM ContentDocument WHERE Id IN: CDIDSet];
        if(cList.size() > 0){
            system.debug('cList'+cList);
            for(String att: CaseIdtoAttachmentMap.values()){
                for(ContentDocument cd: cList){
                    System.debug('attachment Title'+ cd.Title);
                    System.debug('att Name'+ String.ValueOf(att).substringBeforeLast('.'));
                    if(String.ValueOf(att).substringBeforeLast('.') == cd.Title){
                        ContentDocumentsToDelete.add(cd);
                    }
                }
            }
            System.debug('ContentDocumentsToDelete' + ContentDocumentsToDelete);
            if(ContentDocumentsToDelete.size() > 0 && !Test.isRunningTest()){
                delete ContentDocumentsToDelete;
            }
        }

        map<Id, Id> AttachementIdToContentDocumentId = new map<Id, Id>();
        List<ContentDocument> cdList = [SELECT Id, Title, LatestPublishedVersionId  FROM ContentDocument WHERE Id IN: CDIDset];
        for(Attachment a: [SELECT Id, ParentId, Name, ContentType, BodyLength, Body, Description, CreatedDate  FROM Attachment WHERE ParentId IN: CaseIdtoAttachmentMap.keyset()]){
            for(ContentDocument cd: cdList){
                system.debug(String.ValueOf(a.Name).substringBeforeLast('.'));
                system.debug(cd.Title);
                if(String.ValueOf(a.Name).substringBeforeLast('.') == cd.Title){
                    AttachementIdToContentDocumentId.put(a.Id, cd.LatestPublishedVersionId);
                }
            }
        }
        System.debug(AttachementIdToContentDocumentId);
        List<AttachmentWrapper> awList = new List<AttachmentWrapper>();
        List<Attachment> attListToReturn = [SELECT Id, ParentId, Name, ContentType, BodyLength, Body, Description, CreatedDate, IsPartnerShared, Attachment.CreatedBy.Name FROM Attachment WHERE ParentId IN: CaseIdtoAttachmentMap.keyset()];
        for(Attachment a: attListToReturn){
            AttachmentWrapper aw = new AttachmentWrapper();
            aw.AttachmentId = a.Id;
            aw.AttachmentName = a.Name;
            aw.AttachmentCreatedBy = a.CreatedBy.Name ;
            if(a.CreatedBy.Name == 'Connection User'){
                aw.ShowToggle = false;
                aw.AttachmentCreatedBy = a.CreatedBy.Name + ' (Shared From '+ SF2SFConnectorUtility.getExternalSharingRecords(a.ParentId).PartnerName+')';
            }else{
                aw.ShowToggle = true;
                aw.AttachmentCreatedBy = a.CreatedBy.Name ;
            }
            aw.AttachmentCreateDate = a.CreatedDate;
            aw.AttachmentIsShared = a.IsPartnerShared;
            aw.AttachmentURL = URL.getSalesforceBaseUrl().toExternalForm() + '/servlet/servlet.FileDownload?file='+a.Id;
            if(AttachementIdToContentDocumentId.containsKey(a.id)){
                string oldURL = URL.getSalesforceBaseUrl().toExternalForm();
                string newURL = oldURL.replace('.my.salesforce.com', '--c.documentforce.com/sfc/servlet.shepherd/version/download/');
                aw.AttachmentContentDocumentDownloadLink = newURL+AttachementIdToContentDocumentId.get(a.Id) +'?asPdf=false&operationContext=CHATTER';
                aw.AttachmentContentDocumentRelatedId = AttachementIdToContentDocumentId.get(a.Id);
            }
            awList.add(aw);
        }
        
        return awList;
    }

    @AuraEnabled
    public static List<AttachmentWrapper> updateFileSharing(Id AttachmentId, boolean ShareValue){
        system.debug('AttachmentId '+ AttachmentId);
        system.debug('ShareValue '+ ShareValue);
        map<Id, Attachment> CaseIdToAttachmentMap = new map<Id, Attachment>();
        List<Attachment> attList = [SELECT Id, ParentId, Name, ContentType, BodyLength, Body, Description, CreatedDate, IsPartnerShared, Attachment.CreatedBy.Name FROM Attachment WHERE Id =: AttachmentId];
        for(Attachment a: attList){
            if(a.ParentId != null){
                CaseIdToAttachmentMap.put(a.ParentId, a);
            }
        }
        System.debug('CaseIdToAttachmentMap '+CaseIdToAttachmentMap);
        if(ShareValue == false){
            List<PartnerNetworkRecordConnection> pnrcList = [SELECT Id, ConnectionId, Status, LocalRecordId, ParentRecordId, PartnerRecordId, 
                                                            StartDate, EndDate, SendEmails, SendOpenTasks, SendClosedTasks, RelatedRecords 
                                                        FROM PartnerNetworkRecordConnection
                                                        WHERE LocalRecordId =: AttachmentId ];
            system.debug(pnrcList);
            List<PartnerNetworkRecordConnection> pnrcListtoDelete = new List<PartnerNetworkRecordConnection>();
            for(PartnerNetworkRecordConnection pnrc: pnrcList){
                if(pnrc.Status == 'Sent' || pnrc.Status == 'Received'){
                    pnrcListtoDelete.add(pnrc);
                }
            }
            if(pnrcListtoDelete.size() > 0){
                delete pnrcListtoDelete;
            }
        }

        if(ShareValue == true){
            if(!CaseIdToAttachmentMap.isEmpty()){
                List<PartnerNetworkRecordConnection> pnrcList = [SELECT Id, ConnectionId, Status, LocalRecordId, ParentRecordId, PartnerRecordId, 
                                                            StartDate, EndDate, SendEmails, SendOpenTasks, SendClosedTasks, RelatedRecords 
                                                        FROM PartnerNetworkRecordConnection
                                                        WHERE (ParentRecordId IN: CaseIdToAttachmentMap.keyset() OR LocalRecordId IN: CaseIdToAttachmentMap.keyset()) ];
                system.debug(pnrcList);
                List<PartnerNetworkRecordConnection> newConnectionList = new List<PartnerNetworkRecordConnection>();
                if(pnrcList.size() > 0){
                    for(PartnerNetworkRecordConnection connection: pnrcList){
                        if(connection.Status != 'Inactive' || connection.Status != 'Deleted'){
                            System.debug('CaseIdToAttachmentMap '+CaseIdToAttachmentMap);
                            if(CaseIdToAttachmentMap.containsKey(connection.ParentRecordId)){
                                System.debug('AttachmentId '+AttachmentId);
                                System.debug('CaseIdToAttachmentMap.get(connection.ParentRecordId).Id '+CaseIdToAttachmentMap.get(connection.ParentRecordId).Id);
                                // if(CaseIdToAttachmentMap.get(connection.ParentRecordId).Id == AttachmentId){
                                    PartnerNetworkRecordConnection connect = new PartnerNetworkRecordConnection();
                                    connect.ConnectionId = connection.ConnectionId;
                                    connect.LocalRecordId = CaseIdToAttachmentMap.get(connection.ParentRecordId).Id;
                                    connect.ParentRecordId = CaseIdToAttachmentMap.get(connection.ParentRecordId).ParentId;
                                    // connect.RelatedRecords = 'Case, CaseComment, Attachment';
                                    newConnectionList.add(connect);
                                // }
                            }
                            else if(CaseIdToAttachmentMap.containsKey(connection.LocalRecordId)){
                                System.debug('AttachmentId '+AttachmentId);
                                System.debug('CaseIdToAttachmentMap.get(connection.ParentRecordId).Id '+CaseIdToAttachmentMap.get(connection.LocalRecordId).Id);
                                // if(CaseIdToAttachmentMap.get(connection.ParentRecordId).Id == AttachmentId){
                                    PartnerNetworkRecordConnection connect = new PartnerNetworkRecordConnection();
                                    connect.ConnectionId = connection.ConnectionId;
                                    connect.LocalRecordId = CaseIdToAttachmentMap.get(connection.LocalRecordId).Id;
                                    connect.ParentRecordId = CaseIdToAttachmentMap.get(connection.LocalRecordId).ParentId;
                                    // connect.RelatedRecords = 'Case, CaseComment, Attachment';
                                    newConnectionList.add(connect);
                                // }
                            }
                        }
                    }
                }
                if(newConnectionList.size() > 0){
                    System.debug('newConnectionList.size() '+newConnectionList.size());
                    System.debug('newConnectionList '+newConnectionList);
                    if(!Test.isRunningTest()){
                        upsert newConnectionList;
                    }
                }
            }
        }
        
        List<ContentDocumentLink> cdlList = [SELECT Id, LinkedEntityId, ContentDocumentId  FROM ContentDocumentLink WHERE LinkedEntityId =: CaseIdToAttachmentMap.keyset()];
        set<id> CDIDset = new set<id>();
        for(ContentDocumentLink cdl: cdlList){
            CDIDset.add(cdl.ContentDocumentId);
        }
        map<Id, Id> AttachementIdToContentDocumentId = new map<Id, Id>();
        List<ContentDocument> cdList = [SELECT Id, Title, LatestPublishedVersionId  FROM ContentDocument WHERE Id IN: CDIDset];
        for(Attachment a: attList){
            for(ContentDocument cd: cdList){
                system.debug(String.ValueOf(a.Name).substringBeforeLast('.'));
                system.debug(cd.Title);
                if(String.ValueOf(a.Name).substringBeforeLast('.') == cd.Title){
                    AttachementIdToContentDocumentId.put(a.Id, cd.LatestPublishedVersionId);
                }
            }
        }
        System.debug(AttachementIdToContentDocumentId);
        List<AttachmentWrapper> awList = new List<AttachmentWrapper>();
        List<Attachment> attListToReturn = [SELECT Id, ParentId, Name, ContentType, BodyLength, Body, Description, CreatedDate, IsPartnerShared, Attachment.CreatedBy.Name FROM Attachment WHERE ParentId IN: CaseIdToAttachmentMap.keyset()];
        for(Attachment a: attListToReturn){
            AttachmentWrapper aw = new AttachmentWrapper();
            aw.AttachmentId = a.Id;
            aw.AttachmentName = a.Name;
            // aw.AttachmentCreatedBy = a.CreatedBy.Name ;
            if(a.CreatedBy.Name == 'Connection User'){
                aw.ShowToggle = false;
                if(!Test.isRunningTest()){
                    aw.AttachmentCreatedBy = a.CreatedBy.Name + ' (Shared From '+ SF2SFConnectorUtility.getExternalSharingRecords(a.ParentId).PartnerName+')';
                }
                else{
                    aw.AttachmentCreatedBy = a.CreatedBy.Name;
                }
            }else{
                aw.ShowToggle = true;
                aw.AttachmentCreatedBy = a.CreatedBy.Name ;
            }
            aw.AttachmentCreateDate = a.CreatedDate;
            aw.AttachmentIsShared = a.IsPartnerShared;
            aw.AttachmentURL = URL.getSalesforceBaseUrl().toExternalForm() + '/servlet/servlet.FileDownload?file='+a.Id;
            if(AttachementIdToContentDocumentId.containsKey(a.id)){
                string oldURL = URL.getSalesforceBaseUrl().toExternalForm();
                string newURL = oldURL.replace('.my.salesforce.com', '--c.documentforce.com/sfc/servlet.shepherd/version/download/');
                aw.AttachmentContentDocumentDownloadLink = newURL+AttachementIdToContentDocumentId.get(a.Id) +'?asPdf=false&operationContext=CHATTER';
                aw.AttachmentContentDocumentRelatedId = AttachementIdToContentDocumentId.get(a.Id);
            }
            awList.add(aw);
        }
        return awList;
    }

    @AuraEnabled
    public static List<AttachmentWrapper> updateAllFileSharing(Id recordId){
        system.debug('recordId '+ recordId);
        map<Id, List<Attachment>> CaseIdToAttachmentMap = new map<Id, List<Attachment>>();
        List<Attachment> attList = [SELECT Id, ParentId, Name, ContentType, BodyLength, Body, Description, CreatedDate, IsPartnerShared, Attachment.CreatedBy.Name FROM Attachment WHERE ParentId =: recordId];
        for(Attachment a: attList){
            if(a.ParentId != null){
                if(!CaseIdToAttachmentMap.containsKey(a.ParentId)){
                    CaseIdToAttachmentMap.put(a.ParentId, new List<Attachment>());
                    CaseIdToAttachmentMap.get(a.ParentId).add(a);
                }else{
                    CaseIdToAttachmentMap.get(a.ParentId).add(a);
                }
                
            }
        }
        if(!CaseIdToAttachmentMap.isEmpty()){
            List<PartnerNetworkRecordConnection> pnrcList = [SELECT Id, ConnectionId, Status, LocalRecordId, ParentRecordId, PartnerRecordId, 
                                                        StartDate, EndDate, SendEmails, SendOpenTasks, SendClosedTasks, RelatedRecords 
                                                    FROM PartnerNetworkRecordConnection
                                                    WHERE (ParentRecordId IN: CaseIdToAttachmentMap.keyset() OR LocalRecordId IN: CaseIdToAttachmentMap.keyset()) ];
            system.debug(pnrcList);
            List<PartnerNetworkRecordConnection> newConnectionList = new List<PartnerNetworkRecordConnection>();
            if(pnrcList.size() > 0){
                for(PartnerNetworkRecordConnection connection: pnrcList){
                    if(connection.Status != 'Inactive' || connection.Status != 'Deleted'){
                        System.debug('CaseIdToAttachmentMap '+CaseIdToAttachmentMap);
                        if(CaseIdToAttachmentMap.containsKey(connection.ParentRecordId)){
                            // System.debug('AttachmentId '+AttachmentId);
                            // System.debug('CaseIdToAttachmentMap.get(connection.ParentRecordId).Id '+CaseIdToAttachmentMap.get(connection.ParentRecordId).Id);
                            for(Attachment a: CaseIdToAttachmentMap.get(connection.ParentRecordId)){
                                PartnerNetworkRecordConnection connect = new PartnerNetworkRecordConnection();
                                connect.ConnectionId = connection.ConnectionId;
                                connect.LocalRecordId = a.Id;
                                connect.ParentRecordId = a.ParentId;
                                // connect.RelatedRecords = 'Case, CaseComment, Attachment';
                                newConnectionList.add(connect);
                            }
                            // if(CaseIdToAttachmentMap.get(connection.ParentRecordId).Id == AttachmentId){
                                
                            // }
                        }
                        else if(CaseIdToAttachmentMap.containsKey(connection.LocalRecordId)){
                            // System.debug('AttachmentId '+AttachmentId);
                            // System.debug('CaseIdToAttachmentMap.get(connection.ParentRecordId).Id '+CaseIdToAttachmentMap.get(connection.LocalRecordId).Id);
                            for(Attachment a: CaseIdToAttachmentMap.get(connection.LocalRecordId)){
                                PartnerNetworkRecordConnection connect = new PartnerNetworkRecordConnection();
                                connect.ConnectionId = connection.ConnectionId;
                                connect.LocalRecordId = a.Id;
                                connect.ParentRecordId = a.ParentId;
                                // connect.RelatedRecords = 'Case, CaseComment, Attachment';
                                newConnectionList.add(connect);
                            }
                        }
                    }
                }
                if(newConnectionList.size() > 0 && !Test.isRunningTest()){
                    System.debug('newConnectionList.size() '+newConnectionList.size());
                    System.debug('newConnectionList '+newConnectionList);
                    upsert newConnectionList;
                }
            }
        }
        
        List<ContentDocumentLink> cdlList = [SELECT Id, LinkedEntityId, ContentDocumentId  FROM ContentDocumentLink WHERE LinkedEntityId =: CaseIdToAttachmentMap.keyset()];
        set<id> CDIDset = new set<id>();
        for(ContentDocumentLink cdl: cdlList){
            CDIDset.add(cdl.ContentDocumentId);
        }
        map<Id, Id> AttachementIdToContentDocumentId = new map<Id, Id>();
        List<ContentDocument> cdList = [SELECT Id, Title, LatestPublishedVersionId  FROM ContentDocument WHERE Id IN: CDIDset];
        for(Attachment a: attList){
            for(ContentDocument cd: cdList){
                system.debug(String.ValueOf(a.Name).substringBeforeLast('.'));
                system.debug(cd.Title);
                if(String.ValueOf(a.Name).substringBeforeLast('.') == cd.Title){
                    AttachementIdToContentDocumentId.put(a.Id, cd.LatestPublishedVersionId);
                }
            }
        }
        System.debug(AttachementIdToContentDocumentId);
        List<AttachmentWrapper> awList = new List<AttachmentWrapper>();
        List<Attachment> attListToReturn = [SELECT Id, ParentId, Name, ContentType, BodyLength, Body, Description, CreatedDate, IsPartnerShared, Attachment.CreatedBy.Name FROM Attachment WHERE ParentId IN: CaseIdToAttachmentMap.keyset()];
        for(Attachment a: attListToReturn){
            AttachmentWrapper aw = new AttachmentWrapper();
            aw.AttachmentId = a.Id;
            aw.AttachmentName = a.Name;
            // aw.AttachmentCreatedBy = a.CreatedBy.Name ;
            if(a.CreatedBy.Name == 'Connection User'){
                aw.ShowToggle = false;
                // aw.AttachmentCreatedBy = a.CreatedBy.Name + ' (Shared From '+ SF2SFConnectorUtility.getExternalSharingRecords(a.ParentId).PartnerName+')';
                if(!Test.isRunningTest()){
                    aw.AttachmentCreatedBy = a.CreatedBy.Name + ' (Shared From '+ SF2SFConnectorUtility.getExternalSharingRecords(a.ParentId).PartnerName+')';
                }
                else{
                    aw.AttachmentCreatedBy = a.CreatedBy.Name;
                }
            }else{
                aw.ShowToggle = true;
                aw.AttachmentCreatedBy = a.CreatedBy.Name ;
            }
            aw.AttachmentCreateDate = a.CreatedDate;
            aw.AttachmentIsShared = a.IsPartnerShared;
            aw.AttachmentURL = URL.getSalesforceBaseUrl().toExternalForm() + '/servlet/servlet.FileDownload?file='+a.Id;
            if(AttachementIdToContentDocumentId.containsKey(a.id)){
                string oldURL = URL.getSalesforceBaseUrl().toExternalForm();
                string newURL = oldURL.replace('.my.salesforce.com', '--c.documentforce.com/sfc/servlet.shepherd/version/download/');
                aw.AttachmentContentDocumentDownloadLink = newURL+AttachementIdToContentDocumentId.get(a.Id) +'?asPdf=false&operationContext=CHATTER';
                aw.AttachmentContentDocumentRelatedId = AttachementIdToContentDocumentId.get(a.Id);
            }
            awList.add(aw);
        }
        return awList;
    }

    public class AttachmentWrapper{
        @AuraEnabled public Id AttachmentId;
        @AuraEnabled public String AttachmentName;
        @AuraEnabled public String AttachmentCreatedBy;
        @AuraEnabled public Datetime AttachmentCreateDate;
        @AuraEnabled public String AttachmentURL;
        @AuraEnabled public boolean AttachmentIsShared;
        @AuraEnabled public boolean ShowToggle;
        @AuraEnabled public Id AttachmentContentDocumentRelatedId;
        @AuraEnabled public String AttachmentContentDocumentDownloadLink;
    }
}