public class SfdcS2SCaseSyncQueueable implements Queueable, Database.AllowsCallouts {
   
    @TestVisible private static SfdcS2SRemoteCalloutUtil calloutUtil = new SfdcS2SRemoteCalloutUtil();
    @TestVisible private static SfdcS2SOAuth2Controller controller = new SfdcS2SOAuth2Controller();
    
	public void execute(QueueableContext context) {
        process();
    }
    
    @future(callout=true)
    public static void process() {
        
        //get access token first
        SfdcS2SOAuth oauth = controller.refreshAccessToken(false);
       
        List<FeedItem> feedItemsToInsert = new List<FeedItem>();
        
        //get externally linked cases which are still opened
        List<Case> casesFromMyOrg = getOpenCasesFromMyOrg();
        for (Case c : casesFromMyOrg) {
           
   			//get feed items for each case from my org
            List<FeedItem> feedItemsFromMyCase = getMyFeedItems(c.Id);
           
            //look at each feed item from the remote case
            for (FeedItem feedItemFromRemoteCase : getCaseFeedItemsFromRemoteOrg(c.SFDC_S2S_External_Case_ID__c, oauth)) {
                 
                //insert into local org if not yet inserted
                if (isNewFeedItem(feedItemFromRemoteCase, feedItemsFromMyCase)) {      
                 	feedItemsToInsert.add(insertFeedItem(c.Id, feedItemFromRemoteCase.Body, feedItemFromRemoteCase.IsRichText));
                } 
            }
        }
        
        //insert the feedItems if any
        for (FeedItem item : feedItemsToInsert) {
            insert(item);
        }
        
        //update the last synced time
        for (Case c : casesFromMyOrg) {
            c.SFDC_S2S_Last_Synced__c = System.now();
            update c;
        }
        
    }
    
    @TestVisible private static List<FeedItem> getMyFeedItems(String id) {
        return [SELECT Body FROM FeedItem WHERE ParentId = :id];
    }
    
    @TestVisible private static FeedItem insertFeedItem(String id, String body, boolean isRichText) {
        FeedItem item = new FeedItem (
        	parentId = id,
            body = body,
            isRichText = isRichText
        );                              
        return item;
    }
    
    @TestVisible private static boolean isNewFeedItem(FeedItem feedItemFromRemoteCase, List<FeedItem>feedItemsFromMyCase) {
         for (FeedItem feedItemFromMyCase : feedItemsFromMyCase) {   
         	if (feedItemFromRemoteCase.Body.equals(feedItemFromMyCase.Body)) {
            	return false; 
            }
         }
        
        return true;
    }
    
    @TestVisible private static List<Case> getOpenCasesFromMyOrg() {
        //current callout limit per transaction is 10
        List<Case> cases = [SELECT Id, SFDC_S2S_External_Case_ID__c FROM Case 
                            	WHERE SFDC_S2S_External_Case_ID__c != null AND ClosedDate = null
                           			AND SFDC_S2S_Sync_Enabled__c = True ORDER BY SFDC_S2S_Last_Synced__c DESC LIMIT 10 ];
        return cases;       
    }
    
    
    @TestVisible private static List<FeedItem> getCaseFeedItemsFromRemoteOrg(Id caseId, SfdcS2SOAuth oauth) {
        List<FeedItem> feedItems = new List<FeedItem>();
        List<Object> records = calloutUtil.executeRemoteQuery('SELECT Body, IsRichText FROM FeedItem where ParentId = \'' + caseId + '\'', oauth);
        if (records != null) {
            for (Object recordObj : records) {
                Map<String, Object> record = (Map<String, Object>) recordObj;
                
                FeedItem feedItem = new FeedItem();
                feedItem.Body = (String) record.get('Body');
                feedItem.IsRichText = (Boolean) record.get('IsRichText');
                if (feedItem.Body != null) {
                   feedItems.add(feedItem); 
                }
            }            
        }
		return feedItems;        
    }
    
}