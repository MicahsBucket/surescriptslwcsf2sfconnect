/*
	01/19/2017  MD@IC  Created (00140354)
*/
@isTest
private class ContactListExtTest{

	@isTest static void tester(){
		Account a = new Account(
			Name = 'Test Inc.',
			Account_Manager__c = UserInfo.getUserId());
		insert a;
		Contact c = new Contact(
			//Account, First Name, Last Name, Title, Phone # and Email
			AccountId = a.Id,
			FirstName = 'Test',
			LastName = 'Testerson',
			Title = 'T',
			Phone = '5555555555',
			Email = 'tt@test.com',
			Contact_Status__c = 'Active');
		insert c;

		Test.setCurrentPage(Page.ContactList);
		ContactListExt ctrl = new ContactListExt();
		ctrl.accountId = a.Id;
		ctrl.init();

		ctrl.newSortField = 'Name';
		ctrl.sort();
		ctrl.newSortField = 'Email';
		ctrl.sort();

		System.assertEquals(1, ctrl.getContacts().size(), 'wrong number of contacts');

		PageReference pg = ctrl.newContact();
		//System.assert(false, pg.getParameters().get('mode') == 'new');
		//System.assert(false, pg.getUrl().contains('/apex/contact?'));
		System.assert(pg.getUrl().contains('/apex/contact?') && pg.getParameters().get('mode') == 'new', 'wrong url for new contact: ' + pg.getUrl());

		ctrl.contactId = c.Id;
		//System.assertEquals('/apex/contact?id=' + c.Id + '&mode=view', ctrl.viewContact().getUrl(), 'wrong url for edit contact');
		pg = ctrl.viewContact();
		System.assert(pg.getUrl().contains('/apex/contact?') && pg.getParameters().get('mode') == 'view' && pg.getParameters().get('id') == c.Id, 'wrong url for view contact: ' + pg.getUrl());

		//System.assertEquals('/apex/Contact?mode=edit&id=' + c.Id, ctrl.editContact().getUrl(), 'wrong url for edit contact');

		/*User u;

		System.runAs([SELECT Id FROM User WHERE Id = :UserInfo.getUserId()][0]){
			UserRole r = new UserRole(
				//DeveloperName = 'Test_Role',
				//Name = 'Test Role',
				PortalType = 'CustomerPortal',
				PortalAccountId = a.Id);
			insert r;
			u = new User(
				ProfileId = [SELECT Id FROM Profile WHERE Name = 'Customer Community Plus POC'].Id,
				FirstName = c.FirstName,
				LastName = c.LastName,
				Email = c.Email,
				Username = 'tt' + System.currentTimeMillis() + '@test.com',
				CompanyName = a.Name,
				Title = 'title',
				Alias = 'alias',
				TimeZoneSidKey = 'America/New_York',
				EmailEncodingKey = 'UTF-8',
				LanguageLocaleKey = 'en_US',
				LocaleSidKey = 'en_US',
				UserRoleId = r.Id,
				ContactId = c.Id
			);
			insert u;
		}

		System.runAs(u){
			Test.setCurrentPage(Page.ContactList);
			ContactListExt ctrl = new ContactListExt();
		}*/
	}

}