global class SfdcS2SCasesSyncSchedulable implements Schedulable {


    global void execute(SchedulableContext SC) {
        System.enqueueJob(new SfdcS2SCaseSyncQueueable());
    }
    
    public static void scheduleThis() {
        System.schedule('Salesforce to Salesforce Cases Hourly Sync Job', '0 0 * * * ?', new SfdcS2SCasesSyncSchedulable() ); 
    }
}