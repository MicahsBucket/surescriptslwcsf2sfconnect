@isTest
public with sharing class SF2SFConnectorUtilityTEST {
    @isTest
    public static void getConnectionsTest() {
        SF2SFConnectorUtility.getConnectionOptions();
    }

    @isTest(seeAllData = true)
    public static void getExternalSharingRecordsTest() {
        PartnerNetworkRecordConnection pnrc = [SELECT Id, ConnectionId, Status, ParentRecordId, PartnerRecordId, StartDate, EndDate, SendEmails, SendOpenTasks, SendClosedTasks, RelatedRecords, LocalRecordId FROM PartnerNetworkRecordConnection WHERE LocalRecordId != null LIMIT 1];
        
        SF2SFConnectorUtility.getExternalSharingRecords(pnrc.LocalRecordId);
    }

    @isTest(seeAllData = true)
    public static void startSharingTest() {
        PartnerNetworkRecordConnection pnrc = [SELECT Id, ConnectionId, Status, ParentRecordId, PartnerRecordId, StartDate, EndDate, SendEmails, SendOpenTasks, SendClosedTasks, RelatedRecords, LocalRecordId FROM PartnerNetworkRecordConnection WHERE LocalRecordId != null LIMIT 1];
        
        SF2SFConnectorUtility.startSharing(pnrc.LocalRecordId, pnrc.ConnectionId, false );
    }

    @isTest(seeAllData = true)
    public static void stopSharingTest() {
        PartnerNetworkRecordConnection pnrc = [SELECT Id, ConnectionId, Status, ParentRecordId, PartnerRecordId, StartDate, EndDate, SendEmails, SendOpenTasks, SendClosedTasks, RelatedRecords, LocalRecordId FROM PartnerNetworkRecordConnection WHERE LocalRecordId != null LIMIT 1];
        
        SF2SFConnectorUtility.stopSharing(pnrc.LocalRecordId);
    }
    @isTest
    public static void GetMIMETypeTest() {
        SF2SFConnectorUtility.GetMIMEType('json');
        SF2SFConnectorUtility.GetMIMEType('pdf');
        SF2SFConnectorUtility.GetMIMEType('zip');
        SF2SFConnectorUtility.GetMIMEType('jpeg');
        SF2SFConnectorUtility.GetMIMEType('jpg');
        SF2SFConnectorUtility.GetMIMEType('png');
        SF2SFConnectorUtility.GetMIMEType('txt');
        SF2SFConnectorUtility.GetMIMEType('csv');
        SF2SFConnectorUtility.GetMIMEType('html');
        SF2SFConnectorUtility.GetMIMEType('xlsx');
    }
}
