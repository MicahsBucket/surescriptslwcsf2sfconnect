/*
    05/04/15    SB @ IC    00114842
*/
@isTest
public class TestSeverityOne {
    
    @isTest
    public static void testApex() {
        Case cs = new Case(Subject = 'Test',Priority = 'Severity 1 (Use sparingly)');
        insert cs;
        
        String inOutside = BusinessHours.isWithin([SELECT Id FROM BusinessHours WHERE IsDefault = true].Id,System.now()) ? 'Inside' : 'Outside';
        cs = [SELECT Severity_1_Business_Hours__c FROM Case WHERE Id = :cs.Id];
        System.assertEquals(inOutside,cs.Severity_1_Business_Hours__c);
        
        cs.FLAGS__Initial_Response__c = System.now();
        update cs;
        cs = [SELECT Severity_1_Business_Hours__c FROM Case WHERE Id = :cs.Id];
        System.assertEquals('N/A',cs.Severity_1_Business_Hours__c);
        
        cs.FLAGS__Initial_Response__c = null;
        update cs;
        
        Test.startTest();
        Database.executeBatch(new SeverityOneBusinessHours());
        Test.stopTest();
    }
}