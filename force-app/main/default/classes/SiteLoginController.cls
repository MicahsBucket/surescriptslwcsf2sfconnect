/**
 * An apex page controller that exposes the site login functionality
 */
global with sharing class SiteLoginController {
    global String username {get; set;}
    global String password {get; set;}
       
    global pageReference login() {     
	  String startUrl = 'https://login.salesforce.com/secur/login_portal.jsp?orgId=00D3000000002YC&portalId=06080000000QwMM'; // Prod
	  startUrl += '&un=' + username;
	  startUrl += '&pw='+ password;
	
	  PageReference portalPage	= new PageReference(startUrl);
	  portalPage.setRedirect(true);
	  PageReference p = Site.login(username, password, startUrl);
	  if (p == null) {
	  	    Apexpages.addMessage(new ApexPages.Message (ApexPages.Severity.ERROR,
            'If you continue having problems logging in to the portal please contact Surescripts Customer Care by clicking on the chat button below or at 1.866.797.3239.'));

	  	//return Site.login(username, password, null);
	  	return null;
	  	//return new PageReference('/apex/IncorrectCredentials');
	  } else {
	  	return portalPage;
	  }
	}
    
   	global SiteLoginController () {}
    
    global static testMethod void testSiteLoginController () {
        // Instantiate a new controller with all parameters in the page
        SiteLoginController controller = new SiteLoginController ();
        controller.username = 'test@salesforce.com';
        controller.password = '123456'; 
                
        System.assertEquals(controller.login(),null);                           
    }    
}