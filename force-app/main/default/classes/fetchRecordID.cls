public class fetchRecordID
{
    public fetchRecordID(ApexPages.StandardController controller)
    {
    }
    
    public List<String> obj = new List<String>();
    public List<String> fld = new List<String>();
    public string aniValue = ApexPages.currentPage().getParameters().get('ANI');
    public string completeURL = ApexPages.currentPage().getUrl();
    public string fieldValue;
    public List<Sobject> Ids = new Sobject[]{};
    public PageReference newPage;
    public string url;
    public string qryString;
    
    
    public PageReference getRedirectURL()
    {
        Integer i,notnullindex;
        
        try
        {
             if(completeURL==null)
             {
                 return newPage;
             }
             else
             {
                 String[] tempURL = completeURL.split('vf_');
                 
                 for(i = 1;i < tempURL.size(); i++) {
                     obj.add(tempURL[i].split('_')[0]);
                     fld.add(tempURL[i].split('_')[1]);
                 }
                 
                 for(i = 0;i < fld.size();i++) {
                     fld[i] = fld[i].split('=')[0];
                 }
             }
             
             for(i = 0;i < obj.size();i++) {
                 if(Test.isRunningTest() == false) {
                     if(ApexPages.currentPage().getParameters().get('vf_'+obj[i]+'_'+fld[i]).trim() != '') {
                         notnullindex = i;
                         fieldValue = ApexPages.currentPage().getParameters().get('vf_'+obj[i]+'_'+fld[i]);
                         break;
                     }
                 }
             }
             
             if(notnullindex != null) {
                qryString = 'SELECT Id FROM ' + obj[notnullindex] + ' WHERE ' + obj[notnullindex] + '.' + fld[notnullindex] + ' = ' + '\'' + fieldValue + '\'' ; 
                Ids = Database.query(qryString);
             }
          
             if (Ids.size()==0)
             {
                url = '/search/SearchResults?searchType=2&str='+ aniValue;
                newPage = new PageReference(url);
                newPage.setRedirect(true);
             }
             
             if(Ids.size()==1)
             {
                  newPage = new PageReference('/' + Ids[0].id);
                  newPage.setRedirect(true);
          
             }
             else if(Ids.size()>1)
             {
                 url = '/search/SearchResults?searchType=2&str='+ fieldValue;
                 newPage = new PageReference(url);
                 newPage.setRedirect(true);
             }

        }
        catch(exception e)
        {
            if(aniValue<>null)
            {
                url = '/search/SearchResults?searchType=2&str='+ aniValue;
                newPage = new PageReference(url);
                newPage.setRedirect(true);
            }
        }
        return newPage;
      }
     
    static testMethod void fetchRecordID_Test()
    {
       try
       {

            PageReference pageRef = Page.OpenPage;
            Test.setCurrentPageReference(pageRef);
                               
            Case cId = [Select id,casenumber from Case LIMIT 1];    
            
            fetchRecordID fRecordID = new fetchRecordID(new ApexPages.StandardController(cId) );
            fRecordID.obj.add('Case');
            fRecordID.fld.add('Id');
            
            fRecordID.completeURL = '/apex/OpenPage?A=123&ANI=9198989898&vf_lead_fax='+ cId.casenumber;
            fRecordID.aniValue = '9898989898';
            List<Sobject> testIds = new Sobject[]{};
            fRecordID.getRedirectURL();
            fRecordID.Ids.add(cId);
            fRecordID.getRedirectURL();
            fRecordID.Ids.add(cId);
            fRecordID.getRedirectURL();
            testIds=fRecordID.Ids;
            
            
            fRecordID.completeURL = null;
            List<Sobject> testId = new Sobject[]{};
            fRecordID.getRedirectURL();
            
            /*PageReference testNewPage;
            if(testIds.size()==1)
            {
                testNewPage=fRecordID.newPage;
            }
            
            fRecordID.completeURL = '/apex/OpenPage?A=123&ANI=9198989898&vf_lead_fax=989898';
            fRecordID.getRedirectURL();
            testIds=fRecordID.Ids;
            
            if(testIds.size()==1)
            {
                testNewPage=fRecordID.newPage;
            }
            fRecordID.completeURL = '/apex/OpenPage?A=123&ANI=9198989898&vf_lead_fax=0000002';
            fRecordID.getRedirectURL();
            testIds=fRecordID.Ids;
            
            if(testIds.size()==1)
            {
                testNewPage=fRecordID.newPage;
            }    
            
            fRecordID.completeURL = '/apex/OpenPage?A=123&ANI=9198989898&vf_lead_fax=';
            fRecordID.getRedirectURL();
            testIds=fRecordID.Ids;  
              
            if(testIds.size()==1)
            {
                testNewPage=fRecordID.newPage;
            }  
            fRecordID.completeURL=null;
            fRecordID.getRedirectURL();
            testIds=fRecordID.Ids;
            if(testIds==null)
            {
                system.debug('No record found');
            }
            fRecordID.completeURL = '/apex/OpenPage?A=123&ANI=9198989898&vf_lead1_fax=123';
            fRecordID.getRedirectURL();
            testIds=fRecordID.Ids;
            if(testIds<>null)
            {
                testNewPage=fRecordID.newPage;
            }*/
                        
        }
        catch(System.DmlException e)
        {
            system.assert(e.getMessage().contains('Error Occured'),e.getMessage());
        }
        
 
    }
   
}