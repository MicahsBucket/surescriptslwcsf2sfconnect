({
	/*loadRates : function(component, event) {
		this.loadRatesWithYear(component, component.get("v.currentYear"));
	},
	loadRatesWithYear : function(component,year){
		var yearsWrapper = null;
		var results = component.get("v.results");
		if(!$A.util.isEmpty(results)){
			yearsWrapper = results.yearsWrapper;
		}
		this.handleAction(
			component
			,{
				"currentContractJson" : JSON.stringify(component.get("v.currentContract"))
				,"yearsWrapper" : yearsWrapper
				,"yearLength" : component.get("v.numberOfYearsToDisplay")
			}
			,"c.getRates"
			,this.handleLoadRatesCallback
		);
	},
	handleLoadRatesCallback : function(component, returnValue, ctx){
		component.set("v.currentYear",returnValue.yearsWrapper.selectedYear);
		component.set("v.results",returnValue);
	},*/
	//Overrides abstract component helper method.
	handleLoadTableDataCallback : function(component, returnValue, ctx){
		ctx.checkForDifferingQuartersByRates(returnValue.rates, component);
		ctx.handleLoadTableDataCallbackHelper(component, returnValue, ctx);
	},
	handleSaveCallback : function(component, returnValue, ctx){
		ctx.checkForDifferingQuartersByRates(returnValue.rates, component);
		ctx.handleSaveCallbackHelper(component, returnValue, ctx);
	},
	hideRates : function(component, event, helper) {
		component.set("v.results",null);
	},
	handleSaveRates : function(component, event, helper) {
		var allValid = component.find('editField').reduce(
			function (validFields,inputCmp) {
				if(inputCmp.isRendered()){
					inputCmp.showHelpMessageIfInvalid();
					return validFields && inputCmp.get('v.validity').valid;
				}else{
					return true;
				}
			},
			true
		);

		if(!allValid){
			return;
		}

		var rates = [];
		var view = component.get("v.view");

		var ratesToSave = component.get("v.results.rates");
		var currentYear = component.get("v.currentYear");
		for(var i=0; i < ratesToSave.length; i++){
			var rateToSave = ratesToSave[i];
			for(var k=0; k < rateToSave.years.length; k++){
				var rateYear = rateToSave.years[k];
				if(view === 'year'){
					var firstQuarter = rateYear.quarters[0];
					if(!firstQuarter.readOnly){
						rates.push(firstQuarter);
					}

					for(var l=1; l < rateYear.quarters.length; l++){
						rateYear.quarters[l].accountRate = firstQuarter.accountRate;

						if(!rateYear.quarters[l].readOnly){
							rates.push(rateYear.quarters[l]);
						}
					}
				}else if(view === 'quarter' && currentYear === rateYear.year){
					for(var j=0; j < rateYear.quarters.length; j++){
						if(!rateYear.quarters[j].readOnly){
							rates.push(rateYear.quarters[j]);
						}
					}
				}
			}
		}

		this.handleAction(
			component
			,{
				"contractId" : component.get("v.currentContract").Id
				,"ratesJson" : JSON.stringify(rates)
				,"ywJson" : JSON.stringify(component.get("v.results.yearsWrapper"))
			}
			,"c.saveRates"
			,this.handleSaveCallback
		);
	},
	/*toggleRowAction : function(component, event){
		var idx = parseInt(event.currentTarget.dataset.rateindex, 0);
		var actionMenus = component.find("actionMenuRow");
		this.toggleAction(idx, actionMenus);
	},
	toggleAction : function(actionIdx, actionMenus){
		for(var i=0; i < actionMenus.length; i++){
			if(i === actionIdx){
				$A.util.toggleClass(actionMenus[i],"slds-is-open");
			}else{
				$A.util.removeClass(actionMenus[i],"slds-is-open");
			}
		}
	},
	fillRate : function(component, event){
		var idx = parseInt(event.currentTarget.dataset.rateindex, 0);
		var results = component.get("v.results");
		var rate = results.rates[idx];
		for(var i=1; i < rate.quarters.length; i++){
			rate.quarters[i].accountRate = rate.quarters[0].accountRate;
		}
		component.set("v.results",results);
		$A.util.removeClass(component.find("actionMenuRow")[idx],"slds-is-open");
	},
	handleYearChange : function(component){
		if(!$A.util.isEmpty(component.get("v.results"))){
			var resultsYears = component.get("v.results").years;

			var newYears = component.get("v.years");

			var yearsDifferent = $A.util.isEmpty(newYears) || resultsYears.length !== newYears.length;
			for(var i=0; i < newYears.length && !yearsDifferent; i++){
				yearsDifferent = yearsDifferent || (newYears[i] !== resultsYears[i]);
			}

			if(yearsDifferent){
				this.loadRates(component);
			}
		}
	},*/
	switchView : function(component){
		var view = component.get("v.view");

		var currentYear = component.get("v.currentYear");
		var yearsWrapper = component.get("v.results.yearsWrapper");
		if($A.util.isEmpty(currentYear) && !$A.util.isEmpty(yearsWrapper)){
			component.set("v.currentYear",yearsWrapper.years[0].year);
		}

		if(view === 'year'){
			component.set("v.view","quarter");
		}else{
			component.set("v.view","year");
		}
	},
	toggleRateTypeModal : function(component, event){
		var dataset = event.currentTarget.dataset;
		component.set("v.rateTypeMax",dataset.max);
		component.set("v.rateTypeMin",dataset.min);
		component.set("v.rateTypeLabel",dataset.label);
		var modal = component.find("rateTypeModal");
        $A.util.toggleClass(modal, "slds-hide");
	},
	checkForDifferingQuartersByRates : function(rates, component){
		if(!$A.util.isEmpty(rates)){
			for(var i=0; i < rates.length; i = i + 1){
				if(!$A.util.isEmpty(rates[i].years)){
					for(var j=0; j < rates[i].years.length; j = j + 1){
						this.checkForDifferingQuartersByYear(rates[i].years[j]);
					}
				}
			}
		}
	},
	checkForDifferingQuartersByYear : function(year){
		if(!$A.util.isEmpty(year.quarters)){
			year.quarters[0].containsDifferentQuarters = false;
			for(var i=1; i < year.quarters.length; i++){
				if(year.quarters[i].accountRate !== year.quarters[0].accountRate){
					year.quarters[0].containsDifferentQuarters = true;
					return true;
				}
			}
		}
		return false;
	}
})