({
	/*handleYearChange : function(component, event, helper) {
		helper.handleYearChange(component);
	},*/
	loadRates : function(component, event, helper) {
		helper.loadTableData(component, 'c.getRates');
		//helper.loadRates(component);
	},
	hideRates : function(component, event, helper) {
		helper.hideRates(component);
	},
	handleSaveRates : function(component, event, helper) {
		helper.handleSaveRates(component);
	},
	/*toggleRowAction : function(component, event, helper){
		helper.toggleRowAction(component, event);
	},
	fillRate : function(component, event, helper){
		helper.fillRate(component, event);
	},*/
	switchView : function(component, event, helper){
		helper.switchView(component);
	},
	switchReadOnly : function(component, event, helper){
		component.set("v.readOnly",!component.get("v.readOnly"));
	},
	toggleRateTypeModal : function(component, event, helper){
		helper.toggleRateTypeModal(component, event);
	}
})