({
	doInit : function(component) {
		this.handleAction(
			component
			,{'recordId' : component.get("v.recordId")}
			,"c.getContract"
			,this.handleGetContractCallback
		);
	},
	handleGetContractCallback : function(component, returnValue, ctx){
		returnValue.sobjectType = 'Contract';
		component.set("v.currentContract",returnValue);
	}
})