({
	loadTableData : function(component, actionName){
		this.loadTableDataWithYear(component, actionName, component.get("v.currentYear"))
	},
	loadTableDataWithYear : function(component, actionName, year){
		this.handleAction(
			component
			,{
				"contractId" : component.get("v.currentContract").Id
				,"yearsWrapper" : null
				,"yearLength" : component.get("v.numberOfYearsToDisplay")
				,"readOnly" : false
			}
			,actionName
			,this.handleLoadTableDataCallback
		);
	},
	handleLoadTableDataCallback : function(component, returnValue, ctx){
		this.handleLoadTableDataCallbackHelper(component, returnValue, ctx);
	},
	handleSaveCallback : function(component, returnValue, ctx){
		this.handleSaveCallbackHelper(component, returnValue, ctx);
	},
	handleLoadTableDataCallbackHelper : function(component, returnValue, ctx){
		component.set("v.currentYear",returnValue.yearsWrapper.selectedYear);
		component.set("v.results",returnValue);
		component.set("v.readOnly",returnValue.readOnly);
	},
	handleSaveCallbackHelper : function(component, returnValue, ctx){
		component.set("v.currentYear",returnValue.yearsWrapper.selectedYear);
		component.set("v.results",[]);
		debugger;
		component.set("v.results",returnValue);
		component.set("v.successMessage","CPI Metrics successfully updated.");
	},
	handleAction : function(component, actionParams, actionName, callback){
		//$A.$clientService$.setXHRTimeout(15000);
		this.showSpinner(component);
		this.hideError(component);
		var action = component.get(actionName);
		action.setParams(actionParams);
		var self = this;
		action.setCallback(this,function(a){
			try{
				if(a.getState() !== 'SUCCESS'){
					throw {'message' : 'An undetermined error occurred with the Apex call.'};
				}

				var result = a.getReturnValue();

				//Some error likely inside of the Apex code occurred.
				if(result.state !== 'SUCCESS'){
					//Try to get the error message from the lightningdmlerror object
					var errorEncountered = result.errors[0];
					throw {
						'message' : 'An error occurred in the apex call',
						'extendedMessage' : errorEncountered
					};
				}

				//Will throw a JSON exception if the result cannot be parsed.
				var returnValue = JSON.parse(result.jsonResponse);

				//SUCCESS!!! USe the parameterized callback for additional / specific logic.
				var concreteComponent = component.getConcreteComponent();
				callback(concreteComponent,returnValue, self);

				this.hideSpinner(component);
			}catch(ex){
				//Handle any exceptions encountered in the callback
				var errorTitle = 'An error occurred with the action';
				var errorMessage = ex.extendedMessage;

				//Add a detailed description of the error if one is found.
				//if(!$A.util.isEmpty(ex.extendedMessage)){
				//	errorMessage = errorMessage + ' ' + ex.extendedMessage;
				//}
				this.hideSpinner(component);
				self.handleError(component, errorTitle, errorMessage);
			}
		});

		$A.enqueueAction(action);
	},
	handleError : function(component, errorTitle, errorMessage){
		component.set("v.errorTitle",errorTitle);
		component.set("v.errorMessage",errorMessage);
	},
	showSpinner : function(component){
		var spinner = component.getSuper().find("spinner");
		$A.util.removeClass(spinner, "slds-hide");
	},
	hideSpinner : function(component){
		var spinner = component.getSuper().find("spinner");
		$A.util.addClass(spinner, "slds-hide");
	},
	hideError : function(component){
		component.set("v.errorTitle",null);
		component.set("v.errorMessage",null);
		component.set("v.successMessage",null);
	}
})