({
	handleLoadTableDataCallback : function(component, returnValue, ctx){
		ctx.checkForDifferingQuartersByCategories(returnValue.categories, component);
		ctx.handleMetricsLoad(returnValue, component);
		ctx.handleLoadTableDataCallbackHelper(component, returnValue, ctx);

		/*var components = [];
		for(var i = 0; i < returnValue.categories.length; i = i + 1){
			for(var j = 0; j < returnValue.categories[i].metrics.length; j = j + 1){
				for(var k = 0; k < returnValue.categories[i].metrics[j].years.length; k = k + 1){
					for(var l = 0; l < returnValue.categories[i].metrics[j].years[k].quarters.length; l = l + 1){
						var quarter = returnValue.categories[i].metrics[j].years[k].quarters[l];
						var ref = component.getReference("v.results.categories[i].metrics[j].years[k].quarters[l].val");
						var metricValue = quarter.val;

						if(component.get("v.readOnly") || quarter.readOnly){
							components.push(
								[
									'aura:html'
									,{
										tag : "span",
										body : metricValue
									}
								]
							);
						}else{
							components.push(
								[
									'lightning:input'
									,{
										name : "editField",
										label : "&nbsp;",
										value : ref,
										type : "number",
										class : "gridInput",
										min : "0",
										max : "100",
										messageWhenRangeOverflow : $A.get("$Label.c.CPI_Metric_Overflow"),
										messageWhenRangeUnderflow : $A.get("$Label.c.CPI_Metric_Underflow"),
										step : "any"
									}
								]
							);
						}
					}
				}
			}
		}

		$A.createComponents(
	        components,
			function(dynamicComponents, status, errors){
				if(status === 'SUCCESS'){
					var idx = 0;
					var returnValue = component.get("v.results");
					var categories = returnValue.categories;
					for(var i = 0; i < categories.length; i = i + 1){
						for(var j = 0; j < categories[i].metrics.length; j = j + 1){
							for(var k = 0; k < categories[i].metrics[j].years.length; k = k + 1){
								for(var l = 0; l < categories[i].metrics[j].years[k].quarters.length; l = l + 1){
									categories[i].metrics[j].years[k].quarters[l].component = dynamicComponents[idx];
									idx = idx + 1;
								}
							}
						}
					}
            	}else{
            		debugger;
            	}
        	}
        );*/
	},
	handleSaveCallback : function(component, returnValue, ctx){
		ctx.checkForDifferingQuartersByCategories(returnValue.categories, component);
		ctx.handleMetricsLoad(returnValue, component);
		ctx.handleSaveCallbackHelper(component, returnValue, ctx);
	},
	hideMetrics : function(component) {
		component.set("v.results",null);
	},
	handleSaveMetrics : function(component, event, helper) {
		var allValid = component.find('editField').reduce(
			function (validFields,inputCmp) {
				if(inputCmp.isRendered()){
					inputCmp.showHelpMessageIfInvalid();
					return validFields && inputCmp.get('v.validity').valid;
				}else{
					return true;
				}
			},
			true
		);

		if(!allValid){
			return;
		}

		var metrics = [];
		var view = component.get("v.view");
		var currentYear = component.get("v.currentYear");
		var categories = component.get("v.results.categories");

		var displayWeight = component.get("v.displayWeight");
		for(var i=0; i < categories.length; i++){
			var category = categories[i];
			for(var j=0; j < category.metrics.length; j++){
				var metric = category.metrics[j];
				for(var k=0; k < metric.years.length; k++){
					var metricYear = metric.years[k];
					if(view === 'year'){
						var firstQuarter = metricYear.quarters[0];

						if(displayWeight){
							firstQuarter.weight = firstQuarter.val;
						}else{
							firstQuarter.goal = firstQuarter.val;
						}

						if(!firstQuarter.readOnly){
							metrics.push(firstQuarter);
						}

						for(var l=1; l < metricYear.quarters.length; l++){
							metricYear.quarters[l].goal = firstQuarter.goal;
							metricYear.quarters[l].weight = firstQuarter.weight;

							if(!metricYear.quarters[l].readOnly){
								metrics.push(metricYear.quarters[l]);
							}
						}
					}else if(view === 'quarter' && currentYear === metricYear.year){
						for(var m=0; m < metricYear.quarters.length; m++){
							var q = metricYear.quarters[m];
							if(!q.readOnly){
								if(displayWeight){
									q.weight = q.val;
								}else{
									q.goal = q.val;
								}
								metrics.push(q);
							}
						}
					}
				}
			}
		}

		this.handleAction(
			component
			,{
				"contractId" : component.get("v.currentContract").Id
				,"metricsJson" : JSON.stringify(metrics)
				,"ywJson" : JSON.stringify(component.get("v.results.yearsWrapper"))
			}
			,"c.saveMetrics"
			,this.handleSaveCallback
		);

		/*var metrics = [];
		var view = component.get("v.view");
		var categories = component.get("v.results.categories");
		var currentYear = component.get("v.currentYear");

		var yearsWrapper = component.get("v.results.yearsWrapper");

		for(var i=0; i < categories.length; i++){
			var category = categories[i];
			for(var j=0; j < category.metrics.length; j++){
				var metric = category.metrics[j];
				for(var k=0; k < metric.years.length; k++){
					var yearWrapperYear = yearsWrapper.years[k];
					var metricYear = metric.years[k];
					if(view === 'year'){
						var firstQuarter = metricYear.quarters[0];
						var firstQuarterMetric = {
							metric : {
								metricId : firstQuarter.metricId
								,goal : firstQuarter.goal
								,weight : firstQuarter.weight
								,startDateString : yearWrapperYear.quarters[0].startDateString
							}
						}
						//metrics.push(firstQuarterMetric);

						firstQuarterMetric.quarters = [];
						for(var l=1; l < metricYear.quarters.length; l++){
							firstQuarterMetric.quarters.push({ 
								metricId : metricYear.quarters[l].metricId
								,goal : firstQuarter.goal
								,weight : firstQuarter.weight
								,startDateString : yearWrapperYear.quarters[l].startDateString
							});
						}

						metrics.push(firstQuarterMetric);
					}else if(view === 'quarter' && currentYear == metricYear.year){
						for(var l=0; l < metricYear.quarters.length; l++){
							var quarter = metricYear.quarters[l];
							metrics.push({
								metric : {
									metricId : quarter.metricId
									,goal : quarter.goal
									,weight : quarter.weight
									,startDateString : yearWrapperYear.quarters[l].startDateString//quarter.startDateString
								}
							});
						}
					}
				}
			}
		}

		this.handleAction(
			component
			,{
				"contractId" : component.get("v.currentContract").Id
				,"metricsJson" : JSON.stringify(metrics)
				,"ywJson" : JSON.stringify(component.get("v.results.yearsWrapper"))
			}
			,"c.saveMetrics"
			,this.handleSaveCallback
		);*/
	},
	/*handleSaveMetricsCallback : function(component, returnValue, ctx){
		component.set("v.currentYear",returnValue.yearsWrapper.selectedYear);
		component.set("v.results",returnValue);
		component.set("v.successMessage","CPI Metrics successfully updated.");
	},*/
	/*handleYearChange : function(component){
		if(!$A.util.isEmpty(component.get("v.results"))){
			var resultsYears = component.get("v.results").years;

			var newYears = component.get("v.years");

			var yearsDifferent = $A.util.isEmpty(newYears) || resultsYears.length !== newYears.length;
			for(var i=0; i < newYears.length && !yearsDifferent; i++){
				yearsDifferent = yearsDifferent || (newYears[i] !== resultsYears[i]);
			}

			if(yearsDifferent){
				this.loadMetrics(component);
			}

			if(component.get("v.view") === 'quarter'){
				this.loadQuarterLabels(component);
			}
		}
	},*/
	switchView : function(component){
		var view = component.get("v.view");

		var currentYear = component.get("v.currentYear");
		var yearsWrapper = component.get("v.results.yearsWrapper");
		if($A.util.isEmpty(currentYear) && !$A.util.isEmpty(yearsWrapper)){
			component.set("v.currentYear",yearsWrapper.years[0].year);
		}

		if(view === 'year'){
			component.set("v.view","quarter");
		}else{
			component.set("v.view","year");
		}
	},
	checkForDifferingQuartersByCategories : function(categories, component){
		if(!$A.util.isEmpty(categories)){
			for(var i=0; i < categories.length; i = i + 1){
				if(!$A.util.isEmpty(categories[i].metrics)){
					for(var j=0; j < categories[i].metrics.length; j = j + 1){
						if(!$A.util.isEmpty(categories[i].metrics[j].years)){
							for(k = 0; k < categories[i].metrics[j].years.length; k++){
								this.checkForDifferingQuartersByYear(categories[i].metrics[j].years[k], component);
							}
						}
					}
				}
			}
		}
	},
	checkForDifferingQuartersByYear : function(year, component){
		if(!$A.util.isEmpty(year.quarters)){
			year.quarters[0].containsDifferentQuarters = false;
			if(component.get("v.displayGoal")){
				for(var i=1; i < year.quarters.length; i++){
					if(year.quarters[i].goal !== year.quarters[0].goal){
						year.quarters[0].containsDifferentQuarters = true;
						return true;
					}
				}
			}else if(component.get("v.displayWeight")){
				for(var i=1; i < year.quarters.length; i++){
					if(year.quarters[i].weight !== year.quarters[0].weight){
						year.quarters[0].containsDifferentQuarters = true;
						return true;
					}
				}
			}
		}
		return false;
	},
	handleMetricsLoad : function(returnValue, component){
		var displayWeight = component.get("v.displayWeight");
		for(var i = 0; i < returnValue.categories.length; i = i + 1){
			for(var j = 0; j < returnValue.categories[i].metrics.length; j = j + 1){
				for(var k = 0; k < returnValue.categories[i].metrics[j].years.length; k = k + 1){
					for(var l = 0; l < returnValue.categories[i].metrics[j].years[k].quarters.length; l = l + 1){
						var quarter = returnValue.categories[i].metrics[j].years[k].quarters[l];
						if(displayWeight){
							quarter.val = quarter.weight;
						}else{
							quarter.val = quarter.goal;
						}
					}
				}
			}
		}
	}
})