({
	loadMetrics : function(component, event, helper) {
		helper.loadTableData(component, 'c.getMetrics');
	},
	hideMetrics : function(component, event, helper) {
		helper.hideMetrics(component);
	},
	handleSaveMetrics : function(component, event, helper) {
		helper.handleSaveMetrics(component);
	},
	switchView : function(component, event, helper){
		helper.switchView(component);
	},
	handleSystemError : function(component, event, helper){
		debugger;
	},
	switchReadOnly : function(component){
		component.set("v.readOnly",!component.get("v.readOnly"));
	}
})